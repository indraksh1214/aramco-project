/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

const cfenv = require("cfenv");
const dotenv = require("dotenv");
var log4js = require("log4js");

// Load environment variables from local .env when running locally, otherwise use values from Bluemix environment variables
const appEnv = cfenv.getAppEnv();
dotenv.config();

// Environment
const environment =  process.env.NODE_ENV;

// Setup logging
const logger_level = process.env.LOGGER_LEVEL || "debug";
const logger = log4js.getLogger();
logger.level = logger_level;

// Port
const appPortSettings =  process.env.APP_PORT;
const appURL = process.env.APP_URL;

// Bot ID
const secretBotId = process.env.SOCKETIO_BOT_ID || "DefaultBotID";

// Facebook messenger settings
const messengerSettings = {
  credentials: {
    verifyToken: process.env.MESSENGER_VERIFY_TOKEN,
    pageToken: process.env.MESSENGER_PAGE_TOKEN,
    fbAppSecret: process.env.MESSENGER_APP_SECRET,
  },
  webhookEndpoint: process.env.MESSENGER_WEBHOOKENDPOINT, // botmaster will mount this webhook on https://Your_Domain_Name/messenger/webhook1234
};

// Twitter DM settings
const twitterSettings = {
  credentials: {
    consumerKey: process.env.TWITTER_CONSUMER_KEY,
    consumerSecret: process.env.TWITTER_CONSUMER_SECRET,
    accessToken: process.env.TWITTER_ACCESS_TOKEN,
    accessTokenSecret: process.env.TWITTER_TOKEN_SECRET,
  }
}

// Watson Assistant settings for botmaster
const watsonConversationWareOptions = {
  settings: {
    username: process.env.CONVERSATION_USERNAME,
    password: process.env.CONVERSATION_APIKEY,
    version: process.env.CONVERSATION_VERSION_DATE, // as of this writing (01 Apr 2017), only v1 is available
    url: process.env.CONVERSATION_URL, // URL to the API Gateway
  },
  workspaceId: process.env.WORKSPACE_MASTER_ID, // For V1 API
  assistantId: process.env.WATSON_ASSISTANT_ID, // For V2 API
};

// Watson Discovery settings
const discoveryCredentials = {
  apikey: process.env.DISCOVERY_APIKEY,
  version_date: process.env.DISCOVERY_VERSION_DATE,
  environment_id: process.env.DISCOVERY_ENVIRONMENT_ID,
  collection_id: process.env.DISCOVERY_COLLECTION_ID,
  url: process.env.DISCOVERY_URL,
  highlight: process.env.DISCOVERY_OPTION_HIGHLIGHT || false,
};

// Cloudant DB settings
const cloudantDB = {
  url: process.env.CLOUDANT_URL,
  db_answer_store: process.env.CLOUDANT_DB_ANSWER_STORE,
  db_logs: process.env.CLOUDANT_DB_CONVERSATION_LOGS,
  db_feedback: process.env.CLOUDANT_DB_FEEDBACK_LOGS,
};

// Mongo DB settings
const mongoDB = {
  uri: process.env.MONGO_DB_URI,
  dbname: process.env.MONGO_DB_DATABASE,
  ssl: process.env.MONGO_SSL,
  ca_certificate_base64: process.env.MONGO_CA_CERTIFICATE_BASE64,
  db_answer_store: process.env.MONGO_DB_ANSWER_STORE,
  db_faq_store:process.env.MONGO_DB_FAQ_STORE,
  db_logs: process.env.MONGO_DB_CONVERSATION_LOGS,
  db_feedback: process.env.MONGO_DB_FEEDBACK_LOGS,
  db_user_activity_logs: process.env.MONGO_DB_USER_ACTIVITY_LOGS
};

// Redis DB settings
const redisDB = {
  port: process.env.REDIS_PORT,
  host: process.env.REDIS_HOST,
  family: 4, // 4 (IPv4) or 6 (IPv6)
  password: process.env.REDIS_PASSWORD,
  db: 0
};

const crm = {
  myit_api_endpoint: process.env.MYIT_API_ENDPOINT,
  myit_sap_client:process.env.MYIT_SAP_CLIENT
};

const remedyCredentials = {
  remedyAPIHost:process.env.REMEDY_API_ENDPOINT,
  clientId:process.env.REMEDY_CLIENT_ID,
  clientSecret:process.env.REMEDY_CLIENT_SECRET
}

const smartCard = {
  smartcard_api_endpoint:process.env.SMART_CARD_ENDPOINT,
  smartcard_user: process.env.SMARTCARD_USER,
  smartcard_pass: process.env.SMARTCARD_PASS
}

// Log configuration infos
logger.info("Log level: ", logger_level);
logger.info("process.env.NODE_ENV: ", process.env.NODE_ENV);
logger.info("Watson Assistant ID: ", watsonConversationWareOptions.assistantId);
logger.info("Watson Assistant Workspace ID: ", watsonConversationWareOptions.workspaceId);
logger.info("Watson Discovery Collection ID: ", discoveryCredentials.collection_id);

module.exports = {
  environment,
  appPortSettings,
  secretBotId,
  discoveryCredentials,
  logger_level,
  messengerSettings,
  twitterSettings,
  watsonConversationWareOptions,
  appURL,
  cloudantDB,
  mongoDB,
  redisDB,
  crm,
  smartCard,
  remedyCredentials
};
