const io = require("socket.io-client");
const config = require("../../config/config");

class WebsocketTest {
  constructor() {
    this.receivedMessages = [];
  }
  connect() {
    return new Promise(resolve => {
      this.socket = io(`ws://localhost:${config.appPortSettings}`);
      this.socket.on("connect", () => {
        console.log("Connected to websocket");
        resolve();
      });
      this.socket.on("error", error => {
        console.error(error);
      });
      this.socket.on("message", message => {
        console.log("Received message", message);
      });
    });
  }
  disconnect() {
    return new Promise(resolve => {
      if (this.socket.connected) {
        this.socket.on("disconnect", resolve);
        this.socket.disconnect();
      }
    });
  }
  say(text, language) {
    console.log("Sending to the bot:", text);
    this.socket.send({ message: { text, language: language || "en-us" } });
  }
  async sayAndWaitForReply(text, language) {
    this.say(text, language);
    return this.waitForReply();
  }
  async sayAndWaitForReplies(text, numberOfReplies) {
    this.say(text);
    return this.waitForReplies(numberOfReplies);
  }
  waitForTyping() {
    return new Promise(resolve => {
      this.socket.once("message", message => {
        if (message.sender_action === "typing_on") {
          resolve();
        }
      });
    });
  }
  waitForReplies(numberOfReplies) {
    return new Promise(resolve => {
      const replies = [];
      const onMessage = message => {
        if (message.sender_action) {
          return;
        }
        replies.push(message.message);
        if (replies.length === numberOfReplies) {
          this.socket.removeListener("message", onMessage);
          resolve(replies);
        }
      };
      this.socket.on("message", onMessage);
    });
  }
  async waitForReply() {
    const replies = await this.waitForReplies(1);
    return replies[0];
  }
}

module.exports = {
  WebsocketTest
};
