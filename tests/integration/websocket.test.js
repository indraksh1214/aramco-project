const { WebsocketTest } = require("../utils/WebsocketTest");

describe("When communicating via websocket", () => {
  let session;

  jest.setTimeout(30000);

  beforeEach(async () => {
    session = new WebsocketTest();
    await session.connect();
  });

  afterEach(async () => {
    await session.disconnect();
  });

  it("sends initial welcome message", async () => {
    const reply = await session.sayAndWaitForReply("Hi");
    expect(reply.text).toContain("WELCOME");
  });

  it("sends typing indication before replies", async () => {
    session.say("Hi");
    await session.waitForTyping();
    await session.waitForReply();
  });
});
