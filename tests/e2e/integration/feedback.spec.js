const { assert } = require("console");

describe("Feedback tests", () => {
  beforeEach(() => {
    cy.server();
    cy.route("POST", "/feedback").as("feedback");
  });

  beforeEach(() => {
    cy.startChat();
    cy.get("#textInputOne").type("What are the opening times?{enter}");
    cy.get(".from-watson .message-inner-v2").as("message");
  });

  it("adds feedback question to message", () => {
    cy.get("@message")
      .get(".feedbackIcons")
      .contains("Was this answer helpful?");
  });

  describe("When giving positive feedback", () => {
    beforeEach(() => {
      cy.get("@message").get(".upFeedback").click();
    });

    it("sends the feedback", () => {
      cy.wait("@feedback").then(({ request, status }) => {
        expect(request.body.feedback_type).to.equal("POSITIVE");
        expect(request.body.message).to.deep.equal({
          text: "STORE_POSITIVE_FEEDBACK",
        });
        expect(status).to.equal(200);
      });
    });

    it("adds a thank you message", () => {
      cy.contains("Thank you very much, we have learned again.");
    });
  });

  describe("When giving negative feedback", () => {
    beforeEach(() => {
      cy.get("@message").get(".downFeedback").click();
    });

    it("shows a feedback form", () => {
      cy.get(".negativeFeedbackBox").as("form");
      cy.get("@form").contains(
        "Thank you for your feedback. Could you please tell us why?"
      );
      cy.get("@form").contains("The answer is not answering my question.");
      cy.get("@form").contains("The answer is too long.");
      cy.get("@form").contains("The answer is outdated.");
      cy.get("@form").get(".inputField input");
    });

    describe("When submitting the feedback", () => {
      beforeEach(() => {
        cy.contains("The answer is too long.").click();
        cy.get(".inputField input").type("This is a test");
        cy.get(".negativeFeedbackBox .sendButton").click();
      });

      it("sends the feedback", () => {
        cy.wait("@feedback").then(({ request, status }) => {
          expect(request.body.feedback_type).to.equal("NEGATIVE");
          console.log("!!", JSON.stringify(request.body.message));
          expect(request.body.message).to.deep.equal({
            text: "STORE_NEGATIVE_FEEDBACK undefined",
            checkboxes: [
              {
                value: 1,
                label: "The answer is not answering my question.",
                checked: false,
              },
              { value: 2, label: "The answer is too long.", checked: true },
              { value: 3, label: "The answer is outdated.", checked: false },
            ],
            other_reason: "This is a test",
          });
          expect(status).to.equal(200);
        });
      });

      it("adds a thank you message", () => {
        cy.contains("Thank you very much, we have learned again.");
      });
    });
  });
});
