describe("App tests", () => {
  describe("When visiting the website", () => {
    beforeEach(() => {
      cy.visit("/");
    });

    it("shows a header", () => {
      cy.get("header.toolBar")
        .should("exist")
        .contains("Multichannel Accelerator");
    });

    it("shows a footer with text input and send button", () => {
      cy.get("footer.interactionBar").as("footer");
      cy.get("@footer").get(".questionInput input").should("exist");
      cy.get("@footer").get(".sendQuestionButton svg").should("exist");
    });

    it("shows a disclaimer", () => {
      cy.get("#disclaimerConsent").should("exist");
    });

    it("removes the disclaimer when accepting it", () => {
      cy.contains("ACCEPT").click();
      cy.get("#disclaimerConsent").should("not.exist");
    });
  });
});
