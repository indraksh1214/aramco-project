describe("Chat tests", () => {
  describe("When accepting the terms", () => {
    beforeEach(() => {
      cy.visit("/");
      cy.acceptTerms();
    });

    it("shows a typing indicator and message from Watson", () => {
      cy.get("#typing-indicator").should('exist');
      cy.get(".from-watson .botSummary img").should("exist");
      cy.get(".from-watson .message-inner-v2").should("have.length.at.least", 1);
      cy.get("#typing-indicator").should('not.exist');
    });
  });

  describe("When the chat started", () => {
    beforeEach(() => {
      cy.startChat();
    });

    it("sends and shows user message on enter press", () => {
      const text = "How are you?";
      cy.get("#textInputOne").type(text).type("{enter}");
      cy.get(".from-user").contains(text);
    });

    it("sends and shows user message on button press", () => {
      const text = "Good morning";
      cy.get("#textInputOne").type(text);
      cy.get(".sendQuestionButton").click();
      cy.get(".from-user").contains(text);
    });
  });

  describe("When the user sent a message", () => {
    beforeEach(() => {
      cy.startChat();
      cy.get("#textInputOne").type("Hi Watson{enter}");
    });

    it("shows a typing indicator and message from Watson", () => {
      cy.get("#typing-indicator").should('exist');
      cy.get(".from-watson").should("have.length.at.least", 2);
      cy.get("#typing-indicator").should('not.exist');
    });

    it("users can send another message", () => {
      cy.get("#textInputOne").type("How are you doing?{enter}");
      cy.get(".from-watson").should("have.length.at.least", 3);
    });
  });
});
