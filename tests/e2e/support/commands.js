// ***********************************************
// For a comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

/**
 * Accept the terms
 */
Cypress.Commands.add("acceptTerms", () => {
  cy.get(".buttonOK").click();
});

/**
 * Open root site, accept terms ad wait until there is a welcome text from Watson
 */
Cypress.Commands.add("startChat", () => {
  cy.visit("/");
  cy.acceptTerms();
  cy.get(".from-watson"); // wait for first reply
});

/**
 * Check for a typing indicator
 */
Cypress.Commands.add("getTypingIndicator", () => {
  return cy.get("#typing-indicator");
});

/**
 * Gets the messages from watson or user
 */
Cypress.Commands.add("getMessages", (from) => {
  return cy.get(`#scrollingChat .from-${from}`);
});

/**
 * Check for a watson text message
 */
Cypress.Commands.add("containsMessage", { prevSubject: "element" }, (subject, text) => {
  cy.contains(text); // wait for message to appear
  cy.get(`${subject.selector}`).contains(text);
});

/**
 * Check for quick replies
 */
Cypress.Commands.add("containsQuickReplies", { prevSubject: "element" }, (subject, texts) => {
  for (let text of texts) {
    cy.contains(text); // wait for button to appear
    cy.get(`${subject.selector} .buttonsChat`).contains(text);
  }
});

/**
 * Check for discovery results
 */
Cypress.Commands.add("containsDiscoveryResults", { prevSubject: "element" }, (subject, params) => {
  const { title, resultsCount } = params;
  cy.contains(title); // wait for discovery results to appear
  cy.get(`${subject} .longtailResults .longtailCard`).contains(title);
  cy.get(`${subject} .longtailResults .longtailCard`).should('have.length', resultsCount);
});

/**
 * Type text and click send button or press enter
 */
Cypress.Commands.add("sendMessage", (text, params) => {
  cy.get("#textInputOne").type(text);
  if (params && params.by === "button") {
    cy.get(".sendQuestionButton").click();
  } else {
    cy.get("#textInputOne").type("{enter}");
  }
});
