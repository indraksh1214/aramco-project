/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */
"use strict";

const NODE_ENV = process.env.NODE_ENV;

const moment = require("moment");
const express = require("express");
const bodyParser = require("body-parser");
const https = require("https");
const compression = require("compression");
const config = require("./config/config");
const logger = require("./app/utils/logger/logger.js");
const fs = require ("fs");


// var httpsRedirect = require('express-https-redirect');
const {
  IamAuthenticator,
  BearerTokenAuthenticator,
  IamTokenManager
} = require("ibm-watson/auth");
var DiscoveryV2 = require("ibm-watson/discovery/v2");

logger.info("Running on ", process.env.NODE_ENV);

const app = express();


app.use(compression());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

if (process.env.NODE_ENV==="production"){
  const options = {
    key: fs.readFileSync(process.env.KEY_FILE),
    cert: fs.readFileSync(process.env.CERT_FILE),
    passphrase: process.env.PASSPHRASE
  }
  var server = https
    .createServer(options, app)
    .listen(config.appPortSettings, function () {
    logger.info("Server running on secure port: " + config.appPortSettings);
  });

} else {
  var server = app.listen(config.appPortSettings, function () {
    logger.error("not secure server");
    logger.info("Server running on port: " + config.appPortSettings);
  });
}

//Single sign-on

var passport = require('passport');
var saml = require('passport-saml').Strategy;
var userProfile;
var session = require('express-session');

app.use(session({
	secret: process.env.SESSION_SECRET,
	resave: true,
	saveUninitialized: false,
  cookie: {
    maxAge: process.env.BACKEND_SESSION_DURATION*60*1000,
  }
  }))

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
	done(null, user);
});

passport.deserializeUser((user, done) => {
	done(null, user);
});

var saml_parameters;
if(process.env.ARAMCO_SAML == "false")
  saml_parameters = { entryPoint: process.env.SAML_ENTRYPOINT_ONELOGIN, callbackUrl: process.env.SAML_CALLBACKURL_ONELOGIN, acceptedClockSkewMs: -1};
else
  saml_parameters = {
  entryPoint: process.env.SAML_ENTRYPOINT,
  issuer: process.env.SAML_ISSUER,
  callbackUrl: process.env.SAML_CALLBACKURL,
  logoutUrl: process.env.SAML_LOGOUTURL,
  cert: process.env.SAML_CERT,
  disableRequestedAuthnContext: process.env.SAML_disableRequestedAuthnContext,
  acceptedClockSkewMs: -1,
  privateKey: fs.readFileSync(process.env.SAML_PRIVATEKEYPATH , 'utf-8')};

var strategy = new saml(saml_parameters,
	(profile, done) => {
    userProfile = profile;
    done(null, userProfile);
	}
);

passport.use(strategy);

var redirectToLogin = (req, res, next) => {
  if(process.env.SSO_AUTHENTICATION == "true"){
    if(!req.isAuthenticated() || userProfile == null){
		console.log("The user is not authenticated (userProfile is null), you are being redirect to the login page.");
		return res.redirect('./app/login');
  } 
}
	next();
}; 


app.get(
	'/app/login',
	passport.authenticate('saml', {
    //successRedirect: process.env.URL_CONSUME,
	failureRedirect: './app/login'
	})
);


app.get('/app/failed', (req, res) => {
	logger.info("Login failed");
	res.status(401).send('Login failed');
});

app.post(
	'/saml/consume',
	passport.authenticate('saml', {
		failureRedirect: '/app/failed',
		failureFlash: true
	}),
	(req, res) => {
    res.writeHead(200, { 'content-type': 'text/html' })
    fs.createReadStream('./chat-ui/index_afterlogin.html').pipe(res)
  	}
);
//End of Single Sign on

 app.get("/",function (req, res) {
    req.next();
   
});

app.get("/login", redirectToLogin,function (req, res) {
  res.writeHead(200, { 'content-type': 'text/html' })
  fs.createReadStream('./chat-ui/index_afterlogin.html').pipe(res)
 
});

app.use(express.static("./chat-ui/dist/"));

require("./app/api/conversation")(server);

if (process.env.TTS_URL && process.env.TTS_VOICE) {
  app.get("/api/text-to-speech/properties", function (req, res) {
    res.json({
      access_token: process.env.TTS_TOKEN,
      url: process.env.TTS_URL,
      customization: process.env.TTS_CUSTOMIZATION || null,
      voice: process.env.TTS_VOICE,
    })
  })
}

if (process.env.STT_URL && process.env.STT_MODEL) {
  if(process.env.STT_API_KEY){
    const sttAuthenticator = new IamTokenManager({
      apikey: process.env.STT_API_KEY
    });
    app.get("/api/speech-to-text/properties", function (req, res) {
      sttAuthenticator
        .requestToken()
        .then(({result }) => {
            res.json({
              access_token: result.access_token,
              url: process.env.STT_URL,
              model: process.env.STT_MODEL,
              language_customization: process.env.STT_LANGUAGE_CUSTOMIZATION || null,
              acoustic_customization: process.env.STT_ACOUSTIC_CUSTOMIZATION || null,
            })
          })
          .catch(console.error);
    })
  }else if(process.env.STT_TOKEN){
    app.get("/api/speech-to-text/properties", function (req, res) {
      res.json({
        access_token: process.env.STT_TOKEN,
        url: process.env.STT_URL,
        model: process.env.STT_MODEL,
        language_customization: process.env.STT_LANGUAGE_CUSTOMIZATION || null,
        acoustic_customization: process.env.STT_ACOUSTIC_CUSTOMIZATION || null,
      })
      // new AuthorizationV1({
      //   authenticator: new BearerTokenAuthenticator({
      //       bearerToken: process.env.STT_TOKEN,
      //     }),
      //   url: process.env.STT_URL,
      //   disableSslVerification: true
      // }).getToken(function (err, token) {
      //   if (err) {
      //     console.log("token", token);
      //     console.log("Error retrieving token: ", err)
      //     res.status(500).send(err)
      //   } else {
          
      //   }
      // })
    })
  }
}
// require("./app/api/speech/speech-to-text")(app);
// require("./app/api/speech/text-to-speech")(app);

/*
curl -d "fileurl=https://s3.ap.cloud-object-storage.appdomain.cloud/...pdf" -X POST http://localhost:6011/download
This was made to overcome the problem of acess-control-allow-origin from IBM COS
*/
app.get("/download", function (req, res) {
  var fileurl = req.query.fileurl;
  https
    .get(fileurl, function (response) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      res.header("content-type", "application/pdf");
      response.pipe(res);
    })
    .on("error", function () {
      callback(null);
    });
});


app.post("/feedback",  function (req, res) {
  //const database = require("./app/utils/db/cloudant.js");  
  const database = require("./app/utils/db/mongodb.js");

  try {
    var data = req.body;
    if (data.feedback_type.includes("POSITIVE")) {
      var feedback_boolean = true;
    } else if (data.feedback_type.includes("NEGATIVE")) {
      var feedback_boolean = false;
    }

    if (data.message.longtail_answer_id != "undefined") {
      var input = data.watsonUpdate.context.discoveryQuery;
    } else { 
      var input = data.watsonUpdate.input;
      data.message.longtail_answer_id = "ShortTail";
    }

    var feedback_msg = {
      feedback: feedback_boolean,
      conv_id: data.watsonUpdate.context.conversation_id,
      input: input,
      user_id: data.user_id,
      intents: data.watsonUpdate.intents,
      entities: data.watsonUpdate.entities,
      output: data.watsonUpdate.output,
      source: process.env.NODE_ENV,
      timestamp: new Date(),
      timestampHuman: moment().format("LLLL"),
      context: data.watsonUpdate.context,
      longtail_answer_id: data.message.longtail_answer_id,
    };

    if (data.message.checkboxes) {
      feedback_msg.checkbox_1 = data.message.checkboxes[0].checked;
      feedback_msg.checkbox_2 = data.message.checkboxes[1].checked;
      feedback_msg.checkbox_3 = data.message.checkboxes[2].checked;
      //feedback_msg.checkboxes = data.message.checkboxes;
      
      
      
    }
    if (data.message.other_reason) {
      feedback_msg.feedback_from_user = data.message.other_reason;
    } else {
      feedback_msg.feedback_from_user = data.feedback_type;
    }

    logger.debug("feedbac_msg", feedback_msg);
    database.saveFeedback(feedback_msg, function (err, resp) {
      if (err) {
        logger.error("server.js-feedback", err);
      }
    });
  } catch (err) {
    logger.error("server.js-feedback", err);
  }
  res.status(200).json({
    status: "feedback added successfully",
  });
});

app.get("/sessionp", function (req, res) {
  const { v4: uuidv4 } = require("uuid");
  uuidv4();
  var token = uuidv4();
  logger.debug(token);
   res.cookie("JSESSIONID", token, {
    httpOnly: true,
    secure: app.get("env") !== "development",
  });
  res.jsonp({ token: token });
  res.end();
});

/**
 * ENGINEERING CHATBOT ENDPOINTs
 */

app.get("/api/suggest", function (req, res) {

  const {
    IamAuthenticator,
    BearerTokenAuthenticator,
  } = require("ibm-watson/auth");
  let Dauthenticator;
  var discovery = {};
  if (process.env.DISCOVERY_APIKEY) {
    Dauthenticator = new IamAuthenticator({
      apikey: process.env.DISCOVERY_APIKEY,
    });
    const DiscoveryV1 = require("ibm-watson/discovery/v1");
    var discovery = new DiscoveryV1({
      version: config.discoveryCredentials.version_date,
      iam_apikey: config.discoveryCredentials.apikey,
      url: config.discoveryCredentials.url,
    });
    var params = {
      environmentId: config.discoveryCredentials.environment_id,
      collectionId: config.discoveryCredentials.collection_id,
      prefix: req.query.prefix,
      count: 5,
    };
  } else if (process.env.BEARER_TOKEN_DISCOVERY) {
    Dauthenticator = new BearerTokenAuthenticator({
      bearerToken: process.env.BEARER_TOKEN_DISCOVERY,
    });
    var DiscoveryV2 = require("ibm-watson/discovery/v2");

    var discovery = new DiscoveryV2({
      authenticator: Dauthenticator,
      version: config.discoveryCredentials.version_date,
      url: config.discoveryCredentials.url,
      disableSslVerification:
        process.env.DISABLE_SSL_VERIFICATION === "true" ? true : false,
    });
    var params = {
      projectId: process.env.DISCOVERY_PROJECT_ID,
      prefix: req.query.prefix,
      count: 5,
    };
  }

  discovery
    .getAutocompletion(params)
    .then((response) => {
      return res.send(response.result);
    })
    .catch((err) => {
      console.log("error:", err);
    });
});

app.get("/api/page", function (req, res) {
  let Dauthenticator;
  var discovery = {};
  if (process.env.DISCOVERY_APIKEY) {
    Dauthenticator = new IamAuthenticator({
      apikey: process.env.DISCOVERY_APIKEY,
    });
    const DiscoveryV1 = require("ibm-watson/discovery/v1");
    var discovery = new DiscoveryV1({
      version: config.discoveryCredentials.version_date,
      iam_apikey: config.discoveryCredentials.apikey,
      url: config.discoveryCredentials.url,
    });
    var payload = {
      environmentId: config.discoveryCredentials.environment_id,
      collectionId: "", //strange that the colelction is empty
      filter: "document_id::" + req.query.id,
      count: 1,
      naturalLanguageQuery: req.query.text,
    };
  } else if (process.env.BEARER_TOKEN_DISCOVERY) {
    Dauthenticator = new BearerTokenAuthenticator({
      bearerToken: process.env.BEARER_TOKEN_DISCOVERY,
    });
    
    var discovery = new DiscoveryV2({
      authenticator: Dauthenticator,
      version: config.discoveryCredentials.version_date,
      url: config.discoveryCredentials.url,
      disableSslVerification:
        process.env.DISABLE_SSL_VERIFICATION === "true" ? true : false,
    });
    var payload = {
      projectId: process.env.DISCOVERY_PROJECT_ID,
      collectionIds: [],
      filter: "document_id::" + req.query.id ,
      count: 1,
      return: "html,text,document_passages",
      naturalLanguageQuery: req.query.text,
      highlight: true
    };
  }

  discovery.query(payload)
  .then(d_data => {
    
    var passage = "";
    if (d_data.result.results[0].document_passages && d_data.result.results[0].document_passages[0]) {
      passage = d_data.result.results[0].document_passages[0].passage_text;
    }
    return res.json({
      page: d_data.result.results[0].html,
      page_number: d_data.result.results[0].page_number,
      doc_number: d_data.result.results[0].doc_number,
      passage: passage,
      title: d_data.result.results[0].title,
      subtitle: d_data.result.results[0].subtitle,
      text: d_data.result.results[0].text
      
    });
    
  }).catch(err => {
   console.log('error:', err);
   return res.json({
     page: "error",
   });
  })
});

app.get("/userInfo", redirectToLogin, function (req, res) {
  if(process.env.SSO_AUTHENTICATION == "false"){
    res.json({
      "givenname" : "AFM",
      "surname" : "",
      "email" : "",
      "officeLocation" : "",
      "networkId" : "",
      "badgeN": "AFM EM",
      "employeetype":"ACTIVITY LICENSEE"
    });
  } else{
  if(process.env.SIMULATE_SAML_RESPONSE == "true"){
    res.json({
      "givenname" : "Emilio",
      "surname" : "Baresi",
      "email" : "emilio.baresi@ibm.com",
      "officeLocation" : "C-2134,|Floor 2,B Wing,|North Park 3 (Bldg. 33002)|Dhahran|",
      "officeLocation_frontend" : filterPipes("C-2134,|Floor 2,B Wing,|North Park 3 (Bldg. 33002)|Dhahran|"),
      "networkId" : "ebaresi",
      "badgeN": "123456789"
    });
  }else if (!req.user){
    res.status(401).send("User is not authenticated")
  }
  else{
    res.json({
      "givenname" : req.user["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"],
      "surname" : req.user["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"],
      "email" : req.user.nameID,
      "officeLocation" : req.user["http://aramco.com/OfficeLocation"],
      "officeLocation_frontend" : filterPipes(req.user["http://aramco.com/OfficeLocation"]),
      "networkId" : req.user.NetworkID,
      "badgeN": req.user["http://aramco.com/employeeid"],
	  "department": req.user["http://aramco.com/department"],
	  "employeetype": req.user["http://aramco.com/employeetype"].replace(';',''),
	"organization": req.user["http://aramco.com/organization"],
	"businessline": req.user["http://aramco.com/businessline"]? req.user["http://aramco.com/businessline"]: ""
    });
  }
}
});


app.get("/sendCookie" ,function (req, res) {

  if(process.env.SIMULATE_SAP_TOKEN == "true"){
    res.send("This is a simulated SAP token!!!");
  }
  else{
    var cookie = req.headers.cookie.split(';');
    var sap_cookie;
    for(var i=0; i < cookie.length; i++){
    
    if(cookie[i].indexOf("MYSAPSSO2") > -1)
    {
      sap_cookie = cookie[i];
      }}
      
    res.send(sap_cookie.split("=")[1]);
    }
});

function filterPipes(address){
  var add = address.split("|");
  var res = "";
  for(var i = 0; i < add.length; i++){
    if(i == (add.length-2))
      res = res + ",";
    res = res+add[i];
  }
  return res;
}

app.get("/health", (req, res) => {
  console.log('Server Health Check Endpoint called.');
  const data = {
      uptime: process.uptime(),
      message: "Ok",
      date: new Date()
  };

  res.status(200).send(data);
}); 



module.exports = app; 
module.exports.server = server; 
