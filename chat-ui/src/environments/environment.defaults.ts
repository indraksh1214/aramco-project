
export const defaultEnvironment = {
  production: false,
  speechInput: false,
  speechOutput: false,
  mobileWidget: false,
  showLogs: true,
  showRemedy: false
};
