import { Injectable } from '@angular/core';
import { resolve } from 'dns';
import { UserMessage} from '../model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }

  public loginLogic(): Promise<UserMessage> {
    var update;
    //if(!this.checkCookie("IBM_MYSAPSSO2")){ // get the cookies once
    if(true) { // consistently get the cookies 
      var http = new XMLHttpRequest();
      
      return new Promise(function (resolve, reject) {
        http.onreadystatechange = function () { 
          if (http.readyState !== 4) return;
          if (http.status >= 200 && http.status < 300) {
            document.cookie = "IBM_MYSAPSSO2="+http.responseText;
            update = {
            message: {
              text: "logged",
              language: "",
            },
            sessionId: "",
            user_cookies: LoginService.getUserCookies()
          };
            resolve(update);
          } else {
            reject({
              status: http.status,
              statusText: http.statusText
            });
          }
    
        };
        
        http.open('GET', "./sendCookie");
        http.withCredentials =true;
        http.setRequestHeader('X-Forwarded-Proto','https')
        http.send(null);
    
      });
    } else{
      update = {
        message: {
          text: "logged",
          language: "",
        },
        sessionId: "",
        user_cookies: LoginService.getUserCookies()
      };
      return new Promise(resolve => {
          resolve(update);
      });
    }
  }

  private checkCookie(cookieName: string): boolean {
    var name = cookieName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return true;
      }
    }
    return false;
  }

  static getUserCookies(){
    var userCookies = {
      MYSAPSSO2:"none",
      USER_PROFILE:"none"
    }
    var cookies = document.cookie.split(";"); 
    for (var i =0; i< cookies.length; i++){
        var cookie = cookies[i].split("=");
        if (cookie[0].trim()=="IBM_MYSAPSSO2"){
          userCookies.MYSAPSSO2 = cookie[1]
        } 
        if(cookie[0].trim()=="IBM_USERPROFILE"){
          userCookies.USER_PROFILE = cookie[1]
        }
    }
    return userCookies
  }
}
