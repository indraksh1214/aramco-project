import { Location } from '@angular/common';
import { TestBed } from '@angular/core/testing';
import { ChatMessageSender, DiscoveryResult } from '../model';

import { ChatService } from './chat.service';
import { SettingsService } from './settings.service';

class MockSettingsService {
  locale = 'de-DE';
}

class MockLocation {
  path(): string {
    return '';
  }
}

describe('ChatService', () => {
  let service: ChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: SettingsService, useClass: MockSettingsService },
        { provide: Location, useClass: MockLocation },
      ],
    });
    service = TestBed.inject(ChatService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return undefined userID', () => {
    expect(service.userId).toEqual(undefined);
  });

  it('should send a message', () => {
    const sendSpy = spyOn(service.socket, 'send');
    service.sendMessage('Hi');
    expect(sendSpy).toHaveBeenCalledWith({
      message: { text: 'Hi', language: 'de-DE' },
    });
  });

  it('should send a quick reply response', () => {
    const sendSpy = spyOn(service.socket, 'send');
    service.sendQuickReplyResponse({ title: 'Action', payload: 'a' });
    expect(sendSpy).toHaveBeenCalledWith({
      message: { text: 'a', language: 'de-DE' },
    });
  });

  it('should show a length warning once', () => {
    service.showLengthWarning();
    expect(service.messages[0].text).toContain('Hinweis');
    expect(service.messages.length).toEqual(1);
    service.showLengthWarning();
    expect(service.messages.length).toEqual(1);
  });

  it('should add incoming typing indication', () => {
    (service as any).handleMessage({
      sender_action: 'typing_on',
    });
    expect(service.isTyping).toBeTrue();
  });

  it('should add incoming message with text, feedback and empty quick replies', () => {
    service.handleMessage({
      recipient: { id: '123' },
      session: { sess: 'data' },
      message: {
        text: 'Welcome',
        feedback: true,
        quick_replies: [],
      },
    });
    expect(service.isTyping).toBeFalse();
    expect(service.messages).toEqual([
      {
        from: ChatMessageSender.Watson,
        continuous: false,
        quickReplies: [],
        feedback: true,
        text: 'Welcome',
      },
    ]);
    expect(service.sessionData).toEqual({ sess: 'data' });
  });

  it('should add subsequent text message as continuous message', () => {
    service.handleMessage({
      recipient: { id: '123' },
      message: { text: 'A' },
      session: {},
    });
    service.handleMessage({
      recipient: { id: '123' },
      message: { text: 'B' },
      session: {},
    });
    expect(service.messages[0].continuous).toBeFalse();
    expect(service.messages[1].continuous).toBeTrue();
  });

  it('should add incoming message with discovery results', () => {
    service.handleMessage({
      recipient: { id: '123' },
      session: {
        context: {
          discoveryResults: [{}],
        },
      },
      message: {
        text: 'LONGTAIL',
      },
    });
    expect(service.isTyping).toBeFalse();
    expect(service.messages).toEqual([
      {
        from: ChatMessageSender.Watson,
        continuous: false,
        feedback: false,
        discoveryResults: [{} as DiscoveryResult],
      },
    ]);
    expect(service.sessionData).toEqual({
      context: { discoveryResults: [{}] },
    });
  });
});
