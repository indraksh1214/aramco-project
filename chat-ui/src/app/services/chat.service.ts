import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import * as io from 'socket.io-client';
import { v4 as uuidv4 } from 'uuid';
import { SettingsService } from './settings.service';
import { LoginService } from './login.service';
import {
  BotmasterMessage,
  ChatMessage,
  ChatMessageSender,
  QuickReply,
  UserMessage,
} from '../model';
import { detect } from 'detect-browser';
import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Logger } from '../utilities/logger';
import { Evaluator } from '../utilities/evaluator';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  messages: ChatMessage[] = [];
  isTyping = false;
  sessionId: string;
  sessionData: any;
  userId: string;
  private showedLengthWarning = false;
  socket = null;
  isMobile: boolean;
  field;
  logger = Logger.getInstance();
  locations;
  constructor(
    private settingsService: SettingsService,
    private location: Location,
    private loginService: LoginService,
    private http: HttpClient
  ) {
    const browser = detect();
    switch (browser && browser.os) {
      case 'iOS':
      case 'Android OS':
      case 'BlackBerry OS':
      case 'Windows Mobile':
        this.isMobile = true;
        break;
      default:
        this.isMobile = false;
    }

    this.logger.log('isMobile: ', this.isMobile);

    this.initUserId();
    this.initSocket();
    this.fetchLocation();
  }

  private initUserId(): void {
    const queryParams = new URLSearchParams(this.location.path());
    if (queryParams.has('userID')) {
      this.userId = queryParams.get('userID') + '-' + uuidv4();
    } else if (queryParams.has('mobileID')) {
      this.userId = queryParams.get('mobileID');
    } else {
      this.userId = undefined;
    }
  }

  private initSocket(): void {
    let url = '';
    if (this.userId) {
      url = '?botmasterUserId=' + this.userId;
    }

    this.socket = io(url, {
      forceNew: true,
      transports: ['websocket'],
      path:
        (window.location.pathname === '/' ? '' : window.location.pathname) +
        '/socket.io',
    });
    this.socket.on('reconnect_attempt', this.handleReconnectAttempt.bind(this));
    this.socket.on('connect', this.handleConnect.bind(this));
    this.socket.on('ping', this.handlePing.bind(this));
    this.socket.on('disconnect', this.handleDisconnect.bind(this));
    this.socket.on('message', this.handleMessage.bind(this));
    this.socket.on('own_message', this.displayOwnMessage.bind(this));
  }

  get lastMessage(): ChatMessage | null {
    if (!this.messages.length) {
      return null;
    }
    return this.messages[this.messages.length - 1];
  }

  private send(text: string, quickReply: boolean = false): void {
    if (typeof quickReply === undefined) {
      quickReply = false;
    }

    const update = {
      message: {
        text,
        language: this.settingsService.locale,
      },
      quickReply: quickReply,
      sessionId: this.sessionId,
      user_cookies: LoginService.getUserCookies(),
    };
    this.logger.log('Sending Websocket message', update);
    this.socket.send(update, quickReply);
  }

  public restartContext(text: string): void {
    const update = {
      message: {
        text,
        language: this.settingsService.locale,
      },
      user_cookies: LoginService.getUserCookies(),
    };
    this.logger.log('Sending Websocket message', update);
    this.socket.send(update);
    this.messages.push({
      from: ChatMessageSender.User,
      continuous: false,
      feedback: false,
      text: 'Restart',
    });
  }

  start(): void {
    if (this.checkCookie('IBM_USERPROFILE')) this.send('START_ORCHESTRATOR');
    else {
      var http = new XMLHttpRequest();
      http.open('GET', './userInfo');
      http.withCredentials = true;
      http.setRequestHeader('X-Forwarded-Proto', 'https');
      http.send(null);
      http.onload = (xhr) => {
        if (http.status == 200) {
          document.cookie = 'IBM_USERPROFILE=' + http.responseText;
          this.send('START_ORCHESTRATOR');
        }
      };
      http.onerror = (xhr) => {
        window.parent.postMessage('IBM_SAML', '*');
        this.messages.push({
          from: ChatMessageSender.Watson,
          continuous: false,
          feedback: false,
          text:
            'An error occurred related to the user authentication, please refresh the webpage',
        });
      };
    }
  }

  sendMessage(text: string): void {
    this.send(text);
    this.showedLengthWarning = false;
  }

  sendQuickReplyResponse(quickReply: QuickReply): void {
    let replyMessage: UserMessage = {
      message: {
        text: quickReply.title,
        language: this.settingsService.locale,
      },
      sessionId: this.sessionId,
    };
    this.displayOwnMessage(replyMessage);
    this.send(quickReply.payload || quickReply.title, true);
  }

  sendOpenTicketRequest(): void {
    this.send('Troubleshooting & Support');
  }

  showLengthWarning(): void {
    if (this.showedLengthWarning) {
      return;
    }
    this.messages.push({
      from: ChatMessageSender.Watson,
      continuous: false,
      feedback: false,
      text:
        '<b>Note: I realize you are entering a very detailed question. I am happy to try to answer them. However, it is easier for me to find a suitable answer if you ask your question concisely and briefly.</b>',
    });
    this.showedLengthWarning = true;
  }

  private displayOwnMessage(ownMessage: UserMessage): void {
    this.logger.log('Websocket Own message', ownMessage);
    if (
      !(ownMessage.message.text === 'logged') &&
      !(ownMessage.message.text === 'display_user_location')
    )
      this.messages.push({
        from: ChatMessageSender.User,
        continuous: false,
        feedback: false,
        text: ownMessage.message.text,
      });
  }

  private handleReconnectAttempt(): void {
    this.logger.log('Websocket attempting reconnection');
    this.socket.io.opts.transports = ['polling', 'websocket'];
  }

  private handleConnect(): void {
    this.logger.log('Websocket connected to server', this.socket.id);
  }

  private handlePing(): void {
    this.logger.log('Websocket ping', this.socket.id);
    this.socket.emit('pong', {
      beat: 1,
    });
  }

  private handleDisconnect(): void {
    this.logger.warn('Websocket disconnected from server', this.socket.id);
  }

  handleMessage(botmasterMessage: BotmasterMessage): void {
    this.logger.log('Websocket received message', botmasterMessage);
    if (botmasterMessage.sessionId) {
      this.sessionId = botmasterMessage.sessionId;
    }
    if (botmasterMessage.sender_action === 'typing_on') {
      this.isTyping = true;
    } else {
      this.isTyping = false;
      this.sessionData = botmasterMessage.session;
      if (botmasterMessage.message.text.indexOf('LONGTAIL') >= 0) {
        this.addDiscoveryMessage(botmasterMessage);
      } else {
        if (botmasterMessage.message.text.indexOf('LOGINCRM') >= 0) {
          this.addLoginMessage(botmasterMessage);
        } else {
          if (botmasterMessage.message.text.indexOf('DISPLAYFORM') >= 0) {
            this.addDisplayForm(botmasterMessage);
          } else {
            if (botmasterMessage.message.text.indexOf('DISPLAYLOCATION') >= 0) {
              this.locations = botmasterMessage.session.context.location;
              console.log('Locations array from the backend: ');
              console.log(this.locations);
            } else {
              this.addTextMessage(botmasterMessage);
            }
          }
        }
      }
    }
  }

  private addTextMessage(botmasterMessage: BotmasterMessage): void {
    if (
      botmasterMessage.message.text.includes('Employee verified successfully')
    ) {
      this.messages.push({
        from: ChatMessageSender.Watson,
        continuous: this.lastMessage?.from === ChatMessageSender.Watson,
        quickReplies: botmasterMessage.message.quick_replies || null,
        feedback: !!botmasterMessage.message.feedback,
        text: 'Employee verification in progress.. ',
      });
      this.send('employee_success');
    }
    if (botmasterMessage.message.text.includes('Wrong employee information')) {
      this.messages.push({
        from: ChatMessageSender.Watson,
        continuous: this.lastMessage?.from === ChatMessageSender.Watson,
        quickReplies: botmasterMessage.message.quick_replies || null,
        feedback: !!botmasterMessage.message.feedback,
        text: 'Employee verification in progress..',
      });
      this.send('employee_fail');
    }
    if (botmasterMessage.message.text.includes('OTP verified successfully')) {
      this.messages.push({
        from: ChatMessageSender.Watson,
        continuous: this.lastMessage?.from === ChatMessageSender.Watson,
        quickReplies: botmasterMessage.message.quick_replies || null,
        feedback: !!botmasterMessage.message.feedback,
        text: 'OTP verification in progress.. ',
      });
      this.send('otp_success');
    }
    if (botmasterMessage.message.text.includes('OTP verification failed')) {
      this.messages.push({
        from: ChatMessageSender.Watson,
        continuous: this.lastMessage?.from === ChatMessageSender.Watson,
        quickReplies: botmasterMessage.message.quick_replies || null,
        feedback: !!botmasterMessage.message.feedback,
        text: 'OTP verification in progress.. ',
      });
      this.send('otp_fail');
    }

    if (botmasterMessage.message.text.includes('The form was submited')) {
      // disable form
    }

    this.messages.push({
      from: ChatMessageSender.Watson,
      continuous: this.lastMessage?.from === ChatMessageSender.Watson,
      quickReplies: botmasterMessage.message.quick_replies || null,
      feedback: !!botmasterMessage.message.feedback,
      text: botmasterMessage.message.text,
    });
  }

  private addDisplayForm(botmasterMessage: BotmasterMessage): void {
    // for aramco env

    this.field =
     botmasterMessage.session.context.formResults[0].formContent;


    //this.field =
    // botmasterMessage.session.context.formResults[0].formContent;

    this.field.fields
      .filter((f) => f.props.value === null || f.props.value === undefined)
      .forEach((element) => {
        element.props.value = '';
      });
    this.messages.push({
      from: ChatMessageSender.Watson,
      continuous: this.lastMessage?.from === ChatMessageSender.Watson,
      //quickReplies: botmasterMessage.message.quick_replies || null,
      feedback: !!botmasterMessage.message.feedback,
      //text: botmasterMessage.message.text,
      display_form: true,
      formBody: this.field,
      serviceID: botmasterMessage.session.context.formResults[0].scrnid,
    });
  }

  private addDiscoveryMessage(botmasterMessage: BotmasterMessage): void {
    this.messages.push({
      from: ChatMessageSender.Watson,
      continuous: false,
      feedback: false,
      discoveryResults: botmasterMessage.session.context.discoveryResults,
    });
  }

  private addLoginMessage(botmasterMessage: BotmasterMessage): void {
    //var update = this.loginService.loginLogic();
    var temp = this.loginService.loginLogic();
    temp.then((update) => {
      update.message.language = this.settingsService.locale;
      update.sessionId = this.sessionId;
      this.socket.send(update);
    });
    //this.send(update);
  }

  onSubmitForm(e) {
    // e.preventDefault();
    var all_cookies = LoginService.getUserCookies();
    all_cookies['FORM_DATA'] = {
      Service_Id: e.serviceID,
      Fields: e.values,
    };
    const update = {
      message: {
        text: 'form submission ... ',
        language: this.settingsService.locale,
      },
      sessionId: this.sessionId,
      user_cookies: all_cookies,
    };
    this.logger.log('Sending Websocket message', update);
    this.socket.send(update);
  }

  fetchLocation(): void {
    const allCookies = LoginService.getUserCookies();

    const update = {
      message: {
        text: 'display_user_location',
        language: this.settingsService.locale,
      },
      sessionId: this.sessionId,
      user_cookies: allCookies,
    };
    this.logger.log('Sending Websocket message', update);
    this.socket.send(update);
  }

  private checkCookie(cookieName: string): boolean {
    var name = cookieName + '=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return true;
      }
    }
    return false;
  }

  
}
