import { Injectable } from '@angular/core';
import { ChatMessage } from '../model';

@Injectable({
  providedIn: 'root'
})
export class SsmlService {

  constructor() { }

  public getSsmlForMessage(messages: Array<ChatMessage>): Array<string> {

    return messages.map(message => {
      var ssmlString = ""

      message.text = message.text ? message.text.replace(/(<([^>]+)>)/ig, "") : ""
      if(message.text) ssmlString += message.text

      if(message.quickReplies) {
        ssmlString += " <break time='300ms'/> "
        message.quickReplies.forEach(quickReply => {
          ssmlString += " <break time='300ms'/> " + quickReply.title.replace(/(<([^>]+)>)/ig, "")
        })
      }

      if(message.discoveryResults) {
        ssmlString += " <break time='300ms'/> "
        message.discoveryResults.forEach(discoveryResult => {
          ssmlString += " <break time='300ms'/> " + discoveryResult.title.replace(/(<([^>]+)>)/ig, "") + " <break time='300ms'/> " + discoveryResult.body.replace(/(<([^>]+)>)/ig, "") + " <break time='300ms'/> "
        })
      }

      return ssmlString
    })
  }
}
