import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { FeedbackService } from './feedback.service';
import { ChatService } from './chat.service';

class MockChatService {
  userId = 'u123';
  sessionData = { sess: 'data' };
}

describe('FeedbackService', () => {
  let service: FeedbackService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: ChatService, useClass: MockChatService }
      ]
    });
    service = TestBed.inject(FeedbackService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should send positive feedback', () => {
    service.sendPositiveFeedback('answer1234');
    const { request } = httpMock.expectOne('/feedback');
    expect(request.body).toEqual({
      feedback_type: 'POSITIVE',
      user_id: 'u123',
      message: { text: 'STORE_POSITIVE_FEEDBACK', longtail_answer_id: 'answer1234' },
      watsonUpdate: { sess: 'data' }
    });
  });

  it('should send negative feedback without details', () => {
    service.sendNegativeFeedback('answer1234');
    const { request } = httpMock.expectOne('/feedback');
    expect(request.body).toEqual({
      feedback_type: 'NEGATIVE',
      user_id: 'u123',
      message: { text: 'STORE_NEGATIVE_FEEDBACK answer1234', longtail_answer_id: 'answer1234' },
      watsonUpdate: { sess: 'data' }
    });
  });

  it('should send negative feedback with no answerID and details', () => {
    const details = {
      checkboxes: [{ value: 1, label: 'A', checked: true }, { value: 2, label: 'B', checked: false }],
      other_reason: 'Just for fun'
    };
    service.sendNegativeFeedback(undefined, details);
    const { request } = httpMock.expectOne('/feedback');
    expect(request.body).toEqual({
      feedback_type: 'NEGATIVE',
      user_id: 'u123',
      message: { text: 'STORE_NEGATIVE_FEEDBACK undefined', longtail_answer_id: undefined, ...details },
      watsonUpdate: { sess: 'data' }
    });
  });
});
