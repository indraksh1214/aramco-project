import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FeedbackDetails } from '../model';
import { Logger } from '../utilities/logger';
import { ChatService } from './chat.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  logger = Logger.getInstance();
  constructor(private http: HttpClient, private chatService: ChatService) { }

  sendPositiveFeedback(answerId?: string): void {
    this.saveFeedback({
      feedback_type: 'POSITIVE',
      user_id: this.chatService.userId,
      message: {
        text: 'STORE_POSITIVE_FEEDBACK',
        longtail_answer_id: answerId,
      },
      watsonUpdate: this.chatService.sessionData,
    });
  }

  sendNegativeFeedback(answerId?: string, details?: FeedbackDetails): void {
    this.saveFeedback({
      feedback_type: 'NEGATIVE',
      user_id: this.chatService.userId,
      message: {
        text: 'STORE_NEGATIVE_FEEDBACK ' + answerId,
        longtail_answer_id: answerId,
        ...details
      },
      watsonUpdate: this.chatService.sessionData,
    });
  }

  private saveFeedback(feedback: object): void {
    this.logger.log('Saving feedback', feedback);
    this.http.post('./feedback', feedback).subscribe((res) => {
      this.logger.log('Feedback stored', res);
    }, (error) => {
      this.logger.error('Error storing feedback', error);
    });
  }
}
