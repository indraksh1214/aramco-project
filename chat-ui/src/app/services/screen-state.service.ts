import { Injectable } from '@angular/core';
import { Logger } from '../utilities/logger';

@Injectable({
  providedIn: 'root',
})
export class ScreenStateService {
  logger = Logger.getInstance();
  constructor() {}
  allLetters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  varCondLinked = false;
  requestLoading = false;
  id;
  field;
  conditions;
  varCond = [];
  tempChanges = [];
  elementsForRemove = [];
  tempFields;
  tempConds;
  context;
  locationType = '';
  locationLoaded = false;
  fieldBp;

  loadJson(json, scrnid): void {
    const field = json.data['fields'];

    const conditions = json.data['conds'];
    this.id = scrnid;
    this.field = field;
    this.fieldBp = field;
    this.conditions = conditions;
    if (conditions != null) {
      this.runCondsInit();
    }
  }

  runCondsInit() {
    let cond, condid;
    let changeFound = false;
    this.conditions.forEach((element) => {
      cond = element['evalCond'];
      condid = element['condid'];
      var evalResult = this.evalCond(cond, condid);
      if (evalResult == null) return null;
      if (evalResult) {
        this.updateTempChanges(condid, 'ADD', element['actions']);
        element['actions'].forEach((e) => {
          if (e['prop'] == 'value') {
            this.updateFieldProp(e['fieldId'], e['prop'], e['value']);
            changeFound = true;
          }
        });
      } else {
        this.updateTempChanges(condid, 'REM', element['actions']);
        element['actions'].forEach((e) => {
          if (e['prop'] == 'value') {
            this.revertFieldProp(e['fieldId'], e['prop']);
            changeFound = true;
          }
        });
      }
    });
  }
  evalCond(cond, condid) {
    const a = this;
    let tempToken;
    let sepLoc;
    let soFar = cond;
    soFar = soFar.trim();
    let tokens = [];
    let operators = [];
    let value = [];

    let operPrec = {
      '<': 1,
      '>': 1,
      '<=': 1,
      '>=': 1,
      '=': 1,
      '<>': 1,
      AND: 0,
      OR: 0,
      and: 0,
      or: 0,
      ')': 0,
      '(': 1,
    };

    while (true) {
      soFar = soFar.trim();
      let left;
      let right;
      let tempResult;
      let oper;
      if (soFar == '') break;

      if (soFar[0] == "'") {
        sepLoc = soFar.indexOf("'", 1);
        tempToken = soFar.substring(1, sepLoc);
        tokens.push(tempToken);
        soFar = soFar.substring(sepLoc + 1);
        value.splice(0, 0, tempToken);
      } else {
        sepLoc = soFar.indexOf(' ');
        if (sepLoc < 0) {
          tempToken = soFar.substring(0, soFar.length);
        } else {
          tempToken = soFar.substring(0, sepLoc);
        }
        if (
          tempToken == '=' ||
          tempToken == '<' ||
          tempToken == '>' ||
          tempToken == '<>' ||
          tempToken == '<=' ||
          tempToken == 'AND' ||
          tempToken == 'OR' ||
          tempToken == 'or' ||
          tempToken == 'and' ||
          tempToken == '>='
        ) {
          if (
            operators.length > 0 &&
            operPrec[tempToken] < operPrec[operators[0]]
          ) {
            right = value[0];
            left = value[1];
            oper = operators[0];
            tempResult = this.evalResult(oper, left, right);
            if (tempResult == null) return null;
            value.splice(0, 1);
            value.splice(0, 1);
            operators.splice(0, 1);
            value.splice(0, 0, tempResult.toString());
          }
          operators.splice(0, 0, tempToken);

          tokens.push(tempToken);
        } else if (tempToken == '(') {
          operators.splice(0, 0, tempToken);
          tokens.push(tempToken);
        } else if (tempToken == ')') {
          while (true) {
            if (operators[0] == '(') {
              operators.splice(0, 1);
              break;
            }
            right = value[0];
            left = value[1];
            oper = operators[0];
            tempResult = this.evalResult(oper, left, right);
            if (tempResult == null) return null;
            value.splice(0, 1);
            value.splice(0, 1);
            operators.splice(0, 1);
            value.splice(0, 0, tempResult.toString());
          }
        } else if (this.allLetters.indexOf(tempToken[0]) < 0) {
          tokens.push(tempToken);
          value.splice(0, 0, tempToken);
        } else {
          var variableName = tempToken;
          tempToken = a.getFieldValue(tempToken);
          // if it is a variable ... lets link it to the condition for performance
          if (!this.varCondLinked) {
            if (this.varCond[variableName] != null) {
              if (!this.varCond[variableName].includes(condid))
                this.varCond[variableName].push(condid);
            } else {
              this.varCond[variableName] = [condid];
            }
          }

          value.splice(0, 0, tempToken);
          tokens.push(tempToken);
        }
        if (sepLoc < 0) break;
        soFar = soFar.substring(sepLoc);
      }
    }
    while (true) {
      if (operators.length == 0) break;
      var right = value[0];
      var left = value[1];
      var oper = operators[0];
      var tempResult = this.evalResult(oper, left, right);
      if (tempResult == null) return null;
      value.splice(0, 1);
      value.splice(0, 1);
      operators.splice(0, 1);
      value.splice(0, 0, tempResult.toString());
    }
    if (value[0] == 'true') return true;
    else return false;
  }

  getFieldValue(id) {
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id)
        return this.field[i]['props']['value'];
    }
    return null;
  }

  revertFieldProp(id, prop) {
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id) {
        this.field[i]['props'][prop] = this.fieldBp[i]['props'][prop];
      }
    }
  }

  evalResult(oper, left, right) {
    var tempResult = false;
    if (oper == 'AND' || oper == 'and') {
      tempResult = left == 'true' && right == 'true';
      return tempResult;
    }
    if (oper == 'OR' || oper == 'or') {
      tempResult = left == 'true' || right == 'true';
      return tempResult;
    }

    const re = /[A-Za-z]/g;
    if (left == null || right == null) return false;
    if (right.match(re) || right.match(re)) {
      let strComp = -1;
      let inList = false;
      if (Array.isArray(left)) {
        inList = left.includes(right);
      } else {
        strComp = left == right ? 0 : left <= right ? -1 : 1; //In dart > left.compareTo(right); IT IS IMPORTANT !
      }

      switch (oper) {
        case '=':
          if (strComp == 0) tempResult = true;
          else if (inList == true) tempResult = true;
          break;
        case '<':
          if (strComp < 0) tempResult = true;
          break;
        case '>':
          if (strComp > 0) tempResult = true;
          break;
        case '<>':
          if (strComp != 0) tempResult = true;
          break;
        case '<=':
          if (strComp <= 0) tempResult = true;
          break;
        case '>=':
          if (strComp >= 0) tempResult = true;
          break;
      }
    } else {
      let rightNum;
      let leftNum;
      try {
        rightNum = parseFloat(right);
      } catch (Expecption) {
        rightNum = null;
      }
      try {
        leftNum = parseFloat(left);
      } catch (Expecption) {
        leftNum = null;
      }
      if (rightNum == null || leftNum == null) {
        if (rightNum != null || (leftNum != null && oper == '<>')) return true;
        else return false;
      }
      switch (oper) {
        case '=':
          tempResult = leftNum == rightNum;
          break;
        case '<':
          tempResult = leftNum < rightNum;
          break;
        case '>':
          tempResult = leftNum > rightNum;
          break;
        case '<>':
          tempResult = leftNum != rightNum;
          break;
        case '<=':
          tempResult = leftNum <= rightNum;
          break;
        case '>=':
          tempResult = leftNum >= rightNum;
          break;
      }
    }
    return tempResult;
  }

  onFieldChange(fieldId, idx, newVal) {
    this.setFieldValue(fieldId, newVal, 'keyDomain');
    this.runVarConditions(fieldId);

    // if (location != null) {
    //   setLocation(id, location);
    // } else {
    //   setFieldValue(id, newVal, keyDomain);
    //   this.runVarConditions(id);
    // }
    // this.updateWidgetList();

    //TO DO: make this code general for all conds, not only for VPN access
    //TO DO: add this code to function

    const forInit = document.getElementsByClassName('forReInit');
    for (let i = 0; i < forInit.length; i++) {
      forInit[i].innerHTML = '';
    }
    this.elementsForRemove = [];
    if (this.tempChanges.length) {
      const fid = this.tempChanges[this.tempChanges.length - 1].actions[0]
        .fieldId;

      const message = this.field
        .find((f) => f.fieldId == fid)
        .props.domain?.find((d) => d.csticKey == fid).description;

      const messageSpan = document.getElementById(`${fid}-${idx}`);
      // messageSpan.innerHTML = message;
      this.logger.log(message, fid);
      return message;
    }
  }

  setFieldValue(id, val, keyDomain) {
    //String val = null;
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id) {
        if (this.field[i]['props']['screenType'] == 'Multi Select') {
          if (val == 'true') {
            if (this.field[i]['props']['value'] == null)
              this.field[i]['props']['value'] = [];
            this.field[i]['props']['value'].add(keyDomain);
          } else this.field[i]['props']['value'].remove(keyDomain);
        } else {
          this.field[i]['props']['value'] = val;
        }
      }
    }
  }

  runVarConditions(varId) {
    if (this.varCond[varId] != null)
      this.varCond[varId].forEach((element) => {
        this.runCondition(element);
      });
  }

  runCondition(condid) {
    let cond;
    let changeFound = false;

    for (let i = 0; i < this.conditions.length; i++) {
      if (this.conditions[i]['condid'] == condid) {
        let evalResult = this.evalCond(this.conditions[i]['evalCond'], condid); //TO DO: FIX debug and look here for bug
        if (evalResult == null) continue;
        if (evalResult) {
          this.updateTempChanges(condid, 'ADD', this.conditions[i]['actions']);
          this.conditions[i]['actions'].forEach((e) => {
            if (e['prop'] == 'value') {
              this.updateFieldProp(e['fieldId'], e['prop'], e['value']);
              changeFound = true;
            }
          });
        } else {
          this.updateTempChanges(condid, 'REM', this.conditions[i]['actions']);
          this.conditions[i]['actions'].forEach((e) => {
            if (e['prop'] == 'value') {
              this.revertFieldProp(e['fieldId'], e['prop']);
              changeFound = true;
            }
          });
        }
        break;
      }
    }

    return changeFound;
  }

  updateTempChanges(condid, action, tempActions) {
    if (action == 'ADD') {
      var temp = {
        condid: condid,
        actions: tempActions,
      };
      if (!this.tempChanges.includes(temp)) {
        this.tempChanges.push(temp);
      }
    } else {
      this.tempChanges = this.tempChanges.filter(
        (element) => element['condid'] != condid
      );
    }
  }

  updateFieldProp(id, prop, newVal) {
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id) {
        this.field[i]['props'][prop] = newVal;
        if (prop == 'value') {
          this.runVarConditions(id);
        }
      }
    }
  }
}
