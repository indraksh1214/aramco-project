/**
 * CHAT MESSAGES
 */

export enum ChatMessageSender {
  User = 'user',
  Watson = 'watson',
}

export interface QuickReply {
  title: string;
  payload?: string;
}

export interface DiscoveryResult {
  id: string;
  title: string;
  body: string;
  sourceUrl: string;
  document: string;
  page: string;
  passage: Passage;
}

export interface Passage{
  id: string,
  text: string
}

export interface ChatMessage {
  from: ChatMessageSender;
  continuous: boolean;
  feedback: boolean;
  text?: string;
  quickReplies?: QuickReply[];
  discoveryResults?: DiscoveryResult[];
  display_form?: boolean;
  formBody?: any;
  serviceID?: string;
  formConditions?: any;
}

/**
 * FEEDBACK
 */

export interface FeedbackCheckbox {
  value: number;
  label: string;
  checked: boolean;
}

export interface FeedbackDetails {
  checkboxes: FeedbackCheckbox[];
  other_reason: string;
}

/**
 * BOTMASTER FRAMEWORK
 */

export enum BotmasterSenderAction {
  TypingOn = 'typing_on',
}

export interface UserMessage {
  message: {
    text: string;
    language: string;
  };
  sessionId: string;
}

export interface BotmasterMessage {
  recipient: {
    id: string;
  };
  sender_action?: BotmasterSenderAction;
  session: any;
  sessionId?: string;
  message?: {
    text: string;
    quick_replies?: {
      content_type: 'text';
      title: string;
      payload: string;
    }[];
    feedback?: boolean;
  };
}
