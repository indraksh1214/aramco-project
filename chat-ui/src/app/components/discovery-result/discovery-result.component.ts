import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { DiscoveryResult } from 'src/app/model';
import { Logger } from 'src/app/utilities/logger';

const MAX_WORD_COUNT = 25;

@Component({
  selector: 'app-discovery-result',
  templateUrl: './discovery-result.component.html',
  styleUrls: ['./discovery-result.component.scss']
})
export class DiscoveryResultComponent implements OnInit {

  isCollapsed: boolean;
  feedbackSent = false;
  logger = Logger.getInstance();
  @Input()
  result: DiscoveryResult;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.isCollapsed = this.isLongText;
  }

  get isLongText(): boolean {    
    return this.result.body.split(' ').length > MAX_WORD_COUNT;
  }

  toggleCollapse($event): void {
    $event.preventDefault();
    this.isCollapsed = !this.isCollapsed;
  }
  
  viewPassage(){
    let headers = new HttpHeaders();
    headers.set('content-type', 'application/json');
    headers.set('X-Forwarded-Proto', 'https' );
    this.http.get('./api/page?id=' + this.result.passage.id + "&text=" + this.result.passage.text, {headers: headers}).subscribe(data => {
      const res = JSON.parse(JSON.stringify(data));
      let r_json = JSON.stringify(data)
      let obj = JSON.parse(r_json)
      this.logger.log("Passage data: ", obj);
      

      let page = unescape(obj.page)
      let passage = obj.passage
      
          let win = window.open("", '_blank',
          "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,fullscreen=no"
        );
        win.document.body.innerHTML = obj.title != 'undefined'? '<h1 style="margin-left:20px; margin-top: 40px ">'+obj.title+"</h1>": "";
      win.document.body.innerHTML += obj.subtitle != 'undefined' ? '<h2 style="margin-left: 20px ">'+obj.subtitle+"</h2>": "";
        win.document.body.innerHTML += page;
        this.logger.log("Passage windowBody : ", win.document.body);
        
        win.document.body.setAttribute("style",
          "background-image: linear-gradient(to bottom right,white,gray);max-width: 1080px; margin: auto auto !important; float: none !important; ");
        win.document.getElementsByTagName("section")[0].setAttribute("style",
          "padding:20px;background-color:white");
        win.document.title = obj.doc_number +" "
                              + obj.title + " " 
                              + obj.page_number;
        let elements = win.document.getElementsByClassName('text');
        
      for (var element of elements) {
        var text = element.textContent;
        if (passage == "") {
          break;
        }

        if (passage.includes(text)) {
          element.innerHTML = element.innerHTML.replace(text, "<mark style='background-color:#bcf1f5'>" + text + "</mark>")
          passage = passage.replace(text, "").trim();
        } else if (text.includes(passage)) {
          element.innerHTML = element.innerHTML.replace(passage, "<mark style='background-color:#bcf1f5'>" + passage + "</mark>")
          passage = ""
        } else {
          var sentences = passage.split(".")
          sentences.forEach(function (sentence, index) {
            if (text.includes(sentence)) {
              element.innerHTML = element.innerHTML.replace(sentence, "<mark style='background-color:#bcf1f5'>" + sentence + "</mark>")
              passage = passage.replace(sentence + ".", "").trim();
            }
          });
        }
      }
    })
    
  }
}
