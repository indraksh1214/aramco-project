import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { FeedbackCheckbox, FeedbackDetails } from 'src/app/model';
import { FeedbackService } from 'src/app/services/feedback.service';
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-regular-svg-icons';
import { ChatService } from 'src/app/services/chat.service';
import { environment } from "../../../environments/environment"


enum Choice {
  None = '',
  Positive = 'positive',
  Negative = 'negative'
}

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  faThumbsUp = faThumbsUp;
  faThumbsDown = faThumbsDown;
  showRemedyTicket = true;
  checkboxChecked = false;

  choice = Choice.None;
  sent = false;
  details: FeedbackDetails = {
    checkboxes: [
      { value: 1, label: '', checked: false },
      { value: 2, label: '', checked: false },
      { value: 3, label: '', checked: false }
    ],
    other_reason: ''
  };

  @Input()
  answerId: string;

  @Output()
  send = new EventEmitter<void>();

  @Output()
  contentChanged: EventEmitter<number> = new EventEmitter<number>(); 

  constructor(private feedbackService: FeedbackService, private translateService: TranslateService, private chatService: ChatService) {
  }

  ngOnInit(): void {
    for (let idx = 0; idx < this.details.checkboxes.length; idx++) {
      this.translateService.get(`feedback.negative.label${idx + 1}`).subscribe(label => {
        this.details.checkboxes[idx].label = label;
      });
    }
  }

  get clickedThumbsUp(): boolean {
    return this.choice === Choice.Positive;
  }

  get clickedThumbsDown(): boolean {
    return this.choice === Choice.Negative;
  }

  get showNegativeFeedbackBox(): boolean {
    return this.choice === Choice.Negative && !this.sent;
  }

  onThumbsUp(): void {
    if (this.choice !== Choice.None) {
      return;
    }
    this.choice = Choice.Positive;
    this.feedbackService.sendPositiveFeedback(this.answerId);
    this.sent = true;
    this.contentChanged.emit();
    this.send.emit();
  }

  onThumbsDown(): void {
    if (this.choice !== Choice.None) {
      return;
    }
    this.contentChanged.emit();
    this.choice = Choice.Negative;
  }

  onCheckboxChange($event, checkbox: FeedbackCheckbox): void {    
    // clear all radio
    this.details.checkboxes.map(el => el.checked = false) 
    
    const idx = this.details.checkboxes.findIndex(_ => _.value === checkbox.value);
    this.details.checkboxes[idx].checked = $event.target.checked;
    
    this.checkboxChecked = this.details.checkboxes.some(el => el.checked)
  }

  onOtherReasonChange($event): void {    
    this.details.other_reason = $event.target.value;
  }
  
  handleEnter($event): void{
    if($event.keyCode === 13)
      this.onSendNegative()
  }

  onSendNegative(): void {
    this.feedbackService.sendNegativeFeedback(this.answerId, this.details);
    this.sent = true;
    this.contentChanged.emit();
    this.send.emit();
  }

  ticketRequest($event): void {
    $event.preventDefault();
    this.chatService.sendOpenTicketRequest();
  }
}
