import {
  AfterViewInit,
  Component,
  ElementRef,
  Injectable,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { trigger, style, animate, transition } from '@angular/animations';
import {
  State as InputState,
  Transcript,
} from '../speech/input/input.component';
import { State as OutputState } from '../speech/output/output.component';
import { ChatService } from '../../services/chat.service';
import { SsmlService } from '../../services/ssml.service';
import { ChatMessage } from '../../model';
import { environment } from '../../../environments/environment';
import { faTelegramPlane } from '@fortawesome/free-brands-svg-icons';
import { faRedo } from '@fortawesome/free-solid-svg-icons';
import * as print from 'print-js';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('fadeOut', [
      transition(':leave', [
        style({ opacity: 1 }),
        animate('400ms ease-out', style({ opacity: 0 })),
      ]),
    ]),
  ],
})
@Injectable()
export class AppComponent implements OnInit, AfterViewInit {
  faTelegramPlane = faTelegramPlane;

  mobileWidget = environment.mobileWidget;
  speechInput = environment.speechInput;
  speechOutput = environment.speechOutput;

  showDisclaimer = true;
  chatInputText = '';
  state: InputState | OutputState = 'idle';
  transcript: string = '';
  showMenuBar = false;
  darkMode = false;
  textureOn = false;
  windowState: string = 'boxed';
  faRedo = faRedo;

  @ViewChild('chat', { static: false }) chatElement: ElementRef;
  @ViewChildren('message') messageElements: QueryList<any>;

  constructor(
    private chatService: ChatService,
    private ssmlService: SsmlService,
    private translateService: TranslateService
  ) {}

  ngOnInit(): void {
    if(window.sessionStorage.getItem("showDisclaimer")=="FALSE") this.showDisclaimer = false;
    if (!this.showDisclaimer && !this.mobileWidget) this.chatService.start();
  }

  ngAfterViewInit(): void {
    this.messageElements.changes.subscribe((_) => {
      this.scrollToChatBottom();
    });
  }

  get showTypingIndicator(): boolean {
    return this.chatService.isTyping;
  }

  get messages(): ChatMessage[] {
    return this.chatService.messages;
  }

  get ssmlFromMessages(): Array<string> {
    var messages = this.messages.map((a) => ({ ...a })).reverse();

    while (messages.length > 0 && messages[0].from === 'user') {
      messages.shift();
    }

    var lastWatsonOutput = [];

    for (let i = 0; messages[i] && messages[i].from === 'watson'; i++) {
      lastWatsonOutput.push(messages[i]);
    }

    lastWatsonOutput.reverse();

    return this.ssmlService.getSsmlForMessage(lastWatsonOutput);
  }

  onDismissDisclaimer(): void {
    this.showDisclaimer = false;
    window.sessionStorage.setItem("showDisclaimer","FALSE");
    this.chatService.start();
  }

  onChatInputTextChange($event): void {
    this.chatInputText = $event.target.value;
    if (this.chatInputText.length >= 150) {
      this.chatService.showLengthWarning();
    }
  }

  onSendChatText($event): void {
    $event.preventDefault();
    if (!this.chatInputText.trim()) {
      return;
    }
    this.chatService.sendMessage(this.chatInputText);
    this.chatInputText = '';
  }

  scrollToChatBottom(bottomOfChat?: boolean ,timeout?: number): void {
    setTimeout(() =>
      {
      var messages = this.messages.map((a) => ({ ...a })).reverse();
      let offset;
      
      if(messages.length <= 2){
        return
      }      

      if (bottomOfChat){
        this.chatElement.nativeElement.scroll({
          top: this.chatElement.nativeElement.scrollHeight,
          left: 0,
          behavior: 'smooth',
        })
        return;
      }
      var length = this.chatElement.nativeElement.childElementCount
      
     // scrolling for discovery actions
      if (messages[1].from == 'user' && messages[0].from == 'watson' && typeof messages[0].discoveryResults != undefined){
        offset = this.chatElement.nativeElement.children[length-2].offsetTop - 100
        this.chatElement.nativeElement.scroll({
          top: offset,
          left: 0,
          behavior: 'smooth',
        })
        // discovery results with a follow node
      } else if (messages[2].from == 'user' && messages[0].from == 'watson' && typeof messages[0].quickReplies != undefined){
        offset = this.chatElement.nativeElement.children[length - 2].offsetTop - 100
        this.chatElement.nativeElement.scroll({
          top: offset,
          left: 0,
          behavior: 'smooth',
        })
        //default scrolling at the bottom
      }else{
        this.chatElement.nativeElement.scroll({
          top: this.chatElement.nativeElement.scrollHeight,
          left: 0,
          behavior: 'smooth',
        })
      }
        
      },
        
      timeout || 0
    );
  }
  
  openSupport(): void{
    this.translateService.get(`feedback.support-link`).subscribe(url => {
      window.open(url, "_blank", "toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,fullscreen=no")

    });
  }

  changeState(state: InputState | OutputState): void {
    this.state = state;
  }

  displayMessage(transcript: Transcript): void {
    this.transcript = transcript.transcript;

    if (!transcript.isRecording && this.transcript) {
      this.chatService.sendMessage(this.transcript);
      this.transcript = '';
    }
  }

  printPDF(): void {
    print({
      printable: 'scrollingChat',
      type: 'html',
      css: 'assets/print.css',
      honorMarginPadding: false,
      scanStyles: false,
    });
  }

  changeWindowStateTo(state: string) {
    window.parent.postMessage({ state: state }, '*');
    if (state === 'closed') {
      this.windowState = 'boxed';
    } else {
      this.windowState = state;
    }
  }

  restart(): void {
    this.chatService.restartContext('START_ORCHESTRATOR');
    this.scrollRestartTop()
  }
  
  private scrollRestartTop(): void{
    console.log("Native Element: ", this.messageElements)
    
    // this.chatElement.nativeElement.lastElementChild.style.marginBottom = "338px"
    
    // var length = this.chatElement.nativeElement.childElementCount
   
    // this.chatElement.nativeElement.scroll({
    //   top: this.chatElement.nativeElement.clientHeight,
    //   left: 0,
    //   behavior: 'smooth',
    // })
    // this.chatElement.nativeElement.scroll({
    //   top: this.chatElement.nativeElement.scrollHeight,
    //   left: 0,
    //   behavior: 'smooth',
    // })
  }
}
