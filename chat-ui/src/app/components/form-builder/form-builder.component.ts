import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Action } from 'rxjs/internal/scheduler/Action';
import { ChatMessageSender } from 'src/app/model';
import { ChatService } from 'src/app/services/chat.service';
import { ScreenStateService } from 'src/app/services/screen-state.service';
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';
import { cloneDeep } from 'lodash';
import { Evaluator } from '../../utilities/evaluator';

@Component({
  selector: 'app-form-builder',
  templateUrl: './form-builder.component.html',
  styleUrls: ['./form-builder.component.scss'],
})
export class FormBuilderComponent implements OnInit, OnChanges {
  @Input() title: string;
  @Input() fields;
  @Input() serviceID;
  @Input() formConditions;
  localFields;
  @Output() formCanceled: EventEmitter<string> = new EventEmitter<string>();

  fieldIDs;
  conds;
  conditionValue = false;
  displayFormMessages;
  faExclamationTriangle = faExclamationTriangle;
  ///////////////////////////
  shortText = 'Short Text';
  longText = 'Long Text';
  singleSelect = 'Single Select';
  multiSelect = 'Multi Select';
  message = 'Message';
  formContent;
  screenState;
  locations;
  confirmLocation = false;
  locationId;
  selectedLocationId;
  constructor(private chatService: ChatService) {
    this.locations = this.chatService.locations.map((l) => {
      l.city = `${l.city}, ${l.building}, Floor: ${l.floor}`;
      return { city: l.city, locationId: l.locationId };
    });
  }

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.fields && this.fields && this.fields.fields) {
      this.localFields = cloneDeep(this.fields);
      console.warn(this.localFields);
      new Evaluator(this.localFields);
    }
  }
  messageDescription(messageElement): string {
    return messageElement.props.domain.find(
      (d) => d.value === messageElement.props.value
    ).description;
  }
  mapingFieldsIds(form): Array<object> {
    const values = this.fields.fields
      .filter((f) => f.props.screenType !== 'Message')
      .map((i) => i.fieldId)
      .map((id) => {
        if (Object.keys(form).indexOf(id) !== -1) {
          return { fieldId: id, Value: form[id] };
        }
      })
      .filter((fieldId) => fieldId !== undefined)
      .map((m) => {
        if (m.fieldId === this.locationId) {
          return { fieldId: m.fieldId, Value: this.selectedLocationId };
        } else {
          return m;
        }
      });
    return values;
  }
  onSubmitForm(form): boolean {
    if (form.valid) {
      const values = this.mapingFieldsIds(form.value);
      const formValues = {
        serviceID: this.fields.scrnid,
        values,
      };
      console.log(formValues);
      this.chatService.onSubmitForm(formValues);
      this.formCanceled.emit('cancel');
    }
    return false;
  }

  onCancelForm(): void {
    this.chatService.sendMessage('Cancel');
    this.formCanceled.emit('cancel');
  }

  hanldeCondition(value, id) {
    this.localFields.fields.find((f) => f.fieldId === id).props.value = value;

    const cloned = cloneDeep(this.fields);
    cloned.fields.forEach(
      (f, index) => (f.props.value = this.localFields.fields[index].props.value)
    );

    cloned.fields
      .filter((f) => f.props.screenType === 'Message')
      .forEach((f) => (f.props.value = ''));

    let newEval = new Evaluator(cloned);
    newEval.runVarConditions(id);
    this.localFields = cloned;
    console.log('final data', this.fields);
    newEval = undefined;
  }

  getLocationID(id): string {
    console.log(id);
    return (this.locationId = id);
  }
  handleLocations(value, id) {
    this.localFields.fields.map((f) => {
      if (f.props.screenType === 'ITlocation_extra') {
        f.props.visible = 'true';
        return f;
      }
    });
    this.locationId = id;
    this.selectedLocationId = value.locationId;
  }
}
