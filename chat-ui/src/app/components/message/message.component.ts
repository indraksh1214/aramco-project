import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ChatMessage, QuickReply } from 'src/app/model';
import { ChatService } from 'src/app/services/chat.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
})
export class MessageComponent implements OnInit {
  feedbackSent = false;
  formDisplay = true;

  @Input()
  message: ChatMessage;

  @Input()
  isLast: boolean;

  @Input()
  isFirst: boolean;

  @Output()
  contentChanged: EventEmitter<number> = new EventEmitter<number>();

  show: number = 5;
  optionSelected: boolean = false;
  cancelForm = false;

  constructor(private chatService: ChatService) {}

  ngOnInit(): void {}

  get hasTopSpace(): boolean {
    return this.message.from === 'user';
  }

  get hasSmallTopSpace(): boolean {
    return this.message.from === 'watson' && this.message.continuous;
  }

  get hasBotAvatar(): boolean {
    return this.message.from === 'watson' && !this.message.continuous;
  }

  get innerContainerClass(): string[] {
    return [`from-${this.message.from}`, this.isLast ? 'isLast' : '', 'top'];
  }

  onQuickReply($event, quickReply: QuickReply): void {
    $event.preventDefault();
    this.optionSelected = true;
    this.chatService.sendQuickReplyResponse(quickReply);
  }

  feedbackSentHandler() {
    this.contentChanged.emit();
  }
  handleCancelForm(event) {
    if (event === 'cancel') {
      this.cancelForm = true;
    }
  }
}
