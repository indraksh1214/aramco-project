import { ChangeDetectionStrategy, Component, EventEmitter, Input, NgZone, OnChanges, OnDestroy, OnInit, Output } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { faVolumeUp, faVolumeOff } from '@fortawesome/free-solid-svg-icons';
import { detect } from "detect-browser"
import { environment } from '../../../../environments/environment'

import synthesize from 'watson-speech/text-to-speech/synthesize'
import { Logger } from 'src/app/utilities/logger';

@Component({
  selector: 'app-speech-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpeechOutputComponent implements OnChanges, OnDestroy, OnInit {

  @Input() disabled: boolean = false
  @Input() autoPlay: boolean = false
  @Input() ssml: Array<string>
  @Output('state') state: EventEmitter<State> = new EventEmitter<State>()
  logger = Logger.getInstance();
  public readonly enabled: boolean = environment.speechOutput
  public supported: boolean = false
  public available: boolean = false

  private url: string
  private token: string
  private tokenInterval: NodeJS.Timeout
  private voice: string
  private customization: string

  public isSpeaking: boolean = false
  private text: Array<string> = []
  private audio: any

  public faVolumeUp = faVolumeUp
  public faVolumeOff = faVolumeOff

  constructor(private http: HttpClient, private ngZone: NgZone) {
    const browser = detect()
    switch (browser && browser.name) {
      case "chrome":
      case "edge-chromium":
      case "firefox":
        this.supported = true
        break
      default:
        this.logger.warn("Speech-Output: " + browser.name + " does not support IBM Watson Text-to-Speech!")
    }
  }

  ngOnInit() {
    if (this.enabled && this.supported) {
      this.getToken()
      this.tokenInterval = setInterval(() => this.getToken(), 1000 * 60 * 30)
    }
  }

  ngOnChanges() {
    if(JSON.stringify(this.ssml) === JSON.stringify(this.text)) return
    this.text = this.ssml
    if(this.autoPlay && !this.isSpeaking && !this.disabled && this.available) this.toggleSpeaking()
  }

  ngOnDestroy() {
    if (this.tokenInterval) clearInterval(this.tokenInterval)
  }

  public async toggleSpeaking(): Promise<void> {
    if (this.disabled) return

    if (this.isSpeaking) {
      this.isSpeaking = false
      this.state.emit("idle")
      this.audio.pause()
      this.audio.currentTime = 0
    } else {
      if (!this.text) {
        this.logger.error("Speech-Output: text input cannot be " + this.text + "!")
        return
      }

      if (!Array.isArray(this.text) && typeof this.text !== 'string') {
        this.logger.error("Speech-Output: text input cannot be of type " + typeof this.text + "! Must be string or an array of strings.")
        return
      }

      if (!Array.isArray(this.text)) this.text = [ this.text ]

      if (this.text.length === 0) return

      this.isSpeaking = true
      this.state.emit("speaking")

      let counter = 0

      const speak = async (array: Array<string>) => {
        for (const item of array) {
          counter++;
          if (typeof item !== 'string') {
            this.logger.warn("Speech-Output: Array items cannot be of type " + typeof item + "! Must be type string. Current item will be skipped!")
            this.logger.warn(item)
          } else if (item.length === 0) {
            this.logger.warn("Speech-Output: Array item cannot be an empty string. Current item will be skipped!")
          } else {
            await new Promise(resolve => {
              this.audio = synthesize({
                text: item,
                voice: this.voice,
                url: this.url,
                accessToken: this.token,
                customizationId: this.customization ? this.customization : undefined,
                autoPlay: false,
              } as SynthesizeOptions)

              this.audio.onended = () => {
                resolve()
              }

              this.audio.oncanplaythrough = () => {
                if (this.isSpeaking) this.audio.play()
              }
            })
          }
        }
        if(counter < this.text.length) await speak(this.text.slice(counter))
      }

      await speak(this.text)

      this.toggleSpeaking()
    }
  }

  private getToken(): void {
    this.http.get("./api/text-to-speech/properties").subscribe((properties: Properties) => {
      this.logger.log("Speech-Output: Access token renewed.")
      this.available = true
      this.token = properties.access_token
      this.url = properties.url
      this.voice = properties.voice
      this.customization = properties.customization
    }, (err) => {
      this.logger.error("Speech-Output: " + err)
      this.available = false
    })
  }
}

interface Properties {
  access_token: string
  url: string
  voice: string
  customization: string
}

interface SynthesizeOptions {
  url: string
  token: string
  accessToken: string
  text: string
  voice: string
  customizationId?: string
  accept?: string
  xWatsonLearningOptOut?: number
  autoPlay?: boolean
  element?: any
}

export type State = "idle" | "speaking"
