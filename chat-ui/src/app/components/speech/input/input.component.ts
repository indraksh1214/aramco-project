import { Component, EventEmitter, Input, NgZone, OnDestroy, OnInit, Output } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { faMicrophone, faMicrophoneSlash } from '@fortawesome/free-solid-svg-icons';
import { detect } from "detect-browser"
import { environment } from '../../../../environments/environment'

import recognizeMicrophone from "watson-speech/speech-to-text/recognize-microphone"
import { Logger } from 'src/app/utilities/logger';

@Component({
  selector: 'app-speech-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class SpeechInputComponent implements OnDestroy, OnInit {

  @Input() disabled: boolean = false
  @Output('state') state: EventEmitter<State> = new EventEmitter<State>()
  @Output('transcript') transcript: EventEmitter<Transcript> = new EventEmitter<Transcript>()
  logger = Logger.getInstance();
  public readonly enabled: boolean = environment.speechInput
  public supported: boolean = false
  public available: boolean = false

  private url: string
  private token: string
  private tokenInterval: NodeJS.Timeout
  private model: string
  private languageCustomization: string
  private acousticCustomization: string

  public isRecording: boolean = false
  private stream: any
  private jointResult: string = ""
  private result: Array<string> = []

  public faMicrophone = faMicrophone;
  public faMicrophoneSlash = faMicrophoneSlash;

  constructor(private http: HttpClient, private ngZone: NgZone) {
    const browser = detect()
    switch (browser && browser.name) {
      case "chrome":
      case "edge-chromium":
      case "firefox":
        this.supported = true
        break
      default:
        this.logger.warn("Speech-Input: " + browser.name + " does not support IBM Watson Speech-to-Text!")
    }

    if (!window.isSecureContext) {
      this.supported = false
      this.logger.warn("Speech-Input: IBM Watson Speech-to-Text is only available in a secure context!")
    }
  }

  ngOnInit() {
    if (this.enabled && this.supported) {
      this.getToken()
      this.tokenInterval = setInterval(() => this.getToken(), 1000 * 60 * 30)
    }
  }

  ngOnDestroy() {
    if (this.tokenInterval) clearInterval(this.tokenInterval)
  }

  public toggleRecording(): void {
    if (this.disabled) return

    if (this.isRecording) {
      this.stopStream()
    } else {
      this.isRecording = true
      this.jointResult = ""
      this.result = []
      this.startStream()
    }
  }

  private startStream(): void {
    this.state.emit("recording")
    this.stream = recognizeMicrophone(this.getOptions())
    this.ngZone.runOutsideAngular(() => {
      this.stream.on("data", (data) => {
        this.ngZone.run(() => {
          if (data.results[0] && data.results[0].final) {
            this.result.push(data.results[0].alternatives[0].transcript)
            this.jointResult = this.result.join(" ")
          } else {
            this.jointResult = this.result.join(" ") + " " + (data.results[0] ? data.results[0].alternatives[0].transcript : "")
          }
          this.transcript.emit({
            transcript: this.jointResult,
            isRecording: true
          })
        })
      })
      this.stream.on("error", (err) => {
        this.ngZone.run(() => {
          var errMsg = err.toString()
          if (errMsg.startsWith("Error: No speech detected for ")) {
            this.toggleRecording()
          } else {
            if (errMsg.startsWith("Error: could not detect endianness after ")) {
              this.logger.warn("Speech-Input: No speech input received!")
            } else {
              this.logger.error("Speech-Input: " + err)
            }
            this.isRecording = false
            this.jointResult = ""
            this.result = []
            this.state.emit("idle")
          }
        })
      })
    })
  }

  private stopStream(): void {
    if (this.stream) {
      this.stream.stop()
    }
    if (this.jointResult.length > 0) {
      this.transcript.emit({
        transcript: this.jointResult,
        isRecording: false
      })
    }
    this.isRecording = false
    this.state.emit("idle")
  }

  private getToken(): void {
    this.http.get("./api/speech-to-text/properties").subscribe((properties: Properties) => {
      this.logger.log("Speech-Input: Access token renewed.")
      this.available = true
      this.token = properties.access_token
      this.url = properties.url
      this.model = properties.model
      this.languageCustomization = properties.language_customization
      this.acousticCustomization = properties.acoustic_customization
    }, (err) => {
      this.logger.error("Speech-Input: " + err)
      this.available = false
    })
  }

  private getOptions(): RecognizeStream {
    var options: RecognizeStream = {
      accessToken: this.token,
      url: this.url,
      model: this.model,
      smartFormatting: true,
      wordConfidence: false,
      objectMode: true,
      interimResults: true,
      inactivityTimeout: 3
    }

    if (this.languageCustomization) options.languageCustomizationId = this.languageCustomization
    if (this.acousticCustomization) options.acousticCustomizationId = this.acousticCustomization
    
    return options
  }
}

interface Properties {
  access_token: string
  url: string
  model: string
  language_customization: string
  acoustic_customization: string

}

interface RecognizeStream {
  url: string
  readableObjectMode?: boolean
  objectMode?: boolean
  accessToken: string
  model: string
  languageCustomizationId?: string
  acousticCustomizationId?: string
  baseModelVersion?: string
  xWatsonLearningOptOut?: boolean
  xWatsonMetadata?: string
  contentType?: string
  customizationWeight?: number
  inactivityTimeout?: number
  interimResults?: boolean
  keywords?: Array<string>
  keywordsThreshold?: number
  maxAlternatives?: number
  wordAlternativesThreshold?: number
  wordConfidence?: boolean
  timestamps?: boolean
  profanityFilter?: boolean
  smartFormatting?: boolean
  speakerLabels?: boolean
  grammarName?: string
  redaction?: boolean
  processingMetrics?: boolean
  processingMetricsInterval?: number
  audioMetrics?: boolean
}

export interface Transcript {
  transcript: string
  isRecording: boolean
}

export type State = "idle" | "recording"
