import { environment } from "src/environments/environment";

export class Logger {
    private static instance: Logger;
    constructor() {
        if(!environment.showLogs) {

            this.log = () => {};
            this.warn = ()=> {};
            this.debug = () => {};
        }
    }

    public static getInstance(): Logger {
        if(!this.instance) {
            this.instance = new Logger();
         
        }

        return this.instance;
    }

    public log(...args) {
        console.log(...args)
    }

    public warn(...args) {
        console.warn(...args)
    }

    public error(...args) {
        console.error(...args)
    }

    public debug(...args) {
        console.debug(...args)
    }
}

