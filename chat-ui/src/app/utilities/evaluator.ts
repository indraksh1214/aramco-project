import { Logger } from 'src/app/utilities/logger';
export interface FormSchema {
  scrnid: string;
  fields: {
    fieldId: string;
    props: {
      required: boolean;
      datatype: string;
      readOnly: boolean;
      length: number;
      multipleValues: boolean;
      allowsOtherValues: boolean;
      displayAllOptions: boolean;
      screenType: string;
      visible: boolean;
      label: string;
    };
  }[];
  conds: {
    condid: string;
    evalCond: string;
    actions: {
      fieldId: string;
      prop: string;
      value: string;
    }[];
  }[];
}
const allLetters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
export class Evaluator {
  logger: Logger = Logger.getInstance();

  varCondLinked = false;

  requestLoading = false;

  id: string;
  formKey: any;
  curWidgets = [];
  form;
  //List<Field> fields = new List();
  field;
  //    fieldBp;
  onChange: Function;

  conditions = [];

  varCond = {};

  tempChanges = [];

  tempFields;
  tempConds;
  context;
  locationType = '';
  locationLoaded = false;
  //Field tempF;

  constructor(jsonObject) {
    this.id = jsonObject['scrnid'];
    /*tempFields = json['fields'] as List; 
    for(int i = 0 ; i<tempFields.length; i++ ){
      tempF = Field.fromJSON(tempFields[i]);
      fields.push(tempF);
    }*/
    this.field = jsonObject['fields'];

    this.conditions = jsonObject['conds'];

    for (let i = 0; i < this.field.length; i++) {
      if (
        this.field[i]['props']['screenType'].toString().startsWith('ITlocation')
      ) {
        this.locationType = 'IT';
      } else if (
        this.field[i]['props']['screenType'].toString().startsWith('TLlocation')
      ) {
        this.locationType = 'TL';
      }
    }
    if (this.conditions) this.runCondsInit();
  }

  getFieldValue(id: string) {
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id)
        return this.field[i]['props']['value'];
    }
    return null;
  }

  setFieldValue(id: string, val: string, { keyDomain }) {
    //String val = null;
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id) {
        if (this.field[i]['props']['screenType'] == 'Multi Select') {
          if (val == 'true') {
            if (this.field[i]['props']['value'] == null)
              this.field[i]['props']['value'] = [];
            this.field[i]['props']['value'].push(keyDomain);
          } else this.field[i]['props']['value'].remove(keyDomain);
        } else {
          this.field[i]['props']['value'] = val;
        }
      }
    }
  }

  updateFieldProp(id: string, prop: string, newVal: string) {
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id) {
        this.field[i]['props'][prop] = newVal;
        if (prop == 'value') {
          this.runVarConditions(id);
        }
      }
    }
  }

  onFieldChange(id: string, newVal: string, { keyDomain, location }) {
    if (location != null) {
      this.setLocation(id, location);
    } else {
      this.setFieldValue(id, newVal, keyDomain);

      this.runVarConditions(id);
    }
  }

  setLocation(id: string, location) {
    var mainLocation = id;
    var screenType = '';
    var extraId = '';
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['fieldId'] == id) {
        this.field[i]['props']['currentLoc'] = location;
        screenType = this.field[i]['props']['screenType'];
        break;
      }
    }
    screenType = screenType + '_extra';
    for (let i = 0; i < this.field.length; i++) {
      if (this.field[i]['props']['screenType'] == screenType) {
        extraId = this.field[i]['fieldId'];
        break;
      }
    }

    this.setFieldValue(id, location.location.locationId.toString(), null);
    this.setFieldValue(extraId, location.extraInfo, null);
  }

  updateTempChanges(condid: string, action: string, tempActions: any[]) {
    if (action == 'ADD') {
      var temp = {
        condid: condid,
        actions: tempActions,
      };
      if (!this.tempChanges.includes(temp)) {
        this.tempChanges.push(temp);
      }
    } else {
      this.tempChanges = this.tempChanges.filter(
        (element) => element['condid'] == condid
      );
    }
    // updateFieldProp(e['fieldId'], e['prop'], e['value']);
  }

  runCondsInit(): boolean {
    let cond: string, condid;
    let changeFound = false;
    this.conditions.forEach((element) => {
      cond = element['evalCond'];
      condid = element['condid'];
      let evalResult = this.evalCond(cond, condid);
      if (evalResult == null) return null;
      if (evalResult) {
        this.updateTempChanges(condid, 'ADD', element['actions']);

        element['actions'].forEach((e) => {
          this.updateFieldProp(e['fieldId'], e['prop'], e['value']);

          if (e['prop'] == 'value') {
            changeFound = true;
          }
        });
      } else {
        this.updateTempChanges(condid, 'REM', element['actions']);

        element['actions'].forEach((e) => {
          if (e['prop'] == 'value') {
            changeFound = true;
          }
        });
      }
    });
    this.varCondLinked = true;
    return changeFound;
  }

  runVarConditions(varId: string) {
    this.logger.log('Getting varConditions for ', varId);
    if (this.varCond[varId] != null) {
      this.varCond[varId].forEach((element) => {
        this.runCondition(element);
      });
    } else {
      this.logger.log('No varConditions found for ', varId);
    }
  }

  runCondition(condid: string): boolean {
    let cond: string;
    let changeFound = false;

    for (let i = 0; i < this.conditions.length; i++) {
      if (this.conditions[i]['condid'] == condid) {
        this.logger.log(`running condition ${condid}`);
        var evalResult = this.evalCond(this.conditions[i]['evalCond'], condid);
        this.logger.log('the condition is ', this.conditions[i]['evalCond']);
        this.logger.log('the condition result is ', evalResult);
        if (evalResult == null) continue;
        if (evalResult) {
          this.updateTempChanges(condid, 'ADD', this.conditions[i]['actions']);
          this.conditions[i]['actions'].forEach((e) => {
            this.logger.log(`Updating ${e['prop']} of `, e['fieldId']);

            this.updateFieldProp(e['fieldId'], e['prop'], e['value']);
            if (e['prop'] == 'value') {
              changeFound = true;
            }
          });
        } else {
          this.updateTempChanges(condid, 'REM', this.conditions[i]['actions']);
          this.conditions[i]['actions'].forEach((e) => {
            if (e['prop'] == 'value') {
              changeFound = true;
            }
          });
        }
        break;
      }
    }

    return changeFound;
  }

  evalResult(oper: string, left: string | string[], right: string): boolean {
    var tempResult = false;
    if (oper == 'AND' || oper == 'and') {
      tempResult = left == 'true' && right == 'true';
      return tempResult;
    }
    if (oper == 'OR' || oper == 'or') {
      tempResult = left == 'true' || right == 'true';
      return tempResult;
    }

    if (left == null || right == null) return false;
    if (
      new RegExp(/[A-Za-z]/g).test(right) ||
      new RegExp(/[A-Za-z]/g).test(right)
    ) {
      let strComp: number = -1;
      let inList = false;
      if (Array.isArray(left)) {
        inList = left.includes(right);
      } else {
        strComp = left.localeCompare(right);
      }

      switch (oper) {
        case '=':
          if (strComp == 0) tempResult = true;
          else if (inList == true) tempResult = true;
          break;
        case '<':
          if (strComp < 0) tempResult = true;
          break;
        case '>':
          if (strComp > 0) tempResult = true;
          break;
        case '<>':
          if (strComp != 0) tempResult = true;
          break;
        case '<=':
          if (strComp <= 0) tempResult = true;
          break;
        case '>=':
          if (strComp >= 0) tempResult = true;
          break;
      }
    } else {
      let rightNum: number;
      let leftNum: number;
      try {
        rightNum = Number.parseFloat(right);
      } catch (Expecption) {
        rightNum = null;
      }
      try {
        leftNum = Number.parseFloat(left as string);
      } catch (Expecption) {
        leftNum = null;
      }
      if (rightNum == null || leftNum == null) {
        if (rightNum != null || (leftNum != null && oper == '<>')) return true;
        else return false;
      }
      switch (oper) {
        case '=':
          tempResult = leftNum == rightNum;
          break;
        case '<':
          tempResult = leftNum < rightNum;
          break;
        case '>':
          tempResult = leftNum > rightNum;
          break;
        case '<>':
          tempResult = leftNum != rightNum;
          break;
        case '<=':
          tempResult = leftNum <= rightNum;
          break;
        case '>=':
          tempResult = leftNum >= rightNum;
          break;
      }
    }
    return tempResult;
  }

  evalCond(cond: string, condid: string) {
    cond = cond.split('(').join(' ( ');
    cond = cond.split(')').join(' ) ');
    cond = cond.split('=').join(' = ');
    let tempToken;
    let sepLoc: number;
    let soFar: string = cond;
    soFar = soFar.trim();

    let tokens = [];
    let operators = [];
    let value = [];
    let operPrec = {
      '<': 1,
      '>': 1,
      '<=': 1,
      '>=': 1,
      '=': 1,
      '<>': 1,
      AND: 0,
      OR: 0,
      and: 0,
      or: 0,
      ')': 0,
      '(': 1,
    };

    while (true) {
      soFar = soFar.trim();
      let left;
      let right;
      let tempResult: boolean;
      let oper;
      if (soFar == '') break;

      if (soFar[0] == "'") {
        sepLoc = soFar.indexOf("'", 1);
        tempToken = soFar.substring(1, sepLoc);
        tokens.push(tempToken);
        soFar = soFar.substring(sepLoc + 1);
        value.unshift(tempToken);
      } else {
        sepLoc = soFar.indexOf(' ');
        if (sepLoc < 0) {
          tempToken = soFar.substring(0, soFar.length);
        } else {
          tempToken = soFar.substring(0, sepLoc);
        }
        if (
          tempToken == '=' ||
          tempToken == '<' ||
          tempToken == '>' ||
          tempToken == '<>' ||
          tempToken == '<=' ||
          tempToken == 'AND' ||
          tempToken == 'OR' ||
          tempToken == 'or' ||
          tempToken == 'and' ||
          tempToken == '>='
        ) {
          if (
            operators.length > 0 &&
            operPrec[tempToken] < operPrec[operators[0]]
          ) {
            right = value[0];
            left = value[1];
            oper = operators[0];
            tempResult = this.evalResult(oper, left, right);
            if (tempResult == null) return null;
            value.shift();
            value.shift();
            operators.shift();
            value.unshift(tempResult.toString());
          }
          operators.unshift(tempToken);

          tokens.push(tempToken);
        } else if (tempToken == '(') {
          operators.unshift(tempToken);
          tokens.push(tempToken);
        } else if (tempToken == ')') {
          while (true) {
            if (operators[0] == '(') {
              operators.shift();
              break;
            }
            right = value[0];
            left = value[1];
            oper = operators[0];
            tempResult = this.evalResult(oper, left, right);
            if (tempResult == null) return null;
            value.shift();
            value.shift();
            operators.shift();
            value.unshift(tempResult.toString());
          }
        } else if (allLetters.indexOf(tempToken[0]) < 0) {
          tokens.push(tempToken);
          value.unshift(tempToken);
        } else {
          var variableName = tempToken;
          tempToken = this.getFieldValue(tempToken);
          // if it is a variable ... lets link it to the condition for performance
          if (!this.varCondLinked) {
            if (this.varCond[variableName]) {
              if (!this.varCond[variableName].includes(condid))
                this.varCond[variableName].push(condid);
            } else {
              this.varCond[variableName] = [condid];
            }
          }

          value.unshift(tempToken);
          tokens.push(tempToken);
        }
        if (sepLoc < 0) break;
        soFar = soFar.substring(sepLoc);
      }
    }
    while (true) {
      if (operators.length == 0) break;
      var right = value[0];
      var left = value[1];
      var oper = operators[0];
      var tempResult = this.evalResult(oper, left, right);
      if (tempResult == null) return null;
      value.shift();
      value.shift();
      operators.shift();
      value.unshift(tempResult.toString());
    }
    this.logger.log(tokens);
    if (value[0] == 'true') return true;
    else return false;
  }

  getFieldProp(fieldId: string, prop: string) {
    let i: number;
    let j: number;
    let actions = [];
    for (let i = 0; i < this.tempChanges.length; i++) {
      actions = this.tempChanges[i]['actions'];
      for (j = 0; j < actions.length; j++) {
        if (actions[j]['fieldId'] == fieldId && actions[j]['prop'] == prop)
          return actions[j]['value'];
        // field[i]['props'][prop] = fieldBp[i]['props'][prop];
      }
    }
    return null;
  }
}
