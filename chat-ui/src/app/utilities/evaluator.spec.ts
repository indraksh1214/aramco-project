
import { Evaluator } from './evaluator';

describe('Evaluator', () => {
 

  it('should evaluate condition', () => {
    const data = {
        "scrnid": "ITC_ML_VPN",
        "fields": [{
          "fieldId": "ACT",
          "props": {
            "required": "true",
            "datatype": "SYM",
            "readOnly": "false",
            "length": "30 ",
            "multipleValues": "false",
            "allowsOtherValues": "false",
            "displayAllOptions": "true",
            "screenType": "Single Select",
            "visible": "true",
            "label": "Action Required",
            "domain": [{
              "csticKey": "ACT",
              "value": "GRANT",
              "description": "Grant Access"
            }, {
              "csticKey": "ACT",
              "value": "REV",
              "description": "Revoke Access"
            }]
          }
        }, {
          "fieldId": "UIMESSAGE_WARN",
          "props": {
            "required": "false",
            "datatype": "SYM",
            "readOnly": "false",
            "length": "30 ",
            "multipleValues": "false",
            "allowsOtherValues": "false",
            "displayAllOptions": "false",
            "screenType": "Message",
            "visible": "true",
            "label": "Warning:",
            "domain": [{
              "csticKey": "UIMESSAGE_WARN",
              "value": "W_NOTE",
              "description": "Full VPN access requires later image containing latest Operating System (i.e. Windows 10), latest security software and Pulse Secure client version. Please contact IT HELP DESK to request re-imaging your device."
            }]
          }
        }, {
          "fieldId": "//ZTEXTJUSTIFICATION/STRUCT.CONC_LINES",
          "props": {
            "required": "true",
            "datatype": "SYM",
            "readOnly": "false",
            "length": "1000 ",
            "multipleValues": "false",
            "allowsOtherValues": "false",
            "displayAllOptions": "false",
            "screenType": "Long Text",
            "visible": "true",
            "label": "Justification"
          }
        }, {
          "fieldId": "//ZTEXTADDITIONALINFO/STRUCT.CONC_LINES",
          "props": {
            "required": "false",
            "datatype": "SYM",
            "readOnly": "false",
            "length": "1000 ",
            "multipleValues": "false",
            "allowsOtherValues": "false",
            "displayAllOptions": "false",
            "screenType": "Long Text",
            "visible": "true",
            "label": "Additional Information"
          }
        }],
        "conds": [{
          "condid": "FORM_WARNING",
          "evalCond": "ACT = 'GRANT'",
          "actions": [{
            "fieldId": "UIMESSAGE_WARN",
            "prop": "value",
            "value": "W_NOTE"
          }]
        }, {
          "condid": "VIEW_MODE",
          "evalCond": "'INIT' <> 'INIT'",
          "actions": [{
            "fieldId": "ACT",
            "prop": "readOnly",
            "value": "true"
          }]
        }]
      };
      const evaluator = new Evaluator(data);
      data.fields.find(f => f.fieldId === 'ACT').props["value"] = "GRANT";
      evaluator.runVarConditions("ACT");
      console.log("DATA CHANGE:", data);
      expect(data.fields.find(f => f.fieldId === 'UIMESSAGE_WARN').props["value"]).toEqual("W_NOTE");
  });
});
