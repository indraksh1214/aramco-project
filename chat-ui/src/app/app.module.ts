import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './components/app/app.component';
import { FeedbackComponent } from './components/feedback/feedback.component';
import { MessageComponent } from './components/message/message.component';
import { DiscoveryResultComponent } from './components/discovery-result/discovery-result.component';
import { SpeechInputComponent } from './components/speech/input/input.component';
import { SpeechOutputComponent } from './components/speech/output/output.component';
import { FormBuilderComponent } from './components/form-builder/form-builder.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

function createTranslateLoader(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    FeedbackComponent,
    MessageComponent,
    DiscoveryResultComponent,
    SpeechInputComponent,
    SpeechOutputComponent,
    FormBuilderComponent,
    SafeHtmlPipe,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient],
      },
      defaultLanguage: 'en',
    }),
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
