// PLEASE PROVIDE THE URL TO THE CHATBOT WITHOUT A TRAILING SLASH (/)
//URL_TO_CHATBOT = "http://localhost:6011";             // for local deployments 
//URL_TO_CHATBOT = "https://cbd.aramco.com.sa/myit"; // for aramco infrastructure
//URL_TO_CHATBOT = "http://10.150.20.122:6011"; // for TEC environment

//TOOLTIP_TITLE= "Hello! I am your Engineering Virtual Assistant"; // Engineering
TOOLTIP_TITLE= "Hello! I am the A'AMER"; // 


// DO NOT MODIFY FROM HERE
setTimeout(function () {
  //get references to DOM
  const doc = document || window.document
  const head = doc.head || doc.getElementsByName('head')[0]
  const body = doc.body || doc.getElementsByName('body')[0]

  //add styles to the documents head
  const styleElement = doc.createElement('style')
  styleElement.appendChild(doc.createTextNode(getStyles()))

  //define a container element
  const container = doc.createElement("div")
  container.classList.add("accelerator-integration")

  //define chat bubble
  const bubble = doc.createElement("div")
  bubble.classList.add("chat-bubble")

  //define tooltip
  const tooltipContainer = doc.createElement("div")
  tooltipContainer.classList.add("chat-tooltip-container")

  const tooltip = doc.createElement("div")
  tooltip.classList.add("chat-tooltip");
  tooltip.innerHTML = TOOLTIP_TITLE;
  tooltipContainer.appendChild(tooltip)

  //define iframe (wrapper for chat application)
  const iframe = doc.createElement("iframe")
  iframe.setAttribute("src", "https://cbd.aramco.com.sa/myit/")
  iframe.classList.add("chat-window")
  iframe.classList.add("chat-window-closed")

  //add event listeners for iframe
  window.addEventListener('message', (event) => {
    if(event.data && event.data == "IBM_SAML"){
      sessionStorage.setItem("IBM_SAML", "FALSE");
    }
    if (event.data) {
      const state = event.data.state
      console.log("Received state: " + state)

      if (state === "closed") {
        container.classList.remove("accelerator-integration-fullscreen")

        bubble.classList.remove("accelerator-item-hidden")

        tooltipContainer.classList.remove("accelerator-item-hidden")

        iframe.classList.add("chat-window-closed")
        iframe.classList.remove("chat-window-boxed")
        iframe.classList.remove("chat-window-fullscreen")
      } else if (state === "boxed") {
        container.classList.remove("accelerator-integration-fullscreen")

        iframe.classList.add("chat-window-boxed")
        iframe.classList.remove("chat-window-fullscreen")
      } else if (state === "fullscreen") {
        container.classList.add("accelerator-integration-fullscreen")

        iframe.classList.add("chat-window-fullscreen")
        iframe.classList.remove("chat-window-boxed")
      }
    }
  }, false)

  //add actions
  bubble.onmouseenter = () => {
    setTimeout(() => {
      tooltipContainer.classList.add("accelerator-item-visible")
      tooltipContainer.classList.remove("accelerator-item-hidden")
    }, 100)
  }

  bubble.onmouseleave = () => {
    setTimeout(() => {
      tooltipContainer.classList.add("accelerator-item-hidden")
      tooltipContainer.classList.remove("accelerator-item-visible")
    }, 0)
  }

  bubble.onclick = () => {
    if(sessionStorage.getItem("IBM_SAML") != "TRUE"){
      sessionStorage.setItem("IBM_SAML", "TRUE");
      let handler = window.open("https://cbd.aramco.com.sa/myit/login", "Login Window", "width=50, height=50"); 
      var timer = setInterval(function() { 
        if(handler.closed) {
            clearInterval(timer);
            bubble.click();
        }
    }, 1000);
      return; 
    }

    container.appendChild(iframe)

    bubble.classList.add("accelerator-item-hidden")
    bubble.classList.remove("accelerator-item-visible")

    tooltipContainer.classList.add("accelerator-item-hidden")
    tooltipContainer.classList.remove("accelerator-item-visible")

    iframe.classList.add("chat-window-boxed")
    iframe.classList.remove("chat-window-closed")

    setTimeout(() => {
      tooltipContainer.classList.add("accelerator-item-hidden")
      tooltipContainer.classList.remove("accelerator-item-visible")
    }, 500)
  }

  //add elements to DOM
  head.appendChild(styleElement)

  container.appendChild(bubble)
  container.appendChild(tooltipContainer)

  body.appendChild(container);
}, 400)

function getStyles() {
  return `
    @import url('https://cbd.aramco.com.sa/myit/assets/trebuc.woff');

    .accelerator-integration {
      position: fixed;
      bottom: 30px;
      right: 55px;
      z-index:100;
      display: flex;
      flex-direction: row-reverse;
      align-items: flex-end;
    }

    .accelerator-integration-fullscreen {
      bottom: 0;
      right: 0;
      width: 100%;
      height: 100%;
    }

    .accelerator-integration-fullscreen .chat-tooltip {
      display: none;
    }
    
    .accelerator-integration .chat-bubble {
      background-color: white;
      background-image: url(https://cbd.aramco.com.sa/myit/assets/aramco.png);
      background-position: center center;
      background-repeat: no-repeat;
      border-radius: 50%;
      height: 70px;
      width: 70px;
      box-shadow: 0px 0px 15px 0px rgb(0 0 0 / 75%);
      cursor: pointer;
      transition: all 0.3s ease-in-out;
      margin-left: 10px;
      margin-right: 10px;
      margin-bottom: 10px;
    }

    .accelerator-integration .chat-bubble:hover {
      width: 80px;
      height: 80px;
      margin-left: 0;
      margin-right: 0;
      margin-bottom: 0;
    }

    .accelerator-integration .chat-tooltip-container {
      position: relative;
      visibility: hidden;
      height: 67px;
    }

    .accelerator-integration .chat-tooltip {
      font-family: 'Trebuchet MS', sans-serif;
      background: white;
      border-radius: 0.8rem;
      color: black;
      font-size: 1rem;
      font-weight: 100;
      border: solid 2px white;
      padding: 10px;
      margin-right: 14px;
      line-height: 1.3em;
      box-shadow: 0px 0px 15px 0px rgb(0 0 0 / 75%)
    }

    .accelerator-integration .chat-tooltip:before {
      content: "";
      width: 0px;
      height: 0px;
      position: absolute;
      border-left: 10px solid white;
      border-right: 10px solid transparent;
      border-top: 10px solid transparent;
      border-bottom: 10px solid transparent;
      right: -6px;
      bottom: 34px;
    }

    .accelerator-integration .chat-window {
      border: none;
    }

    .accelerator-integration .chat-window-closed {
      visibility: hidden;
      width: 0;
      height: 0;
    }

    .accelerator-integration .chat-window-boxed {
      visibility: visible;
      width: 350px;
      height: 600px;
      position: absolute;
      right: 55px;
      bottom: 0px;
      box-shadow: 0px 0px 15px 0px rgb(0 0 0 / 75%);
      border-radius: 5px;
      z-index: 100;
    }
    @media screen and (max-height: 700px) {
      .accelerator-integration .chat-window-boxed {
        height: calc(100vh - 50px);
      }
    }
    .accelerator-integration .chat-window-fullscreen {
      width: 100%;
      height: 100%;
    }

    .accelerator-item-visible {
      visibility: visible !important;
    }

    .accelerator-item-hidden {
      visibility: hidden !important;
      display: none !important;
    }
  `
}
