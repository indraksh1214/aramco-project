# Chat UI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Backend-requests will be routed to `http://localhost:6004` as configured in `proxy.conf.json`

## Code scaffolding

Run `npm run ng -- generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Localization

The application is localized with [ngx translate](https://github.com/ngx-translate/core).

You can find the translation files in [src/assets/i18n/](./src/assets/i18n/).

## Chat Widget

An example of embedding the Chat UI into a website can be found at [src/widget/](./src/widget/).

To view it in the browser navigate to `http://localhost:4200/widget`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
