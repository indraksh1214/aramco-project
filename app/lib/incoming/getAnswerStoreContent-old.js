/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

//functions to add to logs
const logger = require("../../utils/logger/logger");
const database = require("../../utils/db/mongodb");
const archibusRequestService = require('../../utils/services/archibus-request-service')
//const database = require("../../utils/db/cloudant.js");

const getAnswerStoreContent = {
  type: "incoming",
  name: "get-the-content-from-answer-store",
  controller: (bot, update, next) => {
    logger.debug("----------------");
    logger.debug(update.watsonUpdate.output);
    console.log('update.watsonUpdate.output : ', update.watsonUpdate.output)
    try {
      let userinfo = JSON.parse(update.watsonUpdate.user_cookies.USER_PROFILE);
      logger.info('userinfo : ', userinfo)
      update.userinfo = {
        user_name: userinfo.givenname,
        role_name: userinfo.employeetype,
        em_id: userinfo.badgeN,
      }
      var arrayPromises = [];

      for (var i = 0; i < update.watsonUpdate.output.generic.length; i++) {
        arrayPromises.push(getParsedContent(update.watsonUpdate.output.generic[i], update));
      }

      Promise.all(arrayPromises)
        .then((result) => {
          //if there are buttons we need to added to the end and if there is buttons before we need to merge it always at the end
          // result = checkIfThereAreButtons(result);
          update.watsonUpdate.output.quickReplies = '<button type="button">Click Me!</button>'
          logger.debug("result", result);
          console.log("result", result);

          update.watsonUpdate.output.text = result;
          return next();
        })
        .catch((err) => {
          logger.error("ERROR -getAnswerStoreContent.js:", err);

          return next();
        });
    } catch (err) {
      logger.error("ERROR -getAnswerStoreContent.js:", err);
    }
  },
};

async function getParsedContent(genericObject, update) {
  logger.debug(genericObject);
  console.log('genericObject : ', genericObject);
  console.log('update.userinfo : ', update.userinfo);
  var language = "en-US";
  if (update.message.language && update.message.language.length > 0) {
    language = update.message.language;
  }
  var queryObject = {
    language: language
  };
  logger.debug("-------------------Query Logic-------------------");
  logger.debug(queryObject);
  // console.log(queryObject);
  if (genericObject && genericObject.response_type && genericObject.response_type === "text") {
    queryObject.text = genericObject.text;
    logger.info(" * Watson Text: " + queryObject.text);

    /**
     * Validate if the ouput text is not a real answer
     */

    if (!/^[a-zA-Z0-9_]+$/.test(queryObject.text)) {
      // Validation failed
      console.log(
        "Returning output text from WA since is not a valid ANSWER_STORE_ID: ",
        queryObject.text
      );
      return Promise.resolve(queryObject.text);
    }

    /*/ find the answer in local .env file
    if (/^[a-zA-Z0-9_]+$/.test(queryObject.text)){
      console.log(
        " * valid ANSWER_STORE_ID: ",
        queryObject.text
      );
      var answerTag = queryObject.text
      if (answerTag in process.env) {
        answerTag = process.env[answerTag]
      }
      return Promise.resolve(answerTag);
    }
    /*/

    /**Find the answer in .env and if exist, connect to external service */
    // let API_CALL_KEYWORDS = process.env.API_CALL_KEYWORD.split(' ');
    let SATISFACTORY_SURVEY = process.env.SUBMIT_SATISFACTORY_SURVEY.split(' ');
    console.log('update.watsonUpdate : --------------------------- \n', update.watsonUpdate);
    if (queryObject.text == process.env.CHECK_SERVICE_REQUEST) {
      let enti_len = update.watsonUpdate.output.entities.length;
      let ticketNumber = update.watsonUpdate.output.entities[enti_len - 1].value;
      let result = await archibusRequestService.checkServiceRequestStatus(update.userinfo, ticketNumber);
      if(result.status == 3){
        queryObject.text = result.message
      }
      else return Promise.resolve(result.message);
    } else if (queryObject.text == process.env.ACTIVE_TICKETS) {

      let result = await archibusRequestService.getActiveRequest(update.userinfo);
      if (result.ticket_number) {
        /** save ticket number for  future refrence */
      }
      return Promise.resolve(result.message);
    }else if (queryObject.text == process.env.CANCEL_SERVICE_REQUEST) {
      console.log('update.watsonUpdate.context : ', update.watsonUpdate.context);
      let ticketNumber = update.watsonUpdate.context.ticket;
      let cancel_comment = update.watsonUpdate.context.comments;
      console.log('ticketNumber : ', ticketNumber);
      console.log('cancel_comment : ', cancel_comment);
      let result = await archibusRequestService.cancelRequest(update.userinfo, ticketNumber, cancel_comment);
      if(result.status == 3){
        queryObject.text = result.message
      }
      else return Promise.resolve(result.message);
    } else if (queryObject.text == process.env.PRE_CHECK_EDIT) {

      let enti_len = update.watsonUpdate.output.entities.length;
      let ticketNumber = update.watsonUpdate.output.entities[enti_len - 1].value;
      let result = await archibusRequestService.preCheckUpdate(update.userinfo, ticketNumber);
      if(result.status == 3){
        queryObject.text = result.message
      }
      else if (result.status != 1) {
        return Promise.resolve(result.message);
      }
    } else if (queryObject.text == process.env.EDIT_SERVICE_REQUEST) {
      console.log('update.watsonUpdate.context : ', update.watsonUpdate.context);
      let ticketNumber = update.watsonUpdate.context.ticket;
      let description = update.watsonUpdate.context.update_field;
      console.log('ticketNumber : ', ticketNumber);
      console.log('description **** : ', description);
      let pre_check_result = await archibusRequestService.preCheckUpdate(update.userinfo, ticketNumber);
      if (pre_check_result.status == 1) {
        let result = await archibusRequestService.UpdateRequest(update.userinfo, ticketNumber, description);
        return Promise.resolve(result.message);
      }
      else if(pre_check_result.status == 3){
        queryObject.text = pre_check_result.message
      }
      else{
        return Promise.resolve(pre_check_result.message);
      }
      
    } else if (queryObject.text == process.env.CHECK_SATISFACTORY_SURVEY) {

      let enti_len = update.watsonUpdate.output.entities.length;
      let ticketNumber = update.watsonUpdate.output.entities[enti_len - 1].value;
      let result = await archibusRequestService.preCheckSatisfactorySurvey(update.userinfo, ticketNumber);
      if(result.status == 3){
        queryObject.text = result.message
      }
      else return Promise.resolve(result.message);
    } else if (queryObject.text == process.env.LIST_SATISFACTORY_SURVEY) {

      let result = await archibusRequestService.listPendingSatisfactionSurvey(update.userinfo);
      if (result.ticket_number) {
        /** save ticket number for  future refrence */
      }
      return Promise.resolve(result.message);
    } else if (SATISFACTORY_SURVEY.includes(queryObject.text)) {
      let ticketNumber = update.watsonUpdate.context.ticket_number;
      let rating = update.watsonUpdate.context.rate;
      let review = update.watsonUpdate.context.Notes;
      let result = await archibusRequestService.satisfactionSurvey(update.userinfo, ticketNumber, rating, review);
      if(result.status == 3){
        queryObject.text = result.message
      }
      else if (result.status != 1) {
        return Promise.resolve(result.message);
      }
    }

    // find the answer in dedicated NoSQL Database
    return database
      .getAnswerContent(queryObject, function (resp, err) {
        if (err) {
          logger.error(err);
        }
        logger.error("ERROR -75 getAnswerStoreContent.js:", err);
        return Promise.resolve(resp);
      })
      .then((answerDoc) => {
        logger.debug("final answerDocArray ", answerDoc);
        if (queryObject.text === process.env.RESPONSE_ACTION_KEYWORD) {
          /**Format the message and replace the variable */
          let enti_len = update.watsonUpdate.output.entities.length;
          let rating = update.watsonUpdate.output.entities[enti_len - 1].value;

          answerDoc = answerDoc.replace(/#rating#/g, rating);
        }
        if (answerDoc) return Promise.resolve(answerDoc);
        else throw new Error("undefined answerDoc");
      })
      .catch((err) => {
        logger.error("answerStore - not found tag", err);
        return Promise.reject(err);
      });
    //*/
  } else if (
    genericObject &&
    genericObject.response_type &&
    genericObject.response_type === "pause"
  ) {
    if (genericObject.time && genericObject.time > 0) {
      var pauseTimeoutMS = genericObject.time;
      var pauseTag = "<pauseFromWA wait=" + pauseTimeoutMS + " />";
    } else {
      var pauseTag = "<pauseFromWA />";
    }

    logger.debug(pauseTag);
    return Promise.resolve("pauseTag");

  } else if (
    genericObject &&
    genericObject.response_type &&
    genericObject.response_type === "option"
  ) {
    var buttonsTag = genericObject.title + "<buttons>";
    genericObject.options.forEach((element) => {
      logger.debug(element.label);
      buttonsTag = buttonsTag + element.label + "#" + element.value.input.text + "/";
      // buttonsTag = buttonsTag +`<button name="${element.label}" type="button" value="${element.value.input.text}">${element.label}</button>`
    });
    buttonsTag = buttonsTag.substring(0, buttonsTag.length - 1); //remove the last "/" separator
    buttonsTag = buttonsTag + "</buttons> ";
    return Promise.resolve(buttonsTag);

  } else if (
    genericObject &&
    genericObject.response_type &&
    genericObject.response_type === "suggestion"
  ) {
    var buttonsTag = genericObject.title + " <buttons>";
    genericObject.suggestions.forEach((element) => {
      logger.debug(element.label);
      buttonsTag = buttonsTag + element.label + "/";
    });
    buttonsTag = buttonsTag.substring(0, buttonsTag.length - 1); //remove the last "/" separator
    buttonsTag = buttonsTag + "</buttons> ";
    return Promise.resolve(buttonsTag);

  } else if (
    genericObject &&
    genericObject.response_type &&
    genericObject.response_type === "image"
  ) {
    var imageTag =
      (genericObject.title ? genericObject.title : " ") +
      " <img src='" +
      genericObject.source +
      "' height='100%' width='100%' />";

    return Promise.resolve(imageTag);

  } else if (
    genericObject &&
    genericObject.response_type &&
    genericObject.response_type === "connect_to_agent"
  ) {
    var agent;
    var agent_connection;
    for (const [key, value] of Object.entries(genericObject.transfer_info.target)) {
      console.log(`${key}: ${JSON.stringif(value)}`);
      if (key) {
        agent = key;
        agent_connection = value
      }
    }
    var connect_to_numan_agent =
      (agent_connection ? genericObject.agent_available.message : genericObject.agent_unavailable.message)
    return Promise.resolve(connect_to_numan_agent);

  } else {
    return Promise.resolve(" ... ");
  }
}

module.exports = {
  getAnswerStoreContent,
};