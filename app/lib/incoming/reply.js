/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

//functions to add to logs
const logger = require("../../utils/logger/logger");

const replyToUser = {
  type: "incoming",
  name: "the-responses-from-watson-conversation",
  controller: (bot, update) => {
    try {
      logger.debug(
        "Messages from Watson Assistant: '" +
          update.watsonUpdate.output.text +
          "'" +
          " towards the user " +
          update.sender.id
      );
      // return bot.reply(update);
      return bot.sendTextCascadeTo(update.watsonUpdate.output.text, update.sender.id);
    } catch (err) {
      logger.error(err);
    }
  },
};

module.exports = {
  replyToUser,
};
