/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

//const database = require('../../api/dashdb.js');
//const database = require("../../utils/db/cloudant");
const database = require("../../utils/db/mongodb");
const logger = require("../../utils/logger/logger");

const saveConversation = {
  type: "outgoing",
  name: "saving-conversation-in-database",
  controller: (bot, update, message, next) => {
    try {
      var { watsonUpdate } = update;
    } catch (err) {
      logger.error("error-saving-conversation-in-database", err);
      if (message) logger.error(message);
      if (update) logger.error(update);
    }

    var text = "";

    if (!message.message || !message.message.text) {
      return next();
    }

    if(update){
    var text = update.message.text;

    
    update.session.watsonContext.last_text = {};
    update.session.watsonContext.last_text = text;

    logger.info("SAVING_CONVERSATION of user.id ", message.recipient.id);
  
    database.saveConversation(watsonUpdate, message, function (err, resp) {
      if (err) {
        logger.error(err);
      }
      return Promise.resolve();
    });
    }
    return Promise.resolve();
  },
};

module.exports = {
  saveConversation,
};
