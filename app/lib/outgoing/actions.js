/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

const actions = require("botmaster-fulfill-actions");
const config = require("../../../config/config.js");

const logger = require("../../utils/logger/logger");


/**
 *   INPUT:  <buttons>Si / No</buttons>
 *   OUTPUT: Buttons in UI
 */
actions.buttons = {
  controller: (params) => {
    logger.info("BUTTONs");
    logger.debug("=============BUTTONs=============");

    const buttonTitles = params.content.split("/");
    params.message.message.quick_replies = [];
    for (const buttonTitle of buttonTitles) {
      //mylogger.info("buttonTitle: " + buttonTitle);
      if (buttonTitle.includes("#")) {
        const buttonValues = buttonTitle.split("#");

        params.message.message.quick_replies.push({
          content_type: "text",
          title: buttonValues[0],
          payload: buttonValues[1],
        });
      } else {
        params.message.message.quick_replies.push({
          content_type: "text",
          title: buttonTitle,
          payload: buttonTitle,
        });
      }
    }
    logger.debug(params.update.message);
    return Promise.resolve("");
  },
};

/**
 *   INPUT:  <buttons>Si / No</buttons>
 *   OUTPUT: Buttons in UI
 */
actions.button = {
  controller: (params) => {
    logger.info("BUTTON");
    logger.debug("=============BUTTON=============");
    logger.info(params.message);
    logger.info(params.attributes);
    logger.info(params.content, { depth: null, colors: true });

    logger.info(params.attributes.value, { depth: null, colors: true });

    params.message.message.quick_replies.push({
      content_type: "text",
      title: params.message,
      payload: params.attributes.value,
    });

    logger.debug(params.update.message);
    return Promise.resolve("");
  },
};

/**
 *   INPUT:  <feedback/>
 *   OUTPUT: Show Feedback in UI
 */
actions.feedback = {
  controller: (params) => {
    params.message.message.feedback = {};
    params.message.message.feedback = true;

    var feedback_backup = {
      conv_id: params.update.session.watsonContext.conversation_id,
      user_id: params.message.recipient.id,
      input: params.update.watsonUpdate.input,
      intents: params.update.watsonUpdate.intents,
      entities: params.update.watsonUpdate.entities,
      output: params.update.watsonUpdate.output,
      source: process.env.NODE_ENV,
    };

    params.update.session.watsonContext.feedback_visited = {};
    params.update.session.watsonContext.feedback_visited = feedback_backup;

    return Promise.resolve();
  },
};

// Action selection Menu: 
/**
 *   INPUT:  <menu>Yes#YES/No#NO/May be#MAYBE/Anything Else#ELSE</menu>
 *   OUTPUT: dropdown list in UI
 */
actions.menu = {
  controller: (params) => {
    logger.info("Menu");
    logger.debug("=============Menu=============");

    const menuTitles = params.content.split("/");

    params.message.message.menu_replies = [];
    for (const menuTitle of menuTitles) {
      if (menuTitle.includes("#")) {
        const menuValues = menuTitle.split("#");

        params.message.message.menu_replies.push({
          content_type: "text",
          title: menuValues[0],
          payload: menuValues[1]
        });
      } else {
        params.message.message.menu_replies.push({
          content_type: "text",
          title: menuTitle,
          payload: menuTitle
        });
      }
    }
    //console.log(" * params.update.message:");
    //console.dir(params.update.message, { depth: 5, colors: true });
    
    return Promise.resolve();
  }
};

//-------------------- Integrations ---------------------
const myit = require("./integrations/myit_actions.js");
const discovery = require("./integrations/discovery_actions.js");
const smartcard = require("./integrations/smartcard_actions.js");
const remedy = require("./integrations/remedy_actions.js");

//--------------------MyIT integration---------------------
actions.displayServices = myit.displayServices
actions.displayForm = myit.displayForm
actions.submitForm = myit.submitForm
actions.displayAssets = myit.displayAssets
actions.displayLocation = myit.displayLocation
actions.emailIT = myit.emailIT
actions.requestStatus = myit.requestStatus
actions.getUserLocation = myit.getUserLocation
actions.listEnrolledDevices = myit.listEnrolledDevices

//--------------------Smart Card integration---------------------
actions.verifyOTP = smartcard.verifyOTP
actions.verifyEmployee = smartcard.verifyEmployee

//--------------------Remendy integration---------------------
actions.openTickets = remedy.openTickets

//--------------------Discovery integration---------------------
actions.longtailNLU = discovery.longtailNLU


module.exports = actions;
