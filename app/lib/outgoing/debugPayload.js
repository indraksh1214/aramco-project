/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */



/*
*  This function is used to save the lastContext for DEBUG PAYLOAD
*  Some next steps (e.g. Feedback) depend on this functionality
*/
const savePayloadForDebug = {
 type: 'outgoing',
    name: 'setting-lastContext-to-message-for-debug',
    controller: (bot, update, message, next) => {
      if(update){
        message.session = update.watsonUpdate;
        next();
      } else {
        next();
      }
  }
};

module.exports = {
  savePayloadForDebug
}
