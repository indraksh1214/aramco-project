/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

const actions = require("botmaster-fulfill-actions");
const config = require("../../../../config/config.js");

const logger = require("../../../utils/logger/logger");

const axios = require("axios");
const https = require("https");

const agent = new https.Agent({ 
	rejectUnauthorized: false
  });
  
//console.log(" * Remedy host endpoint:", config.remedyCredentials.remedyAPIHost)


//-------------------GUI generation and Remedy Calls-------------------
/**
 * Utils function to see if the object is empty
 * @param {*} obj
 */
function isEmpty(obj) {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}
/**
 * Util function to create the html to render the tickets
 *
 * Note: This function should be handled in the client code and not in server
 * @param {} resultTickets
 */
function renderTickets(resultTickets) {
  console.log(isEmpty(resultTickets));
  console.log(resultTickets.incidents);
  console.log(resultTickets.incidents.length);

  if (!isEmpty(resultTickets)) {
    var result_text = "";
    if (resultTickets.incidents && resultTickets.incidents.length > 0) {
      result_text = "The following open tickets were found:";
    } else {
      result_text = "No trouble tickets were found.";
    }

    var answer_content = result_text

    //check the incidents Array if they are not null or lenght >0
    // for each array iterate and create a html to display, if lenght =0 display no incidents found

    if (resultTickets.incidents && resultTickets.incidents.length > 0)
      resultTickets.incidents.forEach((inc) => {
        answer_content = answer_content +
          "<div class='longTextBox' id='div_" + inc.id +"'>" +
              "<div class='longTextBoxWrapper'>" +
                "<div class='longTextBoxTitle'> ID: " + inc.id +"</div>" +
                "<div class='longTextBoxSubtitle'> Title: " + inc.title +"</div>" +
                "<div class='longTextBoxSubtitle'> Status: " +inc.status +"</div>" +
              "</div>" +
          "</div>";
      });

    var answer_content = "<div class='longtailResults'>" + answer_content + "</div>";
  } else {
    answer_content =
      "<div class='longtailCard message-inner-v2'><p class='summaryText'> " +
      "Nothing to show" +
      "</p></div>";
  }

  return answer_content;
}
/**
 * Utils function to call the api get open tickets
 * @param {*} alias
 * @param {*} accessToken
 */
function getOpenTickets(alias, accessToken) {
  let remedyConnectorURL = config.remedyCredentials.remedyAPIHost;
  return new Promise(function (resolve, reject) {
    const config = {
      headers: {
        Authorization: "Bearer " + accessToken,
        //"x-user-alias": alias,
		"x-user-alias": "shamasba",
      },
    };
    axios
      .get(remedyConnectorURL + "/v1/remedy/tickets/incidents/open", config)
      .then((result) => {
        console.log(result.data);
        //resolve(JSON.stringify(result.data));
        resolve(renderTickets(result.data));
      })
      .catch((err) => {
        logger.error(err);
        reject("😔 Error " + err);
      });
  });
}

//--------------------Remendy actions---------------------

/**
 *
 * <openTickets />
 */
actions.openTickets = {
  controller: function (params) {
    let remedyConnectorURL = config.remedyCredentials.remedyAPIHost;

    const userId = params.update.sender.id;
    params.bot.sendTextMessageTo(
      "Loading trouble tickets... One moment please!",
      userId
    );

    return new Promise(function (resolve, reject) {
      axios
        .post(remedyConnectorURL + "/v1/authenticate", {
          clientId: config.remedyCredentials.clientId,
          secret: config.remedyCredentials.clientSecret,
        })
        .then((body) => {
          //console.log(body.data);
          return body.data.token;
        })
        .then((accessToken) => {
          console.log(" * NetworkID:",params.update.session.watsonContext.UserNetworkid);  
          resolve(getOpenTickets(params.update.session.watsonContext.UserNetworkid, accessToken));
        })
        .catch((err) => {
			    console.log(" * Error:",err)
          logger.error(err);
          resolve("Error to retrieve access token ");
        });
    });
  },
};

module.exports = actions;
