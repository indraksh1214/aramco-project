/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

const actions = require("botmaster-fulfill-actions");
const config = require("../../../../config/config.js");

const logger = require("../../../utils/logger/logger");

//--------------------Discovery integration---------------------
actions.longtailNLU = {
  controller: (params) => {
    const DiscoveryV1 = require("ibm-watson/discovery/v1");
    var moment = require("moment");

    const filter = params.attributes.filter;
    const query = params.attributes.query;
    const natural_language_query = params.attributes.natural_language_query;
    const passages = params.attributes.passages;
    const aggregation = params.attributes.aggregation;
    const count = params.attributes.count || 3;
    const highlight = params.attributes.highlight || config.discoveryCredentials.highlight;

    return new Promise(function (resolve, reject) {
      var queryInput = "";
      if (query && query.length > 0) {
        queryInput = query;
      } else {
        if (params.update.message.text === "Nichts von dem oben genannten")
          queryInput = params.update.session.watsonContext.last_text;
        else queryInput = params.update.message.text;
      }

      logger.debug("Oooooooooooooooooooooooooooooooooooooooo=====");
      logger.info("DISCOVERY QUERY: " + queryInput + " filter: " + filter);
      logger.debug("highlight ", highlight);
      params.update.session.watsonContext.discoveryQuery = { text: queryInput };
      logger.debug("Oooooooooooooooooooooooooooooooooooooooo=====");

      const discovery = new DiscoveryV1({
        version: config.discoveryCredentials.version_date,
        iam_apikey: config.discoveryCredentials.apikey,
        url: config.discoveryCredentials.url,
      });

      const retryOptions = { retries: 3, minTimeout: 200 };
      const retry = require("async-retry");
      retry(async (bail) => {
        discovery
          .query({
            environment_id: config.discoveryCredentials.environment_id,
            collection_id: config.discoveryCredentials.collection_id,
            filter: filter,
            count: count,
            highlight: highlight,
            natural_language_query: queryInput,
            deduplicate: true,
            deduplicate_field: "question",
          })
          .then((queryResponse) => {
            if (process.env.NODE_ENV === "development") logger.debug(queryResponse);
            //params.update.session.watsonContext.discoveryResultsRAW = {};
            //params.update.session.watsonContext.discoveryResultsRAW = queryResponse;

            var i = 0;
            var discoveryResults = [];
            while (queryResponse.results[i] && i < count) {
              let body = queryResponse.results[i].answer;
              if (typeof body === "undefined") {
                body = "";
              }
              //let highlight = queryResponse.results[i].highlight.answer[0];
              // if (highlight) {
              //   body = highlight;
              // }
              if (queryResponse.results[i].highlight && queryResponse.results[i].highlight.answer) {
                body = "";
                queryResponse.results[i].highlight.answer.forEach((element) => {
                  if (body == "") body = element;
                  else body = body + "<br/>" + element;
                });
              }

              if (
                queryResponse.results[i].metadata.extract_answer_content_format &&
                queryResponse.results[i].metadata.extract_answer_content_format == "markdown"
              ) {
                console.log("========MARKDOWN ORIGINAL FROM WDS=====");
                console.log(queryResponse.results[i].metadata);
                console.log(">>>");
                //body = body.replace(/\n/g, "");
                console.log(body);
                var showdown = require("showdown");
                var converter = new showdown.Converter();
                converter.setOption("simpleLineBreaks", false);
                body = converter.makeHtml(body);
                console.log("Ooooooooooooooooooooo HTML CONVERTED ooooooooooooooooooo=====");
                console.log(body);
              }

              if (
                queryResponse.results[i].extracted_metadata &&
                queryResponse.results[i].extracted_metadata.title
              ) {
                var title = queryResponse.results[i].extracted_metadata.title;
              } else {
                if (
                  queryResponse.results[i].extracted_metadata &&
                  queryResponse.results[i].extracted_metadata.filename
                ) {
                  var title = queryResponse.results[i].extracted_metadata.filename;
                  title = title.replace(".docx", "");
                  title = title.replace(".pdf", "");
                  title = title.replace(".html", "");
                  title = title.replace(".json", "");
                }
              }

              //TODO REMOVE THIS AFTER CHANGE THE CUSTOM CRAWLER
              title = title.replace("ÖffnenMinimieren", "");

              if (
                queryResponse.results[i].metadata &&
                queryResponse.results[i].metadata.ingest_datetime
              ) {
                var ingest_datetime = queryResponse.results[i].metadata.ingest_datetime
                  ? queryResponse.results[i].metadata.ingest_datetime
                  : "";
                var ingest_datetime = moment(ingest_datetime).format("DD.MM.YYYY");
              } else {
                var ingest_datetime = "Datum nicht verfügbar";
              }

              if (
                queryResponse.results[i].metadata &&
                queryResponse.results[i].metadata.source.url
              ) {
                var sourceURL = queryResponse.results[i].metadata.source.url;
                //get the source of the domain
                var url = sourceURL;
                var hostname = new URL(url).hostname;
                try {
                  hostname = hostname.substring(
                    hostname.lastIndexOf(".", hostname.lastIndexOf(".") - 1) + 1
                  );
                  hostname = hostname.split(".");
                  hostname = hostname[0].toUpperCase();
                } catch (e) {
                  hostname = sourceURL;
                }
              } else {
                var hostname = "sample";
                var sourceURL = "https://www.sample.de";
              }

              discoveryResults[i] = {
                body: body.replace(/\n/g, "<br/>"),
                confidence: queryResponse.results[i].result_metadata.confidence,
                ingest_datetime: ingest_datetime,
                // highlight: highlight,
                id: queryResponse.results[i].id,
                sourceUrl: sourceURL,
                hostname: hostname,
                title: title,
              };

              i++;
            }

            // console.dir(discoveryResults, { depth: 2, colors: true });
            params.update.session.watsonContext.discoveryResults = {};
            params.update.session.watsonContext.discoveryResults = discoveryResults;

            //return the word LONGTAIL so then in the javascript if the LONGTAIL word is found it will recoginize that
            //should render LONGTAIL
            return resolve("LONGTAIL");
          })
          .catch((err) => {
            logger.error("ERROR-WDS:", err);
            bail("ERROR-WDS");
            resolve(
              "Die Informationen konnten nicht korrekt abgerufen werden. Bitte versuchen Sie es erneut."
            );
          });
      }, retryOptions);
    });
  },
};

module.exports = actions;
