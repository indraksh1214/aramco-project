/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

const actions = require("botmaster-fulfill-actions");
const config = require("../../../../config/config.js");

const logger = require("../../../utils/logger/logger");

const axios = require("axios");
const https = require("https");

const agent = new https.Agent({ 
	rejectUnauthorized: false
  });
  
const endpoint = config.smartCard.smartcard_api_endpoint
const user = config.smartCard.smartcard_user;
const pass = config.smartCard.smartcard_pass;

//console.log(" * smartCard API endpoint:", endpoint)

//--------------------Smart Card integration---------------------
// VerifyEmployee: Verifies user’s information and sends a one time number to the registered mobile
// https://amdwsp-dv.aramco.com.sa/IVRRest/Service1.svc/VerifyEmployee/BadgeNo/Last4DigitaofNationalIDorIqama

/**
 *   INPUT:  <verifyEmployee BadgeNo=$BadgeNo Last4DigitaofNationalIDorIqama=$Last4DigitaofNationalIDorIqama/>
 *   OUTPUT: Display results of verification in UI
 */
actions.verifyEmployee = {
  controller: (params) => {
    console.log(" * verifyEmployee action is triggered ... ")

    var BadgeNo = params.attributes.BadgeNo
    var Last4DigitaofNationalIDorIqama = params.attributes.Last4DigitaofNationalIDorIqama   
    var url = endpoint + "/VerifyEmployee/" + BadgeNo + "/" + Last4DigitaofNationalIDorIqama
    
    console.log(" * BadgeNo: ",BadgeNo)
    console.log(" * Last4DigitaofNationalIDorIqama: ", Last4DigitaofNationalIDorIqama)
    console.log(url)

    if(config.environment==="development"){ 
      return Promise.resolve("Employee verified successfully");
    } else { // if production

      var userVerified = axios.get(url, {
        httpsAgent: agent,
        auth: {
          username: user,
          password: pass
        }
      })
      .then((response) => {
          console.log(response.data.Sucess)
          if (response.data.Sucess === true){
              console.log(response.data);
              return Promise.resolve("Employee verified successfully");
          } else {
              console.log(response.data);
              return Promise.resolve("Wrong employee information, please try again");
          }
      })
      .catch((error) => {
          console.log("* Error occured while calling endpoint "+ url+":",error);
          return Promise.resolve("Something went wrong, please try again")
      });

      return userVerified;

    }

  }
};

// Verify_OTP: verifies the sent OTP
// https://amdwsp-dv.aramco.com.sa/IVRRest/Service1.svc/Verify_OTP/BadgeNo/OTP
//
/**
 *   INPUT:  <verifyOTP BadgeNo=$BadgeNo  OTP=$OTP/>
 *   OUTPUT: SDisplay results of verification and UnSetSmartCard
 */
actions.verifyOTP = {
  controller: (params) => {
    console.log(" * verifyOTP action is triggered ... ")

    var BadgeNo = params.attributes.BadgeNo
    var OTP = params.attributes.OTP
    
    console.log(" * BadgeNo: ",BadgeNo)
    console.log(" * OTP: ", OTP)
  
    var url = endpoint + "/Verify_OTP/" + BadgeNo + "/" + OTP;
    
    if(config.environment==="development"){ 
      return Promise.resolve("Your request REQUESTID has been submitted to your group leader for approval. It is currently pending the approval of GL_FullName");
    } else { // if production

      var otpVerified = axios.get(url, {
        httpsAgent: agent,
        auth: {
          username: user,
          password: pass
        }
      })
      .then((response) => {
        console.log(response.data.Sucess)
        if (response.data.Sucess === true){
          console.log(response.data);     
          return Promise.resolve(response.data.Message);
        } else {
          console.log(response.data);
          return Promise.resolve("OTP verification failed and / or SmartCard enforcement could not be removed, please try again later");
        }
      })
      .catch((error) => {
        console.log("* Error occured while calling endpoint "+ url+":",error);
        return Promise.resolve("Something went wrong, please contact support")
      });
      return otpVerified;

    }


  }
};


module.exports = actions;
