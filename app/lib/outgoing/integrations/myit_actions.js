/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

const actions = require("botmaster-fulfill-actions");
const { resolve } = require("url");
const config = require("../../../../config/config.js");
const logger = require("../../../utils/logger/logger");
const mongodb = require("../../../utils/db/mongodb.js");



const endpoint = config.crm.myit_api_endpoint
//console.log(" * myIT API endpoint:", endpoint)
const sap_client = config.crm.myit_sap_client 
//console.log(" * sap_client:", sap_client)

const https = require("https");
const agent = new https.Agent({ 
  rejectUnauthorized: false
});	 

const axios = require("axios");

//--------------------- MyIT API Actions ----------------------

// Action login to CRM: 
/**
 *   INPUT:  <loginCRM/>
 *   OUTPUT: Show login screen 
 */

actions.loginCRM = {
  controller: (params) => {
    console.log(" * loginCRM action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies)
    return Promise.resolve("LOGINCRM");
  }
};

// Action get the Service Catalog: 
/**
 *   INPUT:  <displayServices/>
 *   OUTPUT: Show dropdown list of options in UI
 */
// Get the Service Catalog: 
// https://dcr.aramco.com.sa/sap/z_myit/cat?sap-client=052
// URI: /z_myit/cat
// Description: The service provides a list of services that can be requested through the myIT app.

actions.displayServices = {
  controller: (params) => {
    console.log(" * displayServices action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies)
    
    var listServices = "";

    if(config.environment==="development"){
       var response  = {"message": {"type": "S","message": "Catalog loaded successfully"},"data": [{"serviceCode": 1,"serviceId": "ITC_ML_VPN","title": "Full Corporate VPN Access","dynamic": true,"keywords": "VPN laptop remote home","favorite": "X"},{"serviceCode": 2,"serviceId": "ITC_CPI_ACC","title": "Mini-myHome access","dynamic": true,"keywords": "portal employee remote home","favorite": "X"}]}
       for (var i=0;i<response.data.length;i++){
        listServices = listServices + response.data[i].title + "/"
      } 
      listServices = listServices.slice(0,-1)
      return Promise.resolve("<buttons>"+listServices.toString()+"</buttons>"); 
    } else { // if production
      const config = {
        headers: {
          'Content-Type': 'application/json',
          'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
        },
        method: "GET",
        url: endpoint + "/cat",//
        //url: endpoint + "/cat" + "?sap-client="+sap_client, // with client
        httpsAgent: agent
      }     
      var enpointResponse = axios.request(config).then((response) => {
          if (response.data.message.type==="S" || response.data.message.type==="W"){
              for (var i=0;i<response.data.data.length;i++){
                listServices = listServices + response.data.data[i].title + "/"
              }
              listServices = listServices.slice(0,-1)
              logger.info("<buttons>"+listServices+"</buttons>")
              return Promise.resolve("<buttons>"+listServices.toString()+"</buttons>"); 
            } else{
              logger.error(response)
              return Promise.resolve(" * failed to retrive services ");
            }
        }).catch((error) => {
          console.log("* Error occured while calling endpoint ",error);
          return Promise.resolve(" * failed to retrive services ");
        });

        return enpointResponse
    }

  }
};

// Action to obtain metadata for CRM services 
/**
 *   INPUT:  <displayForm scrnid ="ITC_ML_VPN" service_def="1"/>
 *   OUTPUT: Show Forms in UI
 */
// Use the service code to obtain metadata for CRM services 
// (Marked as dynamic in the catalog)
// Note: serviceCode is part of the catalog –example below is for serviceCode = 1 VPN Access
// https://dcr.aramco.com.sa/sap/z_myit/service_def/1?sap-client=052

actions.displayForm = {
  controller: (params) => {
    console.log(" * displayFrom action is triggered ... ");
    console.log(" * user_cookies:", params.update.user_cookies);
    var formResults = [];
    
    var screen_id = -1
    if (params.attributes.scrnid){
      screen_id = params.attributes.scrnid
    }
    var service_def = -1
    if (params.attributes.service_def){
       service_def = params.attributes.service_def
    }

    var form_content = "";
    if(config.environment==="development"){
      if (params.update.session.watsonContext.formContent){
        form_content = {"data":{"conds":[{"condid":"FORM_NOTE","actions":[{"prop":"value","value":"W_NOTE","fieldId":"UIMESSAGE_INSTRUC"}],"evalCond":"ACT = 'REV'"},{"condid":"FORM_WARNING","actions":[{"prop":"value","value":"W_NOTE","fieldId":"UIMESSAGE_WARN"}],"evalCond":"ACT = 'GRANT'"},{"condid":"VIEW_MODE","actions":[{"prop":"readOnly","value":"true","fieldId":"ACT"}],"evalCond":"'INIT' <> 'INIT'"}],"fields":[{"props":{"label":"Action Required","domain":[{"value":"GRANT","csticKey":"ACT","description":"Grant Access"},{"value":"REV","csticKey":"ACT","description":"Revoke Access"}],"length":"30 ","visible":"true","datatype":"SYM","readOnly":"false","required":"true","screenType":"Single Select","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"true"},"fieldId":"ACT"},{"props":{"label":"Note:","domain":[{"value":"W_NOTE","csticKey":"UIMESSAGE_INSTRUC","description":"ISA/ASIA Approval is required"}],"length":"30 ","visible":"true","datatype":"SYM","readOnly":"false","required":"false","screenType":"Message","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"UIMESSAGE_INSTRUC"},{"props":{"label":"Warning:","domain":[{"value":"W_NOTE","csticKey":"UIMESSAGE_WARN","description":"Department Manager Approval is required"}],"length":"30 ","visible":"true","datatype":"SYM","readOnly":"false","required":"false","screenType":"Message","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"UIMESSAGE_WARN"},{"props":{"label":"Justification","length":"1000 ","visible":"true","datatype":"SYM","readOnly":"false","required":"true","screenType":"Long Text","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"//ZTEXTJUSTIFICATION/STRUCT.CONC_LINES"},{"props":{"label":"Additional Information","length":"1000 ","visible":"true","datatype":"SYM","readOnly":"false","required":"false","screenType":"Long Text","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"//ZTEXTADDITIONALINFO/STRUCT.CONC_LINES"}],"scrnid":"ITC_ML_VPN"},"message":{"type":"S","message":"Successfully read service model"}}
      } 
      if (params.attributes.formContent){
        form_content = params.attributes.formContent
      }
      if(form_content == "")
          form_content = {"data":{"conds":[{"condid":"FORM_NOTE","actions":[{"prop":"value","value":"W_NOTE","fieldId":"UIMESSAGE_INSTRUC"}],"evalCond":"ACT = 'REV'"},{"condid":"FORM_WARNING","actions":[{"prop":"value","value":"W_NOTE","fieldId":"UIMESSAGE_WARN"}],"evalCond":"ACT = 'GRANT'"},{"condid":"VIEW_MODE","actions":[{"prop":"readOnly","value":"true","fieldId":"ACT"}],"evalCond":"'INIT' <> 'INIT'"}],"fields":[{"props":{"label":"Action Required","domain":[{"value":"GRANT","csticKey":"ACT","description":"Grant Access"},{"value":"REV","csticKey":"ACT","description":"Revoke Access"}],"length":"30 ","visible":"true","datatype":"SYM","readOnly":"false","required":"true","screenType":"Single Select","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"true"},"fieldId":"ACT"},{"props":{"label":"Note:","domain":[{"value":"W_NOTE","csticKey":"UIMESSAGE_INSTRUC","description":"ISA/ASIA Approval is required"}],"length":"30 ","visible":"true","datatype":"SYM","readOnly":"false","required":"false","screenType":"Message","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"UIMESSAGE_INSTRUC"},{"props":{"label":"Warning:","domain":[{"value":"W_NOTE","csticKey":"UIMESSAGE_WARN","description":"Department Manager Approval is required"}],"length":"30 ","visible":"true","datatype":"SYM","readOnly":"false","required":"false","screenType":"Message","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"UIMESSAGE_WARN"},{"props":{"label":"Justification","length":"1000 ","visible":"true","datatype":"SYM","readOnly":"false","required":"true","screenType":"Long Text","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"//ZTEXTJUSTIFICATION/STRUCT.CONC_LINES"},{"props":{"label":"Additional Information","length":"1000 ","visible":"true","datatype":"SYM","readOnly":"false","required":"false","screenType":"Long Text","multipleValues":"false","allowsOtherValues":"false","displayAllOptions":"false"},"fieldId":"//ZTEXTADDITIONALINFO/STRUCT.CONC_LINES"}],"scrnid":"ITC_ML_VPN"},"message":{"type":"S","message":"Successfully read service model"}}

      formResults[0] = {
        scrnid: screen_id,
        formContent: form_content
      };
      //console.log(JSON.stringify(formResults));
      params.update.session.watsonContext.formResults = {};
      params.update.session.watsonContext.formResults = formResults;

      return Promise.resolve("DISPLAYFORM");

    } else { // if production
      
        // get form meta data
        // https://dcr.aramco.com.sa/sap/z_myit/service_def/1?sap-client=052
        const config = {
          headers: {
            'Content-Type': 'application/json',
            'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
          },
          method: "GET",
          url: endpoint + "/service_def/" +service_def,//
          //url: endpoint + "/service_def/" +service_def+ "?sap-client="+sap_client, // with client
          httpsAgent: agent
        }    

        var enpointResponse = axios.request(config).then((response) => {
          if (response.data.message.type==="S" || response.data.message.type==="W"){
            formResults[0] = {
              scrnid: screen_id,
              formContent: response.data.data
            };
            console.log(" * response data:",response.data.data)
            params.update.session.watsonContext.formResults = {};
            params.update.session.watsonContext.formResults = formResults;
            console.log(" * context:",JSON.stringify(params.update.session.watsonContext.formResults));
            //return the word DISPLAYFORM so then in the javascript if the DISPLAYFORM word is found it will recoginize that
            // render DISPLAYFORM
            return Promise.resolve("DISPLAYFORM");
          } else{
            console.log(response)
            return Promise.resolve(" * failed to retrive form definition ");
          }
        }).catch((error) => {
          console.log("* Error occured while calling endpoint ",error);
          return Promise.resolve("DISPLAYFORM");
        });
  
      return enpointResponse
    }
    
   
  }
};


// Action to submit form data to CRM services 
/**
 *   INPUT:  <submitForm/>
 *   OUTPUT: Message in UI
 */
// https://dcr.aramco.com.sa/sap/z_myit/srv_submit?sap-client=052

actions.submitForm = {
  controller: (params) => {
    console.log(" * submitForm action is triggered ... ");
    console.log(" * user_cookies:", params.update.user_cookies);

    var form_data = {}
    if (params.update.user_cookies.FORM_DATA){
      form_data = params.update.user_cookies.FORM_DATA
    }else {
      form_data = params.update.session.watsonContext.FORM_DATA
    }     
    console.log(" * form_data :", form_data);


    var success_msg = "The form was submited! Thanks";
    var fail_msg = "An error occured while submitting the request. Please notify administrator or try submit the request again later."
    
    if(config.environment==="development"){

      return Promise.resolve(success_msg);
    } else { // if production
      
        // submit data
        // https://dcr.aramco.com.sa/sap/z_myit/srv_submit?sap-client=052
        const config = {
          headers: {
            'Content-Type': 'application/json',
            'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
          },
          method: "POST",
          url: endpoint + "/srv_submit",//
          //url: endpoint + "/srv_submit/?sap-client="+sap_client, // with client
          httpsAgent: agent,
          data: form_data
        }    

        var enpointResponse = axios.request(config).then((response) => {
          if (response.data.message.type==="S" || response.data.message.type==="W"){
            console.log(" * response data:",response.data.data)
			mongodb.updateCountCRM({'_id':params.update.sessionId})

            return Promise.resolve(success_msg);
          } else{
            console.log(" * response data:", response.data.messages)
            return Promise.resolve(fail_msg);
          }
        }).catch((error) => {
          console.log("* Error occured while calling endpoint ",error);
          return Promise.resolve(fail_msg);
        });
  
      return enpointResponse
    }
    
  }
};

// Action to list the user’s IT assets
/**
 *   INPUT:  <displayAssets/>
 *   OUTPUT: Show Assets in UI
 */
// Description: The service provides a list of the user’s IT assets from the IT Assets Management System
actions.displayAssets = {
  controller: (params) => {
    console.log(" * displayAssets action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies);
    
    var listAssets = ""
    if(config.environment==="development"){ 
      var response = {"message": { "type": "S","message": "Assets retreived successfully" },"data": [{"aibn1": "AC332177","txt50": "Computer ATW-R310-ATW-1452 AUDIO TECHNIC","locationId": 734632756,"room": "R22","classDesc": "Copiers","city": "DHAHRAN","building": "3302 NORTH PARK 3","floor": "FLOOR 02"},{"aibn1": "BC123456","txt50": "Dell 17 inch Flat Panel Monitor Screen","locationId": 734632756,"classDesc": "Computer", "city": "DHAHRAN","building": "3302 NORTH PARK 3","floor": "FLOOR 02"}]}
      if (params.attributes.mode){
        console.log(" * mode:", params.attributes.mode);
        if (params.attributes.mode==="list"){
          for (var i=0;i<response.data.length;i++){
            listAssets = listAssets +"<li>"+ response.data[i].aibn1 + "</li>"
          }
          return Promise.resolve("<ul>"+listAssets+"</ul>");
        }
      } else { // display as buttons
        for (var i=0;i<response.data.length;i++){
          listAssets = listAssets + response.data[i].aibn1 + "/"
        } 
        listAssets = listAssets.slice(0,-1)
        return Promise.resolve("<buttons>"+listAssets+"</buttons>");
      }

    } else { // if production
      // https://dcr.aramco.com.sa/sap/z_myit/assets
      const config = {
        headers: {
          'Content-Type': 'application/json',
          'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
        },
        method: "GET",
        url: endpoint + "/assets",
        //url: endpoint + "/assets?sap-client="+sap_client, // with client
        httpsAgent: agent
      }     
      var enpointResponse = axios.request(config).then((response) => {
        console.log(" * enpoint response: ")
        console.log(response)
        if (response.data.message.type==="S" || response.data.message.type==="W"){
        
          if (params.attributes.mode){
            console.log(" * mode:", params.attributes.mode);
            if (params.attributes.mode==="list"){
              for (var i=0;i<response.data.data.length;i++){
                listAssets = listAssets +"<li>"+ response.data.data[i].aibn1 + "</li>"
              } 
              logger.info("<ul>"+listAssets+"</ul>")
              return Promise.resolve("<ul>"+listAssets+"</ul>");
            }
          }else { // display as buttons
            for (var i=0;i<response.data.data.length;i++){
              listAssets = listAssets + response.data.data[i].aibn1 + "/"
            } 
            listAssets = listAssets.slice(0,-1)
            logger.info("<buttons>"+listAssets+"</buttons>")
            return Promise.resolve("<buttons>"+listAssets+"</buttons>");
          }
        
        } else{
          console.log(response)
          return Promise.resolve(" * failed to retrive assets ");
        }
      }).catch((error) => {
        console.log("* Error occured while calling endpoint",error);
        return Promise.resolve(" * failed to retrive assets ");
      });

      return enpointResponse;
    }
    
  }
};

// Action display locations
/**
 *   INPUT:  <displayLocation/>
 *   OUTPUT: Show Location selection in UI
 */
// Description: The service provides a buildings/floors the user should choose from for IT_LOCATION fields in CRM services.
actions.displayLocation = {
  controller: (params) => {
    console.log(" * displayLocation action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies);
    if(config.environment==="development"){ 
      var response = {"message": {"type": "S","message": "Results retreived successfully"},"data": [{"locationId": "123", "building": "Building 1", "floor": "3", "city": "Ljubljana"},{"locationId": "456", "building": "Building 2", "floor": "12", "city": "Ljubljana"}]}
      params.update.session.watsonContext.location = response.data;
      return Promise.resolve("DISPLAYLOCATION");
    } else { // if production
      // https://dcr.aramco.com.sa/sap/z_myit/locations
      const config = {
        headers: {
          'Content-Type': 'application/json',
          'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
        },
        method: "GET",
        url: endpoint + "/locations",
        //url: endpoint + "/locations?sap-client="+sap_client, // with client
        httpsAgent: agent
      }     
      var enpointResponse = axios.request(config).then((response) => {
        console.log(" * enpoint response: ")
        console.log(response)
        if (response.data.message.type==="S" || response.data.message.type==="W"){
          // TODO location Selection UI element
          const userId = params.update.sender.id;
          params.update.session.watsonContext.location = response.data.data;
          return Promise.resolve("DISPLAYLOCATION");
        } else{
          logger.error(response)
          return Promise.resolve(" * failed to provide locations");
        }
      })
      .catch((error) => {
        console.log("* Error occured while calling endpoint",error);
        return Promise.resolve(" * failed to provide locations");
      });

      return enpointResponse;

    }

  }
};
//-----------------

// Action email to IT Help Desk
/**
 *   INPUT:  <emailIT summary="summary" desc="description"/>
 *   OUTPUT: Show notification in UI
 */
// https://dcr.aramco.com.sa/sap/z_myit/locations
// URI: /z_myit/locations
// Description: The service provides a buildings/floors the user should choose from for IT_LOCATION fields in CRM services.

actions.emailIT = {
  controller: (params) => {
    console.log(" * emailIT action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies)
    var parameters = {
        summary:params.attributes.summary,
        desc:params.attributes.desc
    };
    var response = ""
    if(config.environment==="development"){ 
      response = {"message": { "type": "S","message": "email was sent succesfully to IT Help Desk" }}
      return Promise.resolve("email was sent succesfully to IT Help Desk");
    } else { // if production

    // The service can be used to send an email to IT Help Desk
    // https://dcr.aramco.com.sa/sap/z_myit/support
    const config = {
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
      },
      method: "POST",
      url: endpoint + "/support",//
      //url: endpoint + "/support" + "?sap-client="+sap_client, // with client
      httpsAgent: agent,
      data: parameters
    }     
    var enpointResponse = axios.request(config).then((response) => {
        logger.info(" * enpoint response: ")
        //logger.info(response)
		logger.info(response.data.data)
        if (response.data.message.type==="S" || response.data.message.type==="W"){
          //const userId = params.update.sender.id; // alternative output 
          //params.bot.sendTextMessageTo("sending email to IT Help Desk", userId); // alternative output 
		  	mongodb.updateCountRemedy({'_id':params.update.sessionId})
          return Promise.resolve("email was sent succesfully to IT Help Desk");
        } else{
          logger.error(response)
          return Promise.resolve("failed to send the email to IT Help Desk");
        }
      }).catch((error) => {
        console.log("* Error occured while calling endpoint ",error);
        return Promise.resolve("failed to send the email to IT Help Desk");
      });

      return enpointResponse
    }
    
  }
};
//-----------------

// Action request status  
// The service provides a list of service requests for the user and their status
/**
 *   INPUT:  <requestStatus/>
 *   OUTPUT: Show Location selection in UI
 */
actions.requestStatus = {
  controller: (params) => {
    console.log(" * requestServices action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies);
    var response = ""
    if(config.environment==="development"){ 
      response = {"message": { "type": "S","message": "status" }}
      // TODO implement Status Message
      return Promise.resolve("REQUESTSTATUS");
    } else { // if production
      // The service can be used to send an email to IT Help Desk
      // https://dcr.aramco.com.sa/sap/z_myit/reqs
      const config = {
        headers: {
          'Content-Type': 'application/json',
          'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
        },
        method: "GET",
        url: endpoint + "/reqs",
        //url: endpoint + "/reqs?sap-client="+sap_client, // with client
        httpsAgent: agent
      }     
      var enpointResponse = axios.request(config).then((response) => {
        console.log(" * enpoint response: ")
        console.log(response)
        if (response.data.message.type==="S" || response.data.message.type==="W"){
          const userId = params.update.sender.id; // alternative output 
          params.bot.sendTextMessageTo(response.data.data, userId); 
          return Promise.resolve("REQUESTSTATUS");
        } else{
          console.log(response)
          return Promise.resolve("failed to provide request status");
        }
      })
      .catch((error) => {
        console.log("* Error occured while calling endpoint ",error);
        return Promise.resolve("failed to provide request status");
      });

      return enpointResponse;
    }
   
  }
};

//-----------------

// Action request UserID location
/**
 *   INPUT:  <getUserLocation/>
 *   OUTPUT: Show Location 
 */
actions.getUserLocation = {
  controller: (params) => {
    console.log(" * getUserLocation action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies);
    const userId = params.update.sender.id;
    console.log(" * context:", params.update.session.watsonContext);
    var parameters = {
      "query": params.update.session.watsonContext.UserNetworkid
    };
    console.log(" * parameters:", parameters);
    var response = ""
    if(config.environment==="development"){ 
      response = {"message": {"type": "S","message": "Results retreived successfully"},"data": [{"empFullname": " ALSHOSHAN, ABDULLAH ABDULRAHMAN (AAA) - ALSHOSAA","empFirstName": "ABDULLAH","empLastName": "ALSHOSHAN","empMidName": "ABDULRAHMAN","empNetworkid": "ALSHOSAA","empOrgcode": "30039426","empOrgtitle": "INTEGRATED MGMT SOLUTIONS GROUP","empTitle": "IT SYSTEMS ANALYST IV","empBusPriPhone": "+966-13-8800329","empBusMobPhone": "966 506005550","empPerMobPhone": "966 506005550","empBusAddress": "C-B2113, Floor 2, B Wing, North Park 3 (Bldg. 3302), Dhahran, Kingdom Of Saudi Arabia"}]}
      Promise.resolve(response);
    } else { // if production
      // https://dcr.aramco.com.sa/sap/z_myit/phonebook
      const config = {
        headers: {
          'Content-Type': 'application/json',
          'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
        },
        method: "POST",
        url: endpoint + "/phonebook",//
        //url: endpoint + "/phonebook" + "?sap-client="+sap_client, // with client
        httpsAgent: agent,
        data: parameters
      }     
      var enpointResponse = axios.request(config).then((response) => {
          console.log(" * enpoint response: ")
          console.log(response)
          if (response.data.message.type==="S" || response.data.message.type==="W"){
            params.bot.sendTextMessageTo(response.data.data, userId); 
            //params.update.session.watsonContext.UserAddress = response.data.data[0]
            //console.log(" * user data:", params.update.session.watsonContext.UserAddress)
        } else{
          console.log(response)
          Promise.resolve("failed to provide user location");
        }
        return Promise.resolve();
      })
      .catch((error) => {
        console.log("* Error occured while calling endpoint ",error);
        return Promise.resolve("failed to provide user location");
      });

      return enpointResponse;
    }
    
  }
};


 actions.listEnrolledDevices = {
  controller: (params) => {
    console.log(" * CRM Mobility Enrolled Device check action is triggered ... ")
    console.log(" * user_cookies:", params.update.user_cookies);
    
    var response = ""
    if(config.environment==="development"){ 
      //response = { "message": { "type": "S", "message": "Successfully called MDM API" }, "data": [{ "deviceFriendlyName": "alshosaa iPhone iOS 14.4.2 0D95", "model": "iPhone", "platform": "Apple", "operatingSystem": "14.4.2", "complianceStatus": "Compliant" }, { "deviceFriendlyName": "Example 1", "model": "Example model", "platform": "Sth", "operatingSystem": "Niceone", "complianceStatus": "Who knows" } ] }
      response = { "message": { "type": "S", "message": "Successfully called MDM API" }, "data": [] }

      var listOfDevices = "";
      if(response.data.length == 0){
        return Promise.resolve("No devices enrolled under enterprise mobility");
      }else{
      for (var i=0;i<response.data.length;i++){
        listOfDevices = listOfDevices +"<li>Device name: "+ response.data[i].deviceFriendlyName + "</li><ul>";
        listOfDevices = listOfDevices +"<li>Model: "+ response.data[i].model + "</li>";
        listOfDevices = listOfDevices +"<li>Platform: "+ response.data[i].platform + "</li>";
        listOfDevices = listOfDevices +"<li>Operating System: "+ response.data[i].operatingSystem + "</li>";
        listOfDevices = listOfDevices +"<li>Compliance Status: "+ response.data[i].complianceStatus + "</li></ul>";
      }
      return Promise.resolve("The devices enrolled under Enterprise Mobility are: <ul>"+listOfDevices+"</ul>");
    }
    } else { // if production
      // https://dcr.aramco.com.sa/sap/z_myit/phonebook
      const config = {
        headers: {
          'Content-Type': 'application/json',
          'Cookie': 'MYSAPSSO2='+ params.update.user_cookies.MYSAPSSO2
        },
        method: "GET",
        url: endpoint + "/mdm",
        httpsAgent: agent,
      }     
      var enpointResponse = axios.request(config).then((response) => {
          console.log(" * enpoint response: ")
          console.log(response)
          if (response.data.message.type==="S"){
            var listOfDevices = "";
            if(response.data.data.length == 0){
              return Promise.resolve("No devices enrolled under enterprise mobility");
          }
            else{
              for (var i=0;i<response.data.data.length;i++){
                listOfDevices = listOfDevices +"<li>Device name: "+ response.data.data[i].deviceFriendlyName + "</li><ul>";
                listOfDevices = listOfDevices +"<li>Model: "+ response.data.data[i].model + "</li>";
                listOfDevices = listOfDevices +"<li>Platform: "+ response.data.data[i].platform + "</li>";
                listOfDevices = listOfDevices +"<li>Operating System: "+ response.data.data[i].operatingSystem + "</li>";
                listOfDevices = listOfDevices +"<li>Compliance Status: "+ response.data.data[i].complianceStatus + "</li></ul>";
              }
            return Promise.resolve("The devices enrolled under Enterprise Mobility are: <ul>"+listOfDevices+"</ul>"); 
            }
        } else if(response.data.message.type==="E" && response.data.message.message === "Error on messaging http header"){
          return Promise.resolve("No devices enrolled under enterprise mobility");
        }
		else{
			console.log(response)
          return Promise.resoSlve("Failed to provide the CRM mobility enrlolled device check");
		}
		
        return Promise.resolve();
      })
      .catch((error) => {
        console.log("* Error occured while calling endpoint ",error);
        return Promise.resolve("Failed to provide the CRM mobility enrlolled device check");
      });

      return enpointResponse;
    }
    
  }
};


module.exports = actions;


// API endpoints for testing

// basic authorisation or cookies


// Get the Service Catalog:
// https://dcr.aramco.com.sa/sap/z_myit/cat?sap-client=052


// Get metadata for CRM services 
// Note: serviceCode is part of the catalog –example below is for serviceCode = 1 VPN Access
// https://dcr.aramco.com.sa/sap/z_myit/service_def/1?sap-client=052


// Submit new service requests (CRM)
// https://dcr.aramco.com.sa/sap/z_myit/srv_submit?sap-client=052
// Sample JSON payload{"SERVICE_ID":"ITC_ML_VPN","values":[{"fieldId":"ACT","value":"REV"},{"fieldId":"UIMESSAGE_INSTRUC","value":"W_NOTE"},{"fieldId":"//ZTEXTJUSTIFICATION/STRUCT.CONC_LINES","value":"test justification"}]}
// Method: POST


// Get user’s IT assets - get specific user assets
// https://dcr.aramco.com.sa/sap/z_myit/assets?sap-client=052


// Get users location --- >>> need to clarify >> provides all locations
// https://dcr.aramco.com.sa/sap/z_myit/locations?sap-client=052
// Description: The service provides a buildings/floors the user should choose from for IT_LOCATION fields in CRM services.


// Get users request status  
// https://dcr.aramco.com.sa/sap/z_myit/reqs?sap-client=052


// Get user’s managed devices - Mobile no need 
// https://dcr.aramco.com.sa/sap/z_myit/mdm?sap-client=052


// Email to IT Help Desk
// The service can be used to send an email to IT Help Desk
// https://dcr.aramco.com.sa/sap/z_myit/support?sap-client=052
// Method: POST
//body
//{
//  "summary":"summary from postman",
//  "desc":"desc from postman"
//}