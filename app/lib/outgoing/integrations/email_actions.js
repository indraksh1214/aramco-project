/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

const actions = require("botmaster-fulfill-actions");
const config = require("../../../../config/config.js");

const logger = require("../../../utils/logger/logger");



//SMTP mail server
/*var smtpTransport = require("nodemailer-smtp-transport");
var transporter = nodemailer.createTransport(smtpTransport({
    host : config.mailer.originHost,
    port: config.mailer.originPort,
    auth : {
        user : config.mailer.originUser,
        pass : config.mailer.originPass
    }
}));*/

// or for the gmail SMPT
//var transporter = nodemailer.createTransport('smtps:'+config.mailer.originUser.+'@gmail.com:'+config.mailer.originPass.+'@smtp.gmail.com');

/*
exports.sendEmail = function(resultHolder, callback) {

  //load template
  var body = template.read(config);

  //update template with new values
  var now = new Date();
  var dateNow = dateTime.format(now, 'DD.MM.YYYY');
  var timeNow = dateTime.format(now, 'HH:mm:ss');

  body = body.replace("[date]", dateNow);
  body = body.replace("[time]", timeNow);

  //callDateTime
  var callDate = resultHolder.session.context.callZSMDate;
  var callHours = resultHolder.session.context.callZSMHours;
  body = body.replace("[callDate]", callDate);
  body = body.replace("[callHours]", callHours);
  //callNumber - should be asked and extracted from the dialog
  body = body.replace("[callNumber]", resultHolder.session.context.callZSMPhone);
  //prefix - she/he/it/..., should be asked and/or extracted from the dialog
  body = body.replace("[prefix]", "Frau/Herr");
  //secondName - should be asked and extracted from the dialog
  body = body.replace("[fullName]", resultHolder.session.context.callZSMfullName);

  //styling of cells in table
  var style = "border: 1px solid black;border-collapse: collapse;padding: 10px;"
  //forming the chat history
  var count = resultHolder.session.messageId;
  var content = "<tr><th style='" + style + "'>Nr.</th><th style='" + style + "'>" + resultHolder.session.context.callZSMfullName + "</th><th>Eva</th><th style='" + style + "'>Erstellt</th></tr>";

  if (count >= 0 && resultHolder.session.feedback !== undefined) {
    for (var i = 0; i < count; i++) {
      var feedback = resultHolder.session.feedback[i];
      content += "<tr>"
      content += "<td style='" + style + "'>" + feedback.messageId + "</td>";
      content += "<td style='" + style + "'>" + feedback.question + "</td>";
      content += "<td style='" + style + "'>" + feedback.answer + "</td>";
      var createdDT = new Date(feedback.created);
      content += "<td style='" + style + "'>" + dateTime.format(createdDT, 'DD.MM.YYYY HH:mm:ss') + "</td>";
      content += "</tr>"
    }
  }

  body = body.replace("[chatHistory]", content);

  //set mail options
  var mailOptions = {
    from: config.mailer.originEmail,
    to: config.mailer.targetEmail,
    subject: config.mailer.emailTopic,
    html: body
  };

  resultHolder.debug.mail.mailOptions = mailOptions;

  //prepare transporter
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.mailer.originUser,
      pass: config.mailer.originPass
    }
  });
  //send the email
  transporter.sendMail(mailOptions, function(error, info) {
    if (error) {
      callback(error, resultHolder);
    } else {
      callback(null, resultHolder)
    }
  });
}
*/

//--------------------Email integration---------------------
// Get the Service Catalog: 
/**
 *   INPUT:  <email to="support@company.com" topic="Email Topic" body=" email text "/>
 *   OUTPUT: Send email and Show notification in UI
 */
/*
var dateTime = require('date-and-time');
var nodemailer = require('nodemailer');
var template = require('./template');
*/

actions.email = {
  controller: (params) => {

    const topic = params.attributes.topic;
    const to = params.attributes.to;
    const body = params.attributes.body;


    //TO DO Emailing
    // send Email
    // Pass  notification to UI

    logger.debug(params.update.message);
    return Promise.resolve("EMAIL");

  }
};

module.exports = actions;
