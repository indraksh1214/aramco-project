const AuthorizationV1 = require('ibm-watson/authorization/v1');
const { CloudPakForDataAuthenticator } = require('ibm-watson/auth');

const mountS2TEndpoints = function (app) {
  if (process.env.STT_URL && process.env.STT_MODEL && process.env.CP4D_USERNAME && process.env.CP4D_PASSWORD) {
    app.get("/api/speech-to-text/properties", function (req, res) {
      new AuthorizationV1({
        authenticator: new CloudPakForDataAuthenticator({
          username: process.env.CP4D_USERNAME,
          password: process.env.CP4D_PASSWORD,
          url: process.env.CP4D_URL,
          disableSslVerification:true
        }),
        url: process.env.STT_URL
      }).getToken(function (err, token) {
        if (err) {
          console.log("Error retrieving token: ", err)
          res.status(500).send(err)
        } else {
          res.json({
            access_token: token,
            url: process.env.STT_URL,
            model: process.env.STT_MODEL,
            language_customization: process.env.STT_LANGUAGE_CUSTOMIZATION || null,
            acoustic_customization: process.env.STT_ACOUSTIC_CUSTOMIZATION || null,

          })
        }
      })
    })
  }
}

module.exports = mountS2TEndpoints;