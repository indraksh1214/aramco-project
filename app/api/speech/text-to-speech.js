const AuthorizationV1 = require('ibm-watson/authorization/v1');
const { CloudPakForDataAuthenticator } = require('ibm-watson/auth');

const mountT2SEndpoint = function (app) {
  if (process.env.TTS_URL && process.env.TTS_VOICE && process.env.CP4D_USERNAME && process.env.CP4D_PASSWORD) {
    app.get("/api/text-to-speech/properties", function (req, res) {
      new AuthorizationV1({
        authenticator: new CloudPakForDataAuthenticator({
          username: process.env.CP4D_USERNAME,
          password: process.env.CP4D_PASSWORD,
          url: process.env.CP4D_URL,
          disableSslVerification:true
        }),
        url: process.env.TTS_URL
      }).getToken(function (err, token) {
        if (err) {
          console.log("Error retrieving token: ", err)
          res.status(500).send(err)
        } else {
          res.json({
            access_token: token,
            url: process.env.TTS_URL,
            customization: process.env.TTS_CUSTOMIZATION || null,
            voice: process.env.TTS_VOICE,
          })
        }
      })
    })
  }
}

module.exports = mountT2SEndpoint;