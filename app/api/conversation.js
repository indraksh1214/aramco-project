/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

"use strict";

module.exports = function (app) {
  const Botmaster = require("botmaster");
  const SocketioBot = require("./botmaster-socket.io/socket.io_bot");
  const {fulfillOutgoingWare} = require("botmaster-fulfill");
  const SessionWare = require("botmaster-session-ware");
  const WatsonConversationWare = require("./botmaster-watson-assistant/botmaster-wa-v2.js");
  // const {bootstrap} = require('botmaster-button');
  
  const botmaster = new Botmaster({ server: app });

  const incomingMiddleware = require("../lib/incoming");
  const outgoingMiddleware = require("../lib/outgoing");

  // Load config module
  const config = require("../../config/config");

  // functions to generate logs
  const logger = require("../utils/logger/logger.js");

  const socketioSettings = {
    id: config.secretBotId,
    server: app, // this is required for socket.io. You can set it to another node server object if you wish to. But in this example, we will use the one created by botmaster under the hood
  };
  const socketioBot = new SocketioBot(socketioSettings);
  botmaster.addBot(socketioBot);

  try {
    const watsonAssistantRawOutput = WatsonConversationWare(config.watsonConversationWareOptions);

    /* **********************************************************************************
_______________________>>>>____Incoming ______>>>>>__________________________________

        Incoming middleware gets called in order of declaration whenever a valid update 
        from one of the bots added to botmaster (using botmaster.addBot()) is received.
_____________________________________________________________________________________
*************************************************************************************/

    // botmaster.use(incomingMiddleware.commands.performFoundCommands);
    botmaster.use(watsonAssistantRawOutput);
    botmaster.use(incomingMiddleware.getAnswerStoreContent.getAnswerStoreContent);
    botmaster.use(incomingMiddleware.reply.replyToUser);
  } catch (err) {
    logger.error("conversation.js-incommings", err.stack);
  }
  /* **********************************************************************************
_____________________<<<<<<_____Outgoing ______<<<<<<<_______________________________

        Outgoing middleware gets called in order of declaration whenever you send a 
        message using one of the bot's sendMessage methods.
_____________________________________________________________________________________
*************************************************************************************/
  //Declaring BotMaster Actions
  const actions = require("../lib/outgoing/actions");
  /** Commented by sankeeta */
  botmaster.use(fulfillOutgoingWare({
      actions: actions,
      responseTransformer: ({ bot, message, update, next }) => {
        logger.debug("This is the message text in the outgoing: " + message.message.text);
        logger.debug(update.watsonUpdate);
        console.log("This is the message text in the outgoing: " + message.message.text);
        console.log(update.watsonUpdate);
        next();
      }
    }));
 

  botmaster.use(outgoingMiddleware.debugPayload.savePayloadForDebug);
  botmaster.use(outgoingMiddleware.saveConversation.saveConversation);
  // add sendIsTypingMessageTo timer function
  botmaster.use(outgoingMiddleware.sendIsTyping.sendIsTyping);

  // Needed for Watson Assistant V2 handling to pass sessionId to client
  botmaster.use(WatsonConversationWare.passSessionId);


  /* ***********************************************************************
_________________________SESSION HANDLING _________________________________

         SESSION HANDLING ALWAYS _BELOW_ ALL THE USE MIDDLEWARES
__________________________________________________________________________
**************************************************************************/

  // Add session ware
  const sessionWare = SessionWare();
  botmaster.useWrapped(sessionWare.incoming, sessionWare.outgoing);

  // Log messages
  botmaster.on("listening", (message) => {
    logger.info("conversation-message", message);
  });

  // Catch errors and log them
  botmaster.on("error", (bot, err, update) => {
    logger.error(bot.type);
    logger.error(err.stack);

    var errorText = "";
    if (process.env.NODE_ENV === "production") {
      console.log(" * Server side error:")
      console.log(err)
      errorText =
        "Server side error is occured. Please notify adminstrator.";
    } else {
      errorText = "😞 \n " + err.stack;
    }

    const errorMessage = {
      recipient: {
        id: update.sender.id,
      },
      message: { text: errorText },
      errorStack: { error: err.stack },
    };

    bot.sendMessage(errorMessage);
  });
};
