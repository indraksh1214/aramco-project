/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

var AssistantV2 = require("ibm-watson/assistant/v2");
const { IamAuthenticator } = require("ibm-watson/auth");
const { BearerTokenAuthenticator } = require("ibm-watson/auth");
const logger = require("../../utils/logger/logger.js");
const mongodb = require("../../utils/db/mongodb.js");


const sendMessageToWatson = async (params, number_of_retry) => {

  const messageForWatson = {
    assistantId: params.assistantId,
    sessionId: params.sessionId,
    input: {
      message_type: "text",
      text: params.text,
      options: {
        return_context: true,
      },
    },
    context: {
      skills: {
        "main skill": {
          user_defined: params.context || {},
        },
      },
    },
  };

  logger.debug("Seding message to Watson Assistant:", messageForWatson);
  //messageForWatson.context.skills["main skill"].user_defined = {"UserName" : "Emilio"};
  if(params.user_cookies && params.user_cookies["USER_PROFILE"] && isJson(params.user_cookies["USER_PROFILE"])){
    var user_info = JSON.parse(params.user_cookies["USER_PROFILE"]);
    messageForWatson.context.skills["main skill"].user_defined = {
      "UserName" : user_info["givenname"],
      "UserNetworkid" : user_info["networkId"],
      "UserAddress" : user_info["officeLocation"],
      "UserAddress_frontend": user_info["officeLocation_frontend"],
      "UserEmail" : user_info["email"],
      "UserBadgeNo" : user_info["badgeN"]
    }
  }


  try {
    const { result } = await params.watson.message(messageForWatson);
    logger.debug("Received response from Watson Assistant:", result);
    /*if(Math.random() > 1){
      console.log("Opsie, an exception is being triggered");
      throw new Error('This is a simulation, what happens in case of exception');
    }*/
    return result;
  } catch (error) {
    logger.error("Failed to get reply from Watson Assistant:", error);
    logger.error("Number of retry: " + number_of_retry);
    
    if(JSON.parse(error.body)["error"] === "Invalid Session"){
      // return invalid session token to frontend
      return  {"output":{"generic":[{"response_type":"text","text":"invalid_session"}]},"context":{"skills":{"main skill":{}}}}
    }
    else if(number_of_retry <= 5){
      return sleep(2000).then(() => { return sendMessageToWatson(params, number_of_retry+1) });
      //return sendMessageToWatson(params, number_of_retry+1);
    }  else{
      throw error
    }
  }
};

const WatsonConversationWare = (options) => {
  if (!options || !options.settings || !options.workspaceId) {
    throw new Error(
      "In order to create a watson conversation middleware, " +
        "you need to pass in options that contain settings and workspaceId keys"
    );
  }

  let WAauthenticator;
  if (process.env.CONVERSATION_APIKEY) {
    WAauthenticator = new IamAuthenticator({
      apikey: process.env.CONVERSATION_APIKEY
    }
	);
  } else if (process.env.CONVERSATION_BEARER_TOKEN) {
    WAauthenticator = new BearerTokenAuthenticator({
      bearerToken: process.env.CONVERSATION_BEARER_TOKEN,
      url:process.env.CP4D_URL
    });
  }

  const watson = new AssistantV2({
    version: options.settings.version,
    authenticator: WAauthenticator,
    serviceUrl: options.settings.url,
	disableSslVerification:true
  });


  const watsonConversationWareController = async (bot, update) => {
    // If no session is started yet, create a new session and store in update
    if (!update.sessionId) {
      try {
        let { result } = await watson.createSession({
          assistantId: options.assistantId,
        });
        update.sessionId = result.session_id;
        logger.info("Created new Watson Assistant session", update.sessionId);
		
		//save to database mongodb
		 
        //var timer = setInterval(()=>{
			//if(update.user_cookies.USER_PROFILE && update.user_cookies.USER_PROFILE != 'none'){
				var user = JSON.parse(decodeURIComponent(update.user_cookies.USER_PROFILE));
				var row = {
					'_id': update.sessionId,
					'networkId': user.networkId,
					'organization': user.organization,
					'department': user.department,
					'businessline': user.businessline,
					'employeetype': user.employeetype,
					'timestamp': new Date()
				};
				mongodb.saveToUserLogs(row);
			//	clearInterval(timer);
			//}
		//},1000)
		
	  } catch (error) {
        logger.error("Unable to create Watson Assistant session", error);
        throw error;
      }
    }

    // If there is no message, return
    if (!update.message || !update.message.text) {
      logger.debug("Got an update with no text, not sending it to Watson");
      return Promise.resolve();
    }

    // Send the message to Watson Assistant
    // Change the context to pass in-memory session data to Watson Assistant
    update.watsonUpdate = await sendMessageToWatson({
      context: update.session.watsonContext||{},
      sessionId: update.sessionId,
      text: update.message.text,
      watson,
      assistantId: options.assistantId,
      user_cookies: update.user_cookies
    },0);
   
    update.watsonUpdate.user_cookies = update.user_cookies

    // For compatibility with Watson Assistant V1 middleware:
    const watsonUpdate = update.watsonUpdate;
    watsonUpdate.context =
      watsonUpdate.context.skills["main skill"].user_defined;

      //to avoid the first message with an empty context
      if(watsonUpdate.context == undefined){
        watsonUpdate.context ={}
      }
    update.session.watsonContext = watsonUpdate.context;
 

    // Store response and WA instance in update object
    update.watsonConversation = watson;
  };

  return {
    type: "incoming",
    name: "watson-conversation-middleware",
    controller: watsonConversationWareController,
  };
};

WatsonConversationWare.passSessionId = {
  type: "outgoing",
  name: "watson-assistant-pass-session-id",
  controller: (bot, update, message, next) => {
    if (update) {
      message.sessionId = update.sessionId;
      next();
    }
    next();
  },
};

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}






module.exports = WatsonConversationWare;
