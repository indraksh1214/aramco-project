const { reject } = require('lodash');
const { resolve } = require('url');
const logger = require("../../utils/logger/logger");
const date = require('date-and-time');
const axios = require('axios').default;
const service_request_status_values = {
    "R":"Requested",
    "Rev":"Reviewed but On Hold" ,
    "Rej":"Rejected",
    "A":"Approved",
    "AA":"With Design",
    "I":"Issued and In Process",
    "HP":"On Hold for Parts", 
    "HA":"On Hold for Access",
    "HL":"On Hold for Labo",
    "S":"Stopped",
    "Can":"Cancelled",
    "Com":"Completed",
    "Clo":"Closed"
}
/** Connect To ARCHIBUS service */
exports.archibusService = (request)=>{
    return new Promise(async (resolve, reject) => {
        console.log('request : ',request);
        console.log('request.params : ',request.params);
        try{
            const response = await axios({
                method: request.method,
                url: `${process.env.API_URL_ARAMCO}${request.path}`,
                headers: {
                    token:`${process.env.API_TOKEN_ARAMCO}`,
                    username:request.username
                },
                params: request.params,
                data:request.body
            });
            console.log('response : ',response);
            logger.info('API response status: ',response.status)
            logger.info('API response config headers : ',response.config.headers)
            logger.info('API response config method : ',response.config.method)
            logger.info('API response config url : ',response.config.url)
            logger.info('API response config params : ',response.config.params)
            logger.info('API response config data : ',response.config.data)
            if(response.status!=200){
                
                throw(response)
            }
            else{
                resolve(response.data);
            }
        }
        catch(err){
            logger.info('API err : ',err)
            console.log(err);
            if(err.response.status==404){
                // message = message;
                resolve({status:404});
            }
            reject(err);
        }
    })
}

/**Get details for previously submitted requests */
exports.getServiceDetails = (request)=>{ 
    /** Request should conatin username ,service_request_id,*/
    console.log('request : ',request);
    return new Promise(async(resolve,reject)=>{
        try {
            let service_request_id =  request.service_request_id;
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
            
            request.path = `data?`
            request.method = `get`
            request.params = {
                dataSource:'Api_MaintenanceRequests_v1',
                filter:`[{"fieldName":"activity_log_id","filterValue":"${service_request_id}","filterOperation":"="}]`
            }
            let response = await this.archibusService(request);
            resolve(response);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}

/**Get details for previously submitted requests */
exports.getAllActiveRequest = (request)=>{
    /** Request should conatin username ,employee id,*/
    console.log('request : ',request);
    return new Promise(async(resolve,reject)=>{
        try {
            
            request.path = `data?`
            request.method = `get`
            request.params = {
                dataSource:'Api_MaintenanceRequests_v1',
                filter:`[ {"fieldName":"requestor","filterValue":"${request.employee_id}","filterOperation":"="}, {"fieldName":"status","filterValue":"S","filterOperation":"!="}, {"fieldName":"status","filterValue":"Can","filterOperation":"!="}, {"fieldName":"status","filterValue":"Clo","filterOperation":"!="}]`
            }
            let response = await this.archibusService(request);
            resolve(response);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}

/**Cancel requests in Archibus that have not yet been issued */
exports.cancelServiceRequest = (request)=>{
    /** Request should conatin username ,service_request_id,work_request_id,comment*/
    return new Promise(async(resolve,reject)=>{
        try {
            let service_request_id =  request.service_request_id;
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
            request.path = `maintenance/work-requests/${request.work_request_id}`
            request.method = `DELETE`
            request.params = {}
            request.body = { 
                "wr_id": request.work_request_id, 
                "activity_log_id": service_request_id, 
                "wr_step_waiting.comments": request.comment 
                } 
            let response = await this.archibusService(request);
            resolve(true);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}

/**Find out if logged in user have pending satisfaction survey steps*/
exports.getSatisfactionSurveySteps = (request)=>{ 
    /** Request should conatin username,employee_id,role,work_request_id */
    return new Promise(async(resolve,reject)=>{
        try {
            // let service_request_id =  request.service_request_id.sanitize();
            let filter;
            if(request.work_request_id){
                filter = `[ 
                    {"fieldName":"step_type","filterValue":"survey","filterOperation":"="}, 
                    {"fieldName":"wr_id","filterValue":"${request.work_request_id}","filterOperation":"="}, 
                    {"fieldName":"user_name","filterValue":"${request.username}","filterOperation":"="}, 
                    {"fieldName":"step_type","filterValue":"survey","filterOperation":"=","relativeOperation": "OR"}, 
                    {"fieldName":"wr_id","filterValue":"${request.work_request_id}","filterOperation":"="}, 
                    {"fieldName":"em_id","filterValue":"${request.employee_id}","filterOperation":"="}, 
                    {"fieldName":"step_type","filterValue":"survey","filterOperation":"=","relativeOperation": "OR"}, 
                    {"fieldName":"wr_id","filterValue":"${request.work_request_id}","filterOperation":"="}, 
                    {"fieldName":"role_name","filterValue":"${request.role}","filterOperation":"="}
                ]` 
            }
            else{
                filter = `[ 
                    {"fieldName":"step_type","filterValue":"survey","filterOperation":"="}, 
                    {"fieldName":"user_name","filterValue":"${request.username}","filterOperation":"="}, 
                    {"fieldName":"step_type","filterValue":"survey","filterOperation":"=","relativeOperation": "OR"}, 
                    {"fieldName":"em_id","filterValue":"${request.employee_id}","filterOperation":"="},
                    {"fieldName":"step_type","filterValue":"survey","filterOperation":"=","relativeOperation": "OR"},
                    {"fieldName":"role_name","filterValue":"${request.role}","filterOperation":"="} 
                ]` 
            }
            request.path = `data?`
            request.method = `GET`
            request.params = {
                dataSource:'Api_MaintenanceStepWaiting_v1',
                filter:filter
            
            }
            // request.body = { 
            //     "wr_id": request.work_request_id, 
            //     "activity_log_id": service_request_id, 
            //     "wr_step_waiting.comments": request.comment 
            //     } 
            let response = await this.archibusService(request);
            resolve(response);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}

/**Submit satisfaction Survey */
exports.submitSatisfactionSurvey = (request)=>{ 
    /** Request should conatin username,rating,review,work_request_id ,step_id */
    return new Promise(async(resolve,reject)=>{
        try {
            request.path = `maintenance/work-requests/${request.work_request_id}/saveSatisfaction`
            request.method = `PATCH`
            request.params = {
            }
            request.body = {

                "activity_log.activity_log_id":request.service_request_id, 

                "activity_log.satisfaction":request.rating, 

                "activity_log.satisfaction_notes":request.review, 

                "wr.wr_id":request.work_request_id, 

                "wr.satisfaction":request.rating, 

                "wr.satisfaction_notes":request.review, 

                "wr_step_waiting.step_log_id":request.step_id
                } 
            let response = await this.archibusService(request);
            console.log('response : ',response);
            resolve(true);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}

/**Read Service Request description */
exports.readServiceRequestDescription = (request)=>{
    /** Request should conatin username ,service_request_id*/
    return new Promise(async(resolve,reject)=>{
        try {
            let service_request_id =  request.service_request_id;
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
            request.path = `service-desk/requests/${request.service_request_id}`
            request.method = `GET`
            request.params = {
                fields:`description,wr_id`
            }
            request.body = { 
                } 
            let response = await this.archibusService(request);
            resolve(response);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}

/**Edit Service Request description */
exports.editServiceRequest = (request)=>{
    /** Request should conatin username ,service_request_id,description*/
    return new Promise(async(resolve,reject)=>{
        try {
            let service_request_id =  request.service_request_id;
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
            request.path = `service-desk/requests/${request.service_request_id}`
            request.method = `PATCH`
            request.params = {}
            request.body = { 
                "activity_log_id": service_request_id, 
                "description": request.description 
                } 
            let response = await this.archibusService(request);
            resolve(true);
            
        } catch (error) {
            reject(error);
        }
        
        
    })
}
/** */ 

exports.checkServiceRequestStatus = (userinfo,service_request_id)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number. Would you like to check for another ticket number?"
           
            const requests = await this.getServiceDetails({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(requests.length){
                let requestor = requests[0]['wr.requestor'];
                if(requestor!=userinfo.user_name && requestor!=userinfo.em_id){
                    status = 3;
                    message = `unauthorised_access`
                }
                else
                {    status = 1;
                    let request_status = requests[0]['wr.status'];
                    let service_request_status = service_request_status_values[request_status]
                    let date_scheduled_end = requests[0]['wr.date_escalation_completion'];
                    let date_scheduled=new Date(date_scheduled_end);
                    let estimated_date = 'not estimated' 
                    if(date_scheduled_end){
                        
                        estimated_date = new Date(date_scheduled).toLocaleDateString('en-us', {year:"numeric", month:"short",day: 'numeric'});
                    }
                    let today = new Date();
                    let artical = 'is'
                    if(service_request_status == 'N/A'){
                        
                        service_request_status = `not yet defined`;
                    }
                    else{
                        service_request_status = `<i>${service_request_status.toLowerCase().capitalize()}</i>`
                    }
                    if(today>date_scheduled){
                        artical = 'was'
                    }
                    message = `The status of the request is ${service_request_status} and the scheduled end ${artical} ${estimated_date}. Would you like to check for another status request?`
                }
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            // console.log(err);
            resolve({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
        }
    })
}

exports.getActiveRequest = (userinfo)=>{
    return new Promise(async (resolve, reject) => {
       
        try{
            let status = 0;
            let message = "Hmmm.., looks like there are no active tickets."
           
            const response = await this.getAllActiveRequest({
                username:userinfo.user_name,
                employee_id:userinfo.em_id
            });
            let res_len = response.length;
            if(res_len){
                ticket_number = 0;
                status = 1;
                let buttonsTag;
                if(res_len > 0){
                    message=`Here are the active service request. Which one do you want to start with?`
                    buttonsTag = `<buttons>`
                    response.forEach(element => {
                        let service_request_id = element["wr.activity_log_id"]
                        let prob_type = element["wr.prob_type"]
                        let date_requested=new Date(element["wr.date_requested"]);
                        let description = element["wr.description"]
                        buttonsTag += `Service request ${service_request_id} created on ${new Date(date_requested).toLocaleDateString('en-us', {year:"numeric", month:"short",day: 'numeric'})} for ${prob_type} # ${service_request_id} /`;
                 
                    });
                    console.log('buttonsTag : ',buttonsTag)
                    buttonsTag = buttonsTag.substring(0, buttonsTag.length - 1);
                    buttonsTag +=`</buttons>`;
                    
                
                }else{
                    let service_request_id = response[0]["wr.activity_log_id"]
                    ticket_number = service_request_id;
                    message=`Okay, you have only one ${service_request_id} active service request. Would you like to check the status?`
                    buttonsTag = `<buttons>Yes # ${service_request_id} / No # stop </buttons>`
                }
                message = message + buttonsTag
                
            }
            else{
                message = 'There are no active service requests'
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            // console.log(err);
            resolve({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
        }
    })
}


exports.cancelRequest = (userinfo,service_request_id,comment)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id;
        if(typeof service_request_id === "string"){
            service_request_id =  service_request_id.sanitize();
        }
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number.  Perhaps your ticket has already been cancelled, or you can reverify and try canceling again. "
           
            /** Pre check for cancalling */
            const responsePrecheck = await this.getServiceDetails({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(responsePrecheck && responsePrecheck.length){
                status = 1;
                let request_status = responsePrecheck[0]['wr.status'];
                let cost_total = responsePrecheck[0]['wr.cost_total'];
                let service_request_status = service_request_status_values[request_status]
                let requestor = responsePrecheck[0]['wr.requestor'];
                if(requestor!=userinfo.user_name && requestor!=userinfo.em_id){
                    status = 3;
                    message = `unauthorised_access`
                }
                else if(request_status == 'R'||'A'||'Rej'||'AA' && cost_total==0){
                    /** Cancel Request */
                    let work_request_id = responsePrecheck[0]['wr.wr_id'];
                    let responseCancelRequest = await this.cancelServiceRequest({
                        username:userinfo.user_name,
                        service_request_id:service_request_id,
                        work_request_id:work_request_id,
                        comment:comment
                    })
                    //logger.info('responseCancelRequest : ',responseCancelRequest);
                    console.log('responseCancelRequest : ',responseCancelRequest);
                    if(responseCancelRequest==true){
                        message = `Done! Your request has been canceled.`
                    }
                    else message = `Opps! There was an error while canceling your request. Please try again after some time.`

                }
                else{
                    /** This request cannot be canceled */
                    service_request_status = `<i>${service_request_status.toLowerCase().capitalize()}</i>`
                    message = `The status of the request is ${service_request_status}. Sorry, I cannot cancel this request.`
                }
                
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            console.log(err);
            resolve({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
        }
    })
}

exports.preCheckSatisfactorySurvey  = (userinfo,service_request_id)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number. Would you like to check for another ticket number?"
           
            const responsePrecheck = await this.getServiceDetails({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(responsePrecheck.length){
                let requestor = responsePrecheck[0]['wr.requestor'];
                if(requestor!=userinfo.user_name && requestor!=userinfo.em_id){
                    status = 3;
                    message = `unauthorised_access`
                }
                else{     
                    status = 1;
                    let work_request_id = responsePrecheck[0]['wr.wr_id'];
                    let responsegetSatisfactionSurveySteps = await this.getSatisfactionSurveySteps({
                        username:userinfo.user_name,
                        employee_id:userinfo.em_id,
                        role:userinfo.role_name,
                        work_request_id:work_request_id
                    })
                    if(responsegetSatisfactionSurveySteps.length==1){
                        message = ` On a scale of 1-5, how satisfied are you with the service/experience you received? Your feedback is valuable to us and will help us improve our service in the future.
                        <buttons> 5 - Exceptional # 5 / 4 - Above Average # 4 / 3 - Average # 3 / 2 - Below Average # 2 / 1 - Poor # 1 </buttons>`;
                    }
                }
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            // console.log(err);
            reject({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
        }
    })
}

exports.satisfactionSurvey = (userinfo,service_request_id,rating,review)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id
            if(typeof service_request_id === "string"){
                service_request_id =  service_request_id.sanitize();
            }
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number. Would you like to check for another ticket number?"
           
            const responsePrecheck = await this.getServiceDetails({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(responsePrecheck.length){
                let requestor = responsePrecheck[0]['wr.requestor'];
                if(requestor!=userinfo.user_name && requestor!=userinfo.em_id){
                    status = 3;
                    message = `unauthorised_access`
                }
                else{
                    let work_request_id = responsePrecheck[0]['wr.wr_id'];
                    let responsegetSatisfactionSurveySteps = await this.getSatisfactionSurveySteps({
                        username:userinfo.user_name,
                        employee_id:userinfo.em_id,
                        role:userinfo.role_name,
                        work_request_id:work_request_id
                    })
                    if(responsegetSatisfactionSurveySteps.length==1){
                        let step_id = responsegetSatisfactionSurveySteps[0]["wr_step_waiting.step_log_id"]
                        // status = 1;
                        let responseSubmitSatisfactionSurvey = await this.submitSatisfactionSurvey({
                            username:userinfo.user_name,
                            service_request_id:service_request_id,
                            work_request_id:work_request_id,
                            rating:rating,
                            review:review,
                            step_id : step_id
                        })
                        if(responseSubmitSatisfactionSurvey==true){
                            status = 1;
                            message = ''
                        }
                    }
                    else{
                        // throw('Missing work request id')
                        message = 'Missing work request id'
                    }
                }
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        
        }
        catch(err){
            // console.log(err);
            resolve({status:0,message:'Opps, looks like there was some error. Please try after some time'});
        }
    })
}

exports.listPendingSatisfactionSurvey = (userinfo)=>{
    return new Promise(async (resolve, reject) => {
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number. Would you like to check for another ticket number?"
            let ticket_number = 0;
            let responsegetSatisfactionSurveySteps = await this.getSatisfactionSurveySteps({
                username:userinfo.user_name,
                employee_id:userinfo.em_id,
                role:userinfo.role_name
            })
            logger.info('responsegetSatisfactionSurveySteps : ',responsegetSatisfactionSurveySteps)
            let rec_len = responsegetSatisfactionSurveySteps.length;
            console.log('rec_len : ',rec_len)
            if(rec_len>0){
                console.log('Inside if rec_len : ',rec_len)
                status = 1;
                let buttonsTag;
                if(rec_len > 1){
                    message=`Let's get started. These are the service request ticket numbers for which you can leave your helpful comments and ratings.Which one do you want to start with?`
                    buttonsTag = `<buttons>`
                    responsegetSatisfactionSurveySteps.forEach(element => {
                        let service_request_id = element["wr_step_waiting.activity_log_id"]
                        buttonsTag += ` ${service_request_id} # ${service_request_id} /`;
                 
                    });
                    console.log('buttonsTag : ',buttonsTag)
                    buttonsTag = buttonsTag.substring(0, buttonsTag.length - 1);
                    buttonsTag +=`</buttons>`;
                    
                
                }else{
                    let service_request_id = responsegetSatisfactionSurveySteps[0]["wr_step_waiting.activity_log_id"]
                    ticket_number = service_request_id;
                    message=`Okay, you have one service request ${service_request_id} pending for review. Would you like to provide your insightful feedback and rating?`
                    buttonsTag = `<buttons>Yes # ${service_request_id} / No # stop </buttons>`
                }
                message = message + buttonsTag
                
            }
            else{
                console.log('Inside else rec_len : ',rec_len)
                message = 'There are no requests pending for satisfactory survey'
            }
            let output = {
                status : status,
                message : message,
                ticket_number : ticket_number
            }
            resolve(output);
        
        }
        catch(err){
            // console.log(err);
            resolve({status:0,message:'Opps, looks like there was some error. Please try after some time'});
        }
    })
}

exports.preCheckUpdate = (userinfo,service_request_id)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id;
        if(typeof service_request_id === "string"){
            service_request_id =  service_request_id.sanitize();
        }
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number.  Perhaps your ticket has already been cancelled, or you can reverify and try updating again. "
           
            /** Pre check for cancalling */
            const responsePrecheck = await this.getServiceDetails({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(responsePrecheck.length){
                
                let request_status = responsePrecheck[0]['wr.status'];
                let service_request_status = service_request_status_values[request_status]
                
                let requestor = responsePrecheck[0]['wr.requestor'];
                if(requestor!=userinfo.user_name && requestor!=userinfo.em_id){
                    status = 3;
                    message = `unauthorised_access`
                }
                else if(request_status == 'R'||'A'||'Rej'||'AA'){
                    status = 1;
                    message='';
                    /** Cancel Request */
                    let work_request_id = responsePrecheck[0]['wr.wr_id'];
                }
                else{
                    /** This request cannot be edited */
                    service_request_status = `<i>${service_request_status.toLowerCase().capitalize()}</i>`
                    message = `The status of the request is ${service_request_status}. I'm sorry, I'm unable to change this request. Want to make any updates to another service request?`
                }
                
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            console.log(err);
            reject({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
        }
    })
}


exports.UpdateRequest = (userinfo,service_request_id,description)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id;
        if(typeof service_request_id === "string"){
            service_request_id =  service_request_id.sanitize();
        }
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number. Please reverify your ticket number."
           
            /** read Service Request Description */
            const responsePrecheck = await this.readServiceRequestDescription({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(responsePrecheck && responsePrecheck.description){
                status = 1;
                let wr_id = responsePrecheck.wr_id;
                let old_description = responsePrecheck.description;
                const now = new Date();
                let update_date = date.format(now, 'YYYY-MM-DD HH:mm:ss');
                description=`${old_description}\n[${update_date}][${userinfo.user_name}] - ${description}`
                    // let work_request_id = responsePrecheck[0]['wr.wr_id'];
                let responseUpdateRequest = await this.editServiceRequest({
                    username:userinfo.user_name,
                    service_request_id:service_request_id,
                    description:description
                })
                console.log('responseUpdateRequest : ',responseUpdateRequest);
                if(responseUpdateRequest==true){
                    message = `Done! Your request has been updated. Do you want to update another ticket?`
                }
                else message = `Opps! There was an error while updating your request. Please try again after some time.`
                
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            console.log(err);
            // if(err.status==404){
            //     message = message;
            //     resolve({status:1,message:message});
            // }
            // else{
                resolve({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
            // }
        }
    })
}

exports.checkServiceRequestStatus_old = (userinfo,service_request_id)=>{
    return new Promise(async (resolve, reject) => {
        service_request_id =  service_request_id.sanitize()
        try{
            let status = 0;
            let message = "Sorry, we could not find your ticket number. Would you like to check for another ticket number?"
           
            const requests = await this.getServiceDetails({
                username:userinfo.user_name,
                service_request_id:service_request_id
            });
            // console.log('requests : ',requests);
            if(requests.length){
                status = 1;
                let service_request_status = requests[0]['activity_log.status'];
                let date_scheduled_end = requests[0]['activity_log.date_estimated'];
                let date_scheduled=new Date(date_scheduled_end);
                let estimated_date = 'not estimated' 
                if(date_scheduled_end){
                    
                    estimated_date = new Date(date_scheduled).toLocaleDateString('en-us', {year:"numeric", month:"short",day: 'numeric'});
                }
                let today = new Date();
                let artical = 'is'
                if(service_request_status == 'N/A'){
                    
                    service_request_status = `not yet defined`;
                }
                else{
                    service_request_status = `<i>${service_request_status.toLowerCase().capitalize()}</i>`
                }
                if(today>date_scheduled){
                    artical = 'was'
                }
                message = `The status of the request is ${service_request_status} and the scheduled end ${artical} ${estimated_date}. Would you like to check for another status request?`
            }
            let output = {
                status : status,
                message : message,
                name :"",
                number : ""
            }
            resolve(output);
        }
        catch(err){
            console.log(err);
            reject({status:0,message:'Opps, looks like there was some error while fetching your status request. Please try after some time'});
        }
    })
}

Object.defineProperty(String.prototype, 'getdays', {
value: function() {
//   return this.charAt(0).toUpperCase() + this.slice(1);
    var one_day = 1000 * 60 * 60 * 24
    let date_scheduled_end = new Date(this) ;
    let today = new Date();
    console.log('date_scheduled_end : ',date_scheduled_end);
    console.log('today : ',today);
    var time_diff = Math.round(date_scheduled_end.getTime() - today.getTime()) / (one_day);
    console.log('time_diff : ',time_diff);
    var days_diff = time_diff.toFixed(0);
    if(time_diff>0 && time_diff<0.5){
    return 'by today'
    }
    else if(days_diff==1)
    return 'in 1 day';
    else if(days_diff>1)
    return `in ${days_diff} days`;
    else return 0;
},
enumerable: false
});

Object.defineProperty(String.prototype, 'capitalize', {
    value: function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    },
    enumerable: false
});

Object.defineProperty(String.prototype, 'sanitize', {
    value: function() {
        return this.toString().replace(/^[ '"]+|[ '"]+$|( ){2,}/g,'$1');
    },
    enumerable: false
});