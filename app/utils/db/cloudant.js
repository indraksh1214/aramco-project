/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

//Load config module
const config = require("../../../config/config");
const logger = require("../../utils/logger/logger");

var moment = require("moment");

var url = config.cloudantDB.url;
var db_answer_store = require("cloudant-quickstart")(url, config.cloudantDB.db_answer_store);
var db_conversations = require("cloudant-quickstart")(url, config.cloudantDB.db_logs);
var db_feedback = require("cloudant-quickstart")(url, config.cloudantDB.db_feedback);

module.exports = {
  saveConversation: saveConversation,
  saveFeedback: saveFeedback,
  getAnswerContent: getAnswerContent,
};

function saveConversation(data, message, callback) {
  return new Promise(function (resolve, reject) {
    if (data) {
      if (data.input) {
        var input = data.input.text || "";
      }
      if (data.intents && data.intents[0]) {
        if (data.intents[0].intent) var intent = data.intents[0].intent;
        else var intent = "";
      }
      if (data.output) {
        output = data.output.text[0];
      }
      if (data.context) {
        var conversationLog = {
          conv_id: data.context.conversation_id,
          user_id: message.recipient.id,
          input: input,
          intent: intent || "",
          intents: data.intents,
          output: message.message.text,
          timestamp: new Date(),
          timestampHuman: moment().format("LLLL"),
          source: process.env.NODE_ENV,
          context: data.context
        };
      }
    } else {
      var conversationLog = {
        _id: "error",
        error: "error",
      };
    }

    db_conversations.insert(conversationLog).then(function (body) {
      logger.info("SAVE-DATABASE : You have inserted the conversation", body._id);

      if (body) {
        resolve(body._id);
      } else {
        logger.error("==========================================");
        logger.error("ERROR SAVE-DATABASE : You have inserted the conversation", err);
        logger.error("==========================================");
        reject(err);
      }
    });
  });
}

function saveFeedback(data) {
  return new Promise(function (resolve, reject) {
    db_feedback.insert(data).then(function (body) {
      logger.info("SAVE-FEEDBACK : You have inserted the feedback", body._id);

      if (body) {
        resolve(body._id);
      } else {
        reject(err);
      }
    });
  });
}

function getAnswerContent(queryObject) {
  var outputText = queryObject.text;
  var channel = "web"

  var queryID = queryObject.text + ":" + channel + ":" + queryObject.language;
  console.log(queryID)
  var results = [];

  return db_answer_store
    .get(queryID)
    .then(function (body) {
      logger.debug("GET ANSWER DOC with AnswerID: ", body.answerId);
      if (body.answerOptions && body.answerOptions.length > 0) {
        outputText = body.answerOptions[0].answerText;
      }
      return Promise.resolve(outputText);
    })

    .catch(function (err) {
      if (err.error.error === "not_found") {
        logger.info("INFO - NOT FOUND SO SHOWING THE TEXT FROM ASSISTANT");
        return Promise.resolve(outputText);
      } else {
        logger.error("ERROR-cloudant.js", queryID, err.message);
        throw new Error(err);
        //TODO if we have an error 429 too_many_requests we should try the retry functionality
      }
    });
}
