/**
 * Copyright 2019 IBM Corp. All Rights Reserved
 * IBM Confidential Source Code Materials
 *
 * IBM grants recipient of the source code (“you”) a non-exclusive, non-transferable, revocable (in the case of breach of this license or termination of your subscription to
 * the applicable IBM Cloud services or their replacement services) license to reproduce, create and transmit, in each case, internally only, derivative works of the source
 * code for the sole purpose of maintaining and expanding the usage of applicable IBM Cloud services. You must reproduce the notices and this license grant in any derivative
 * work of the source code. Any external distribution of the derivative works will be in object code or executable form only.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an “AS IS” BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */

//Load config module
const config = require("../../../config/config");
const logger = require("../../utils/logger/logger");
var moment = require("moment");

var MongoClient = require('mongodb').MongoClient;
var db_client;
var db_conversations;
var db_answer_store;
var db_feedback;
var db_user_activity_logs;

var connection_params = {}
if (config.mongoDB.ssl==="true"){
  ca = [new Buffer(config.mongoDB.ca_certificate_base64, 'base64')];
  connection_params = {
    ssl: true,
    sslValidate: true,
    sslCA: ca,
    poolSize: 1,
    useUnifiedTopology: true
  }
} else if (config.mongoDB.ssl==="false"){
  connection_params = { 
    poolSize: 1,
    useUnifiedTopology: true
  }
}

MongoClient.connect(config.mongoDB.uri, connection_params, function(err, db) {
    try {
      if (err) throw(err);
      // initiate db
      db_client = db
      // logger.info(" * connected to MongoDB Server successfully", db);

      // initiate collections
      db_conversations = db_client.db(config.mongoDB.dbname).collection(config.mongoDB.db_logs);
      db_feedback = db_client.db(config.mongoDB.dbname).collection(config.mongoDB.db_feedback);
      db_answer_store = db_client.db(config.mongoDB.dbname).collection(config.mongoDB.db_answer_store);
      db_faq_store = db_client.db(config.mongoDB.dbname).collection(config.mongoDB.db_faq_store);
	  db_user_activity_logs = db_client.db(config.mongoDB.dbname).collection(config.mongoDB.db_user_activity_logs);
	  
      logger.info(" * initiated collections successfully");
    } catch (err) {
      logger.info(" * failed while initiating collections");
      logger.error(err);
    }    
});

module.exports = {
  saveConversation: saveConversation,
  saveFeedback: saveFeedback,
  getAnswerContent: getAnswerContent,
  saveToUserLogs: saveToUserLogs,
  updateCountCRM: updateCountCRM,
  updateCountRemedy: updateCountRemedy
};

function saveConversation(data, message, callback) {
  return new Promise(function (resolve, reject) {
    if (data) {
      if (data.input) {
        var input = data.input.text || "";
      } else if (data.context && data.context.last_text) {
        //var input = "Test text";
        var input = data.context.last_text;
      }
      if (data.intents && data.intents[0]) {
        if (data.intents[0].intent) var intent = data.intents[0].intent;
        else var intent = "";
      } else if(data.output && data.output.intents && data.output.intents[0] && data.output.intents[0].intent)
      var intent = data.output.intents[0].intent;
      if (data.output) {
        output = data.output.text[0];
      }
      if (data.context) {
        var conversationLog = {
          conv_id: data.context.conversation_id,
          user_id: message.recipient.id,
          input: input,
          intent: intent || "",
          intents: data.output.intents,
          output: message.message.text,
          timestamp: new Date(),
          timestampHuman: moment().format("LLLL"),
          source: process.env.NODE_ENV,
          context: data.context
        };
      }
      //console.log(conversationLog);
    } else {
      var conversationLog = {
        _id: "error",
        error: "error",
      };
    }
    
    db_conversations.insertOne(conversationLog).then(function (body) {
      logger.info("SAVE-DATABASE : You have inserted the conversation", body.insertedId);

      if (body) {
        resolve(body.insertedId);
      } else {
        logger.error("==========================================");
        logger.error("ERROR SAVE-DATABASE : You have inserted the conversation", err);
        logger.error("==========================================");
        reject(err);
      }
    });
  });
}

function saveFeedback(data) {
  return new Promise(function (resolve, reject) {
    db_feedback.insertOne(data).then(function (body) {
      logger.info("SAVE-FEEDBACK : You have inserted the feedback", body.insertedId);
      
      if (body) {
        resolve(body.insertedId);
      } else {
        reject(err);
      }
    });
  });
}

function getAnswerContent(queryObject) {
  var outputText = queryObject.text;
  var channel = "web"

  var queryID = queryObject.text + ":" + channel + ":" + queryObject.language;
  
  var results = [];

  console.log(" * queryID:"+queryID)

  var getAnswerFrom = queryObject.text.split("_");
  console.log('getAnswerFrom[0] : ',getAnswerFrom[0])
  var collection_name = db_answer_store;
  if(getAnswerFrom[0]==='Faq'){
    collection_name = db_faq_store
  }
  console.log('collection_name : ',collection_name)
  return collection_name.findOne({type:queryObject.text}).then(function (body) {
      //logger.info("  * AnswerID: ", body);
      if (body){
        // if (body.answerOptions && body.answerOptions.length > 0) {
        //   outputText = body.answerOptions[0].answerText;
        // }
        if (body.message) {
          outputText = body.message;
        }
      }
      return Promise.resolve(outputText);
    }).catch(function (err) {
      if (err.error.error === "not_found") {
        logger.info("INFO - NOT FOUND SO SHOWING THE TEXT FROM ASSISTANT");
        return Promise.resolve(outputText);
      } else {
        logger.error("ERROR-mongodb.js", queryID, err.message);
        throw new Error(err);
        //TODO if we have an error 429 too_many_requests we should try the retry functionality
      }
    });
}

async function saveToUserLogs(row){
	return new Promise(function (resolve, reject) {
			db_user_activity_logs.insertOne(row).then(function (body) {
			console.log("New user has been added to userLogs", body.insertedId);
			  if (body) {
				resolve(body.insertedId);
			  } else {
				reject('error: db_user_activity_logs.insertOne');
			  }
			});
  });
}

async function updateCountCRM(query){
	db_user_activity_logs.updateOne(query, {$inc:{'CRM_Count': 1}}).then(function(){
		console.log("UPDATE CRM COUNT");
	}).catch(function(err){
		logger.error("Error updating CRM Count: ", query, err);
	})
}

async function updateCountRemedy(query){
	db_user_activity_logs.updateOne(query, {$inc:{'Remedy_Count': 1}}).then(function(){
		console.log("UPDATE Remedy COUNT");
	}).catch(function(err){
		logger.error("Error updating Remedy Count: ", query, err);
	})
}