const dotenv = require("dotenv");
dotenv.config({path: '../../.env'});
const fs = require('fs');
const argv = require('minimist')(process.argv.slice(2));

dotenv.config();

// Mongo DB settings
const mongoDB = {
  uri: process.env.MONGO_DB_URI,
  dbname: process.env.MONGO_DB_DATABASE,
  ssl: process.env.MONGO_SSL,
  ca_certificate_base64: process.env.MONGO_CA_CERTIFICATE_BASE64,
  db_logs: process.env.MONGO_DB_CONVERSATION_LOGS,
  db_feedback: process.env.MONGO_DB_FEEDBACK_LOGS,
  db_answer_store: process.env.MONGO_DB_ANSWER_STORE,
};

console.log(" * mongoDB settings:",mongoDB);

var connection_params = {}
if (mongoDB.ssl==="true"){
  ca = [new Buffer(mongoDB.ca_certificate_base64, 'base64')];
  connection_params = {
    ssl: true,
    sslValidate: true,
    sslCA: ca,
    poolSize: 1,
    reconnectTries: 1
  }
} else if (mongoDB.ssl==="false"){
  connection_params = {}
}

console.log(" * mongoDB connection params:",connection_params);

var MongoClient = require('mongodb').MongoClient;
var db_logs;
var db_feedback;
var db_answer_store;

MongoClient.connect(mongoDB.uri, connection_params, function(err, db) {
    try {
      if (err) throw(err);
      // initiate db
      db_client = db
      console.log(" * connected to MongoDB Server successfully for admin route");
      // initiate collections
      db_logs = db_client.db(mongoDB.dbname).collection(mongoDB.db_logs);
      db_feedback = db_client.db(mongoDB.dbname).collection(mongoDB.db_feedback);
      db_answer_store = db_client.db(mongoDB.dbname).collection(mongoDB.db_answer_store);
      console.log(" * initiated collections successfully for admin route");

      console.log(" * importing answer store ... ");
      import_all(db_client, db_answer_store, "answer_store")
      console.log(" * answer store was imported successfully ");

    } catch (err) {
      console.log(" * failed while initiating collections for admin route");
      console.log(err);
    }
});

function import_all(db_client, db, file_name){
    let filePath = argv.file || "tools/migration/data_exchange/" + file_name + ".json"
    console.log("filePath: ", filePath)
  fs.readFile(filePath, (err, data) => {
    if (err) throw err;
    let json_data = JSON.parse(data);

    db.deleteMany({}, function(err, res) {
      if (err) throw err;
      console.log("Number of documents deleted: " + res.result.n);

      db.insertMany(json_data.collection, function(err, res) {
        if (err) throw err;
        console.log("Number of documents inserted: " + res.insertedCount);
        db_client.close();
      });

    });



  });
}

